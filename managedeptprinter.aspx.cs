﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class managedeptprinter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string GetAll()
    {
        string lstDept = new SettingExPrintingBLL().GetOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            DeptLists = lstDept

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string UpdateDepartmentPrinter(string DepartmentName,string Printer)
    {
        int status = new SettingExPrintingDAL().UpdateDepartmentPrinter(DepartmentName,Printer);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            Status = status,
        };
        return ser.Serialize(JsonData);
    }



}