﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class admin : System.Web.UI.MasterPage
{
    public string EmployeeName { get; set; }
    public string BranchName { get; set; }
    //public string m_smessage { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (IsPostBack == false)
        //{
        //    SoftKeyboard sf = new SoftKeyboard();
        //    sf.getvalue();
        //}
        if (!Request.Cookies.AllKeys.Contains(Constants.AdminId))
        {
            Response.Redirect("loginerror.aspx");
        }

        EmployeeName = Request.Cookies[Constants.EmployeeName].Value;
        BranchName = Request.Cookies[Constants.BranchName].Value;
        //User objUser = new User();
        //new UserBLL().GetSiteUrl(objUser);
        //m_smessage = objUser.Title;


    }

 
}
