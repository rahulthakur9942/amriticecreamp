﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using System.Web.Script.Serialization;

public partial class backoffice_DayOpenClose : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection();
    int retVal = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            GetCurrentStatus();

        }
    }

    void GetCurrentStatus()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Branchid.Value = Convert.ToString(Branch);
        hdnClosedDay.Value = "";
        hdnOpenedDay.Value = "";
        txtDate.Text = DateTime.Now.ToString("d");
        string date = "";
        string Status = "";
        date = new DayOpenCloseDAL().GetCurrentStatus(Branch, out Status);

        if (Status == "1")
        {
            hdnOpenedDay.Value = Convert.ToDateTime(date).ToString("d");
        }
        else
        {
            hdnClosedDay.Value = Convert.ToDateTime(date).ToString("d");
        }
        hdnStatus.Value = Status;

        string OpenClose = "Close";
        if (rbDayOpen.Checked)
        {
            OpenClose = "Open";

        }

        int retVal = new DayOpenCloseDAL().DayOpenCloseCheckDelivery(OpenClose, DateTime.Now.Date, Branch);
        con.Close();
        if (hdnOpenedDay.Value != "")
        {
            rbDayClose.Enabled = true;
            rbDayOpen.Enabled = false;

            rbDayOpen.Checked = false;
            rbDayClose.Checked = true;


            hdnLastOpenedDate.Value = date;
            lblMessage.Text = "Date '" + date + "' is not Closed";
        }
        else
        {
            rbDayClose.Enabled = false;
            rbDayOpen.Enabled = true;

            rbDayOpen.Checked = true;
            rbDayClose.Checked = false;

        }

    }
    [WebMethod]
    public static string GetPendingSettlement(string BranchID)
    {
        //Get OpenCloseDay Date Start
        string SQlQuery = "select date from dayopenclose";
        Connection con = new Connection();
        DataTable DayOpenClosedataTable = new DataTable();
        SqlDataAdapter DayOpenClosedad = new SqlDataAdapter(SQlQuery, con.sqlDataString);
        DayOpenClosedad.Fill(DayOpenClosedataTable);
        string Date = DayOpenClosedataTable.Rows[0].ItemArray[0].ToString();
        //Get OpenCloseDay Date End

        string OpenClose = "Close";
        int RetVal = new DayOpenCloseDAL().DayOpenCloseCheckDelivery(OpenClose, Convert.ToDateTime(Date), Convert.ToInt32(BranchID));

        //check KotRaise Bill start//
        SQlQuery = "select Distinct  TableID,TotalAmount from dbo.Kot where Active = 1 and Complementary = 0 and billnowprefix='' and  BranchId =" + BranchID;
        DataTable DataTableKotRaise = new DataTable();
        SqlDataAdapter KotRasieDad = new SqlDataAdapter(SQlQuery,con.sqlDataString);
        KotRasieDad.Fill(DataTableKotRaise);
        StringBuilder sb = new StringBuilder();

        if (DataTableKotRaise.Rows.Count >0)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Table");
            dt.Columns.Add("Amount");

            sb.Append("<table><tr><h3>KOTs are pending for Billing.</h3></tr>");
            sb.Append("<tr><td  colspan='6'><th colspan='2'>Table<th></td><td><th>Amount</th></td></tr>");
            for (int i = 0; i < DataTableKotRaise.Rows.Count; i++)
            {
                sb.Append("<tbody><tr><td colspan='9'>" + DataTableKotRaise.Rows[i].ItemArray[0].ToString() + "</td><td></td><td>" + DataTableKotRaise.Rows[i].ItemArray[1].ToString() + "</td></tr><tbody>");
            }
            sb.Append("</table>");
        }
        //check KotRaise Bill end//

        //Pending Payments bill start//
        //SQlQuery = "SELECT [Code],net_amount,bill_date FROM(SELECT DISTINCT billnowprefix AS [Code], billnowprefix AS [BillNoWPrefix],net_amount,Isnull(Sum(cash_amount + credit_amount + crcard_amount + onlinepayment), 0.00) AS Total_amount, bill_date FROM bill_master  GROUP BY billnowprefix,  net_amount,bill_date)q WHERE  total_amount<> net_amount";
        SQlQuery = "select BillNoWPrefix as CODE, net_amount,bill_date from dbo.bill_master where net_amount<>Cash_Amount + Credit_Amount + CrCard_Amount + OnlinePayment  and BranchId = '"+BranchID+ "' and Bill_Value <> 0 and pos_id =3 GROUP BY billnowprefix,  net_amount,bill_date,cst_id";
            DataTable PendingBillDataTable = new DataTable();
            SqlDataAdapter PendingBillDad = new SqlDataAdapter(SQlQuery, con.sqlDataString);
            PendingBillDad.Fill(PendingBillDataTable);
            //Pending Payments bill end//
            if (PendingBillDataTable.Rows.Count > 0)
            {
                sb.Append("<table><tr><h3>Pending Settlement.</h3></tr>");
                sb.Append("<tr><td  colspan='6'><th colspan='2'>BillNO<th></td><td><th>Amount</th><th>Bill Date</th></td></tr>");
                for (int i = 0; i < PendingBillDataTable.Rows.Count; i++)
                {
                    DateTime Dates = Convert.ToDateTime(PendingBillDataTable.Rows[i].ItemArray[2]);
                    sb.Append("<tbody><tr><td colspan='9'>" + PendingBillDataTable.Rows[i].ItemArray[0].ToString() + "</td><td></td><td>" + PendingBillDataTable.Rows[i].ItemArray[1].ToString() + "</td><td>" + Dates.ToString("dd/MM/yyyy") + "</td></tr><tbody>");
                }
                sb.Append("</table>");
            }
      
        var JsonData = new
        {
            productData = sb.ToString(),
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string OpenClose = "Close";
        if (rbDayOpen.Checked)
        {
            OpenClose = "Open";
        }
        retVal = new DayOpenCloseDAL().DayOpenCloseCheckDelivery(OpenClose, Convert.ToDateTime(txtDate.Text), Branch);
        RequestCookies.Value = Convert.ToString(retVal);
        if (retVal == -1)
        {
            Response.Write("<script>alert('Day Already Opened')</script>");
        }
        
        else if (retVal == -2)
        {
            Response.Write("<script>alert('Billing Already Done In Future Date')</script>");
        }

        else if (retVal == -3)
        {
            Response.Write("<script>alert('Past Day Is Not Closed')</script>");
        }

        else if (retVal == -5)
        {
            Response.Write("<script>alert('Day Opened Successfully')</script>");
            Response.Write("<script>window.close();</script>");
        }
        else if (retVal == -11)
        {
            Response.Write("<script>alert('Day already Closed')</script>");
        }
        else if (retVal == -10)
        {
            pnlOpenClose.Visible = false;
            pnlVerify.Visible = true;
        }
        else if (retVal == -15)
        {
            Response.Write("<script>alert('Day Closed Successfully')</script>");
            Response.Write("<script>window.close();</script>");
        }
        else if (retVal == -21)
        {
            Response.Write("<script>alert('KOTs are pending for Billing.')</script>");
        }
        else if (retVal == -22)
        {
            Response.Write("<script>alert('Bills are pending for Settlement.')</script>");
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.close();</script>");
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        pnlOpenClose.Visible = true;
        pnlVerify.Visible = false;
        Response.Redirect("Reports/rptDeliveryNote.aspx?Date=" + Convert.ToDateTime(txtDate.Text) + "");
    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int retVal = new DayOpenCloseDAL().DayOpenCloseForceClose(Branch);
        if (retVal == 1)
        {
            Response.Write("<script>alert('Day Closed Successfully' )</script>");
            Response.Write("<script>window.close();</script>");
            pnlOpenClose.Visible = true;
            pnlVerify.Visible = false;
        }
    }
}