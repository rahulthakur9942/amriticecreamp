﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class managecompanies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSuppliers();
        }

        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.COMPANIES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
    void BindSuppliers()
    {
        ltSuppliers.Text = new SupplierBLL().GetSupplierHtml();
    }

    [WebMethod]
    public static string Insert(int CompanyId, string Title, bool IsActive, string Suppliers)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.COMPANIES));

        string[] arrRoles = sesRoles.Split(',');


        if (CompanyId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Company objCompany = new Company()
        {
            Company_ID = CompanyId,
            Company_Name = Title.Trim().ToUpper(),
            SupplierList=Suppliers,
            IsActive = IsActive,
            UserId = Id,

        };

        string[] arrSuppliers = Suppliers.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("Id");
        DataRow dr;

        for (int i = 0; i < arrSuppliers.Length; i++)
        {
            dr = dt.NewRow();
            dr["Id"] = arrSuppliers[i];
            dt.Rows.Add(dr);
        }


        status = new CompanyBLL().InsertUpdate(objCompany,dt);
        var JsonData = new
        {
            company = objCompany,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 CompanyId)
    {
        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.COMPANIES));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }

        Company objCompany = new Company()
        {
            Company_ID = CompanyId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new CompanyBLL().DeleteCompany(objCompany);
        var JsonData = new
        {
            company = objCompany,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}