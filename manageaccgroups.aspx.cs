﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class manageaccgroups : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.ACCOUNTGROUPS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string Insert(int GroupId, string GroupName, string Account)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.ACCOUNTGROUPS));

        string[] arrRoles = sesRoles.Split(',');


        if (GroupId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);

        AccGroups objAccGroup = new AccGroups()
        {
            S_Id = GroupId,
            S_CODE = "",
            S_NAME = GroupName.Trim().ToUpper(),
            AMOUNT = 0,
            UserId = Id,
            H_CODE = Account,


        };
        status = new AccGroupsBLL().InsertUpdate(objAccGroup);
        var JsonData = new
        {
            AccGroup = objAccGroup,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindHeads()
    {

        string HeadsData = new AccHeadsBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            HeadsOptions = HeadsData

        };
        return ser.Serialize(JsonData);
    }

     
}