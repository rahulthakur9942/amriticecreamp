﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ManageTable : System.Web.UI.Page
{
  MangeTable objManageTable = new MangeTable();
    string multiprint = "";
  protected void Page_Load(object sender, EventArgs e)
  {
    BindGrid();
  }
  public void btnsave_Click(object sender, EventArgs e)
  {
        if (chkmultiprint.Checked == true)
        {
            multiprint = "true";

        }
        else
        {
            multiprint = "false";
        }

    if(btnsave.Text=="Submit")
    {
      objManageTable.Insert_Update_Record(txtTableName.Value, 0, multiprint);
      txtTableName.Value = "";
            chkmultiprint.Checked = false;
      ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Tables Added Successfully!');", true);
    }
    else  if(btnsave.Text == "Update")
    {
      objManageTable.Insert_Update_Record(txtTableName.Value, Convert.ToInt32(ViewState["TableID"]), multiprint);
      btnsave.Text = "Submit";
      txtTableName.Value = "";
            chkmultiprint.Checked = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Tables Updated Successfully!');", true);
    }
    BindGrid();
  }
  protected void gv_display_OnRowCommand(object sender, GridViewCommandEventArgs e)
  {
    int TableID = Convert.ToInt32(e.CommandArgument);
    DataSet ds = objManageTable.GetRecordTableByID(TableID);
    
      ViewState["TableID"] = ds.Tables[0].Rows[0]["TableID"];
      txtTableName.Value = ds.Tables[0].Rows[0]["TableName"].ToString();
       multiprint= ds.Tables[0].Rows[0]["MultiPrint"].ToString();

        if (multiprint == "True")
        {
            chkmultiprint.Checked = true;

          

        }
        else {
            chkmultiprint.Checked = false;
        }
        btnsave.Text = "Update";
       
      
  }
  protected void OnPaging(object sender, GridViewPageEventArgs e)
  {
    gv_display.PageIndex = e.NewPageIndex;
    gv_display.DataBind();
    BindGrid();
  }
  public void BindGrid()
  {
    gv_display.DataSource = objManageTable.GetRecordTable();
    gv_display.DataBind();
  }
}