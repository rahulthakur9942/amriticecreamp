﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managestates : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]

    public static string Delete(Int32 StateId)
    {
        States objState = new States()
        {
            State_ID = StateId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new StateBLL().DeleteState(objState);
        var JsonData = new
        {
            state = objState,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}