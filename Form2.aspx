﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Form2.aspx.cs" Inherits="Form2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


        <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="js/SearchPlugin.js"></script>

       <script type="text/javascript">



       var DeliveryNoteCollection = [];
       function clsBill() {
          
           this.BillNo = "";
           this.Amount = 0;
          
       }

    
        function BindNotes() {

            var html = "";
         
           for (var i = 0; i < DeliveryNoteCollection.length; i++) {
               
               
         

               html += "<tr> ";
               html += "<td><input type='checkbox' class='chkselval' id='chkbox"+i+"'/></td>";
               html += "<td>" + DeliveryNoteCollection[i]["BillNo"] + "</td>";
               html += "<td>" + DeliveryNoteCollection[i]["Amount"] + "</td>";
             
              
             
               html += "</tr>";

              
           }


           $("#tbShowBills").html(html);
          


       }

       $(document).ready(
           function () {
               $("#txtStartDate,#txtEndDate").val($("#<%=hdnDate.ClientID%>").val());
               $('#txtStartDate').daterangepicker({
                   singleDatePicker: true,
                   calender_style: "picker_1"
               }, function (start, end, label) {
                   console.log(start.toISOString(), end.toISOString(), label);
               });

               $('#txtEndDate').daterangepicker({
                   singleDatePicker: true,
                   calender_style: "picker_1"
               }, function (start, end, label) {
                   console.log(start.toISOString(), end.toISOString(), label);
               });

               $("#btnShow").click(
                   function () {


                       DeliveryNoteCollection = [];

                       $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();


                       var DateFrom = $("#txtStartDate").val();
                       var DateTo = $("#txtEndDate").val();
                       var Branch = $("#ddlBranch").val();

                       $.ajax({
                           type: "POST",
                           data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","Branch": "' + Branch + '"}',
                           url: "Form2.aspx/GetNonExciseBYDate",
                           contentType: "application/json",
                           dataType: "json",
                           success: function (msg) {

                               var obj = jQuery.parseJSON(msg.d);
                               if (obj.BillsList.length > 0) {
                                   for (var i = 0; i < obj.BillsList.length; i++) {

                                    
                                       TO = new clsBill();


                                       TO.BillNo = obj.BillsList[i]["BillNowPrefix"];
                                       TO.Amount = obj.BillsList[i]["Net_Amount"];

                                       DeliveryNoteCollection.push(TO);

                                   }
                                   
                                   BindNotes();
                               }
                               else {
                                   alert("No Non-Excise Bills Found.");
                                   return;

                               }

                           },
                           error: function (xhr, ajaxOptions, thrownError) {

                               var obj = jQuery.parseJSON(xhr.responseText);
                               alert(obj.Message);
                           },
                           complete: function () {

                               $.uiUnlock();
                           }

                       });



                   });



                   $("#btnDelete").click(
                   function () {

                     
                       var Billno = [];
                       var BillNowPrefix = [];
                       var Amount = [];
                      
                       var Branch = $("#ddlBranch").val();

                       if (DeliveryNoteCollection.length == 0)
                       {
                           alert("No Record Selected!");

                           return false;

                       }
                       for (var i = 0; i < DeliveryNoteCollection.length; i++) {
                          
                           var Billprefix = "";
                           var netamount = 0;
                        
                           Billprefix = DeliveryNoteCollection[i]["BillNo"];
                           netamount = DeliveryNoteCollection[i]["Amount"];




                           if ($("#chkbox" + [i]).prop("checked") == true)
                               {
                               BillNowPrefix[i] = Billprefix;
                               Amount[i] = netamount;
                           }
                           

                       }

                    

                       if (confirm("Are You sure to save this record")) {
                           $.uiLock('');


                           $.ajax({
                               type: "POST",
                               data: '{"BillNowPrefix":"' + BillNowPrefix + '","Branch": "' + Branch + '"}',
                               url: "Form2.aspx/Insert",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);
                                   if (obj.Status == "0") {
                                       alert("Sale Saved Successfully");

                                       DeliveryNoteCollection = [];

                                       $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                       $("#ChkSelectAll").prop('checked', false);
                                   }
                                   else {
                                       alert("Operation Failed");
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {
                                   $.uiUnlock();

                               }



                           });

                       }



                   });




           });


           function toggle(source) {
               var checkboxes = document.querySelectorAll('input[type="checkbox"]');
               for (var i = 0; i < checkboxes.length; i++) {
                   if (checkboxes[i] != source)
                       checkboxes[i].checked = source.checked;
               }
           }
               </script>

      <style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
     <form id="form1" runat="server">
      <asp:HiddenField ID="hdnDate" runat="server"/>
    <div class="right_col" role="main">
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Manage Non-Excise Bills </h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">

                                  <tr><td  style="width: 150px">
                                     <label class="control-label">Choose Branch:</label>
                                    </td>

                                         <td colspan="300px">      
                                     <select id="ddlBranch" style="width:321px;height:30px" clientidmode="Static" runat="server">
                                           
                                             </select>


                                    </td></tr>
                                 
                                       <tr>
                                      
                                      <td style="width:80px"><label class="control-label">From:</label></td>  <td  style="width:130px"> <input type="text" class="form-control"  id="txtStartDate" style="width:107px" /></td>
                                       <td style="width:80px"><label class="control-label">To:</label></td>  <td  style="width:150px"> <input type="text" class="form-control"  id="txtEndDate" style="width:107px"  /></td>
                                        <td><button type="button" class="btn btn-success" id="btnShow"  style="width:150px">Show</button></td>





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>
                          

                 

                      <div class="row" id="dvShowBills" > 
               
                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                             
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th><input type="checkbox" id="ChkSelectAll" onclick="toggle(this);" />All<br /></th><th>BillNo</th><th>Amount</th></tr>
</thead>
<tbody id="tbShowBills">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnDelete" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                    
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>
                            </div>
                        </div>
  
    </div>
         </form>

</asp:Content>

