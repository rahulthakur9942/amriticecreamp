﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DayOpenClose.aspx.cs" Inherits="backoffice_DayOpenClose" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <style type="text/css">
       .ui-dialog-titlebar.ui-corner-all.ui-widget-header.ui-helper-clearfix.ui-draggable-handle{
           background-color:red;
       }
   </style>
	 
		
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:Panel ID="pnlOpenClose" runat="server" Height="50%">
       
    <table style="width:350px;background-color:#DCDCDC">
    <tr><td style="background-color:gray;color:White;font-weight:bold">Day Open Close</td></tr>
    <tr><td>
    <table style="width:100%">
    <tr><td><asp:RadioButton ID="rbDayOpen" Checked="true" runat="server"  Text="Day Open"  GroupName="Grp1"/></td><td align="right">mm/dd/yy<asp:TextBox ID="txtDate" ForeColor="Gray" ReadOnly="true" Font-Italic="true" runat="server" BackColor="#DCDCDC"  ></asp:TextBox></td></tr>
    <tr><td><asp:RadioButton ID="rbDayClose" runat="server"  Text="Day Close" GroupName="Grp1"/></td><td></td></tr>
    <tr><td><asp:GridView ID="gvPandingAmount" runat="server">
      </asp:GridView></td></tr>
    </table>
    <asp:HiddenField ID="Branchid" runat="server" />
    </td></tr>
    <tr>
    <td style="padding-left:10px">
    
    <asp:HiddenField ID="hdnOpenedDay" runat="server" />
    <asp:HiddenField ID="hdnClosedDay" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnLastOpenedDate" runat="server" />
        <asp:HiddenField ID="RequestCookies" runat="server" />
    <asp:Label ForeColor="#EA2F00" Font-Italic="true" ID="lblMessage" runat="server"></asp:Label></td>
    </tr>
    <tr>
   
    <td align="right" style="background-color:Silver">
    <table>
    <tr><td><asp:Button ID="btnOK"  class="btn btn-primary btn-small"   runat="server" 
            Text="OK" onclick="btnOK_Click" /></td><td>
            <asp:Button  class="btn btn-primary btn-small"   ID="btnCancel" runat="server" 
                Text="Cancel" onclick="btnCancel_Click" /></td></tr>
    </table>
    </td>
    </tr>
    </table>
    </asp:Panel>
    
        <asp:Panel ID="pnlVerify" runat="server"  Visible="false">
            <table style="width:350px;background-color:#DCDCDC">
    <tr><td style="background-color:gray;color:White;font-weight:bold">Day Open Close</td></tr>
    <tr>
        <td>

            <table>
                <tr><td>There are some Pending Delivery Notes. Are You Sure you want to Continue.</td></tr>
                <tr><td>
                    <table>
                        <tr>
                            <td><asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_Click"/></td>
                            <td><asp:Button ID="btnNo" runat="server" Text="No" OnClick="btnNo_Click"/></td>
                        </tr>

                    </table>

                    </td</tr>
 
            </table>

        </td>

    </tr>
    </table>

        </asp:Panel>
        <div id="dialog" title="Pending KOTs /Settlement"  style="display:none;">
 
</div>
    </form>
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        var BranchID = $("#Branchid").val();
        $.ajax({
            type: "POST",
            data: "{'BranchID':" + BranchID + "}",
            url: "DayOpenClose.aspx/GetPendingSettlement",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                if (obj.productData != "") {
                    $("#dialog").removeAttr("style");
                    $("#dialog").html(obj.productData);
                    $(function () {
                        $("#dialog").dialog();
                    });
                }
                //if (obj.status == "KotRaiseBill") {
                //    $("#ui-id-1").text($("#ui-id-1").text().replace('Pending Settlement', 'KOTs are pending for Billing.'));
                //}
            },
        });
    });
</script>