﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class managedesignations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CheckRole();
    }




    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.DESIGNATION));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
   
    [WebMethod]
    public static string Insert(int DesgID, string DesgName, bool IsActive)
    {


        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.DESIGNATION));

        string[] arrRoles = sesRoles.Split(',');


        if (DesgID == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Designations objDesignation = new Designations()
        {
            DesgID = DesgID,
            DesgName = DesgName.Trim().ToUpper(),
            IsActive = IsActive,
            UserId = Id,

        };
        status = new DesignationBLL().InsertUpdate(objDesignation);
        var JsonData = new
        {
            designation = objDesignation,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]

    public static string Delete(Int32 DesgID)
    {
        Designations objDesignation = new Designations()
        {
            DesgID = @DesgID,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new DesignationBLL().DeleteDesignation(objDesignation);
        var JsonData = new
        {
            department = objDesignation,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}