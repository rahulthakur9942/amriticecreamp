﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managediscounts.aspx.cs" Inherits="managediscounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.timepicker.js" type="text/javascript"></script>
    <script src="js/jquery.timepicker.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
         <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var DiscountType = "";
        var m_DiscountID = 0;

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

        function TakeMeTop() {
            $("html, body").animate({ scrollTop: 0 }, 500);
        }

        function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');

        }

        function fill(DiscountType) {
            if (DiscountType == "ItemDiscount") {

                $("#txtDiscountAmt").css({ "display": "block" });
                $("#lbldiscount").css({ "display": "block" });
            }


            $.ajax({
                type: "POST",
                data: '{"Category": "' + DiscountType + '" }',
                url: "managediscounts.aspx/FillControl",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlDiscountOn").html(obj.OptionData);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    BindGrid(DiscountType);

                }

            });


        }
        function ResetControls() {
            m_DiscountID = -1;
            var btnAdd = $("#btnAdd");
            var btnUpdate = $("#btnUpdate");
            $("#txtStartDate").val("");
            $("#txtEndDate").val("");
            $("#txtCaption").val("");
            $("#txtStartTime").val("");
            $('#chkFullDay').prop('checked', false);
            $("#txtEndTime").val("");
            $("#txtDiscountPer").val("0");
            $("#txtDiscountAmt").val("0");

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
           btnAdd.css({ "display": "none" });
           for (var i = 0; i < arrRole.length; i++) {
               if (arrRole[i] == "1") {

                   btnAdd.css({ "display": "block" });
               }

           }
          

            btnUpdate.css({ "display": "none" });
           

            $("#btnReset").css({ "display": "none" });
            fill(DiscountType);
            validateForm("detach");
        }

        function InsertUpdate() {
         
            var objDiscount = {};
            if (!validateForm("frmCity")) {
                return;
            }

            
            var Id = m_DiscountID;
            var category = $("#ddlDiscountType").val();
            var CompanyId = 0;
            var GroupId = 0;
            var SubGroupId = 0;
            var ItemId = 0;
           
            var Description = "";
            if (category == "ItemDiscount") {
               Description= $("#txtddlItems").val();
                if ($("#ItemDiscount").val() == "") {
                    alert("Choose Item on which you want to give Discount.");
                    $("#ItemDiscount").focus();
                    return;
                }
            }
            else {
                Description = $("#ddlDiscountOn option:selected").text();
                if ($("#ddlDiscountOn").val() == "") {
                    alert("Choose option on which you want to give Discount.");
                    $("#ddlDiscountOn").focus();
                    return;
                }
            
            }

            if (category == "CompanyDiscount") {
                CompanyId = $("#ddlDiscountOn").val();

            }
            else if (category == "GroupDiscount") {
                GroupId = $("#ddlDiscountOn").val();
            }
            else if (category == "SubGroupDiscount") {
                SubGroupId = $("#ddlDiscountOn").val();
            }
            else if (category == "ItemDiscount") {
            ItemId = $("#ddlItems option:selected").val();

           }
        
            
             
            var Startdate = $("#txtStartDate").val();
           
            var Enddate = $("#txtEndDate").val();
          
            var StartTime = $("#txtStartTime").val();
            var EndTime = $("#txtEndTime").val();
            var DiscountPer = $("#txtDiscountPer").val();
            var DiscountAmt = $("#txtDiscountAmt").val();
            var Fullday = false
            if ($('#chkFullDay').is(":checked")) {
                Fullday = true;
            }
            var IsActive = false;

            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }

            objDiscount.Discount_ID = Id;
            objDiscount.Description = Description;
            objDiscount.Category = category;
            objDiscount.Start_Date = Startdate;
            objDiscount.End_Date = Enddate;
            objDiscount.Company_ID = CompanyId;
            objDiscount.Group_ID = GroupId;
            objDiscount.Item_ID = ItemId;
            objDiscount.Discount_Per = DiscountPer;
            objDiscount.Discount_Amt = DiscountAmt;
            objDiscount.FULLDAY = Fullday;
            objDiscount.START_TIME = StartTime;
            objDiscount.END_TIME = EndTime;
       
            objDiscount.SGroup_ID = SubGroupId;
            objDiscount.IsActive = IsActive;



            var DTO = { 'objDiscount': objDiscount };

            $.uiLock('');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managediscounts.aspx/Insert",
                data: JSON.stringify(DTO),
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);


                    if (obj.Status == -11) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == -1) {

                        var txtValue = $("#ddlDiscountOn option:selected").text();

                        alert("Insertion Failed.Discount Already Exists for this ." + txtValue);
                        return;
                    }

                    if (Id == "0") {

                        jQuery("#jQGridDemo").jqGrid('addRowData', obj.discount.Discount_ID, obj.discount, "last");
                        $("#txtDiscountAmt").css({ "display": "none" });
                        $("#lbldiscount").css({ "display": "none" });
                        alert("Discount is added successfully.");

                        ResetControls();
                    }
                    else {

                        var myGrid = $("#jQGridDemo");
                        var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                        myGrid.jqGrid('setRowData', selRowId, obj.discount);
                        $("#txtDiscountAmt").css({ "display": "none" });
                        $("#lbldiscount").css({ "display": "none" });
                        alert("Discount is Updated successfully.");

                        ResetControls();
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }





        $(document).ready(
    function () {

        $("#ddlItems").supersearch({
            Type: "Product",
            Caption: "Please enter Item Name ",
            AccountType: "",
            Width: 214,
            DefaultValue: 0,
            Godown: 0

        });



        $("#ddlDiscountType").change(function () {

            if ($(this).val() == "0") {

                jQuery("#jQGridDemo").GridUnload();
                DiscountType = 0;
                return;
            }


            DiscountType = $("#ddlDiscountType").val();

            if (DiscountType == "ItemDiscount") {

                $("#dvOuter_ddlItems").css({ "display": "block" });
                $("#ddlDiscountOn").css({ "display": "none" });
            }
            else {
                $("#dvOuter_ddlItems").css({ "display": "none" });
                $("#ddlDiscountOn").css({ "display": "block" });

            }
            fill(DiscountType);

        });







        $('#txtStartTime').timepicker();

        $('#txtEndTime').timepicker();


        $('#txtStartDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            $('#txtStartDate').change();
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#txtEndDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            $('#txtEndDate').change();
            console.log(start.toISOString(), end.toISOString(), label);
        });


        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {

                    $("#btnAdd").show();



                    $("#btnAdd").click(
                    function () {


                        m_DiscountID = 0;


                        InsertUpdate();
                    }
                    );


                }

                else if (arrRole[i] == "3") {



                    $("#btnUpdate").click(
                   function () {
                    InsertUpdate();

                    }

                 );

                }
                else if (arrRole[i] == "2") {

                    $("#btnDelete").show();

                    $("#btnDelete").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Discount is selected to Delete");
             return;
         }

         var DiscountId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Discount_ID')


         if (confirm("Are You sure to delete this record")) {
             $.uiLock('');


             $.ajax({
                 type: "POST",
                 data: '{"DiscountId":"' + DiscountId + '"}',
                 url: "managediscounts.aspx/Delete",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     if (obj.status == -10) {
                         alert("You don't have permission to perform this action..Consult Admin Department.");
                         return;
                     }

                     BindGrid();
                     ResetControls();
                     alert("Record is Deleted successfully.");




                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {
                     $.uiUnlock();
                 }
             });


         }


     }
     );

                }

            }

        }

     


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );

      


        $("#chkFullDay").change(function () {


            if ($('#chkFullDay').prop('checked') == true) {

                $("#txtStartTime").val("12:00am").attr('disabled', 'disabled');
                $("#txtEndTime").val("11:30pm").attr('disabled', 'disabled');
            }
            else {

                $("#txtStartTime").val("").removeAttr('disabled');
                $("#txtEndTime").val("").removeAttr('disabled');
            }
        });



    });
   

          </script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Discounts</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Discount</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" ">
                     
                      <tr><td class="headings" align="left" style="text-align:left">Discount Type:</td><td align="left" style="text-align:left"> <select id="ddlDiscountType" style="width:170px;height:35px"  class="validate ddlrequired" >
                     <option value="0">--Choose Option--</option>
                     <option value="CompanyDiscount">Company Discount</option>
                     <option value="GroupDiscount">Group Discount</option>
                     <option value="SubGroupDiscount">SubGroup Discount</option>
                     <option value="ItemDiscount">Item Discount</option></select></td></tr>
                     
                      <tr><td class="headings" align="left" style="text-align:left">Choose:</td><td align="left" style="text-align:left"> 
                   <select id="ddlItems">
                                             <option value=""></option>
                                             </select>


                    
 

</div>
                      
                      <select  style="width:170px;height:35px" ID ="ddlDiscountOn" ></select>
                      
    </td></tr>
                      <tr><td class="headings" align="left" style="text-align:left">Discount(%):</td><td align="left" style="text-align:left">  <input type="text"  name="txtDiscountPer" class="form-control validate required valNumber "  data-index="1" id="txtDiscountPer" style="width: 170px"/></td><td class="headings" align="left" style="text-align:left">Full Day Discount</td><td align="left" style="text-align:left"><input type="checkbox" id="chkFullDay" data-index="2"  name="chkFullDay" /></td></tr>
                      <tr><td class="headings" align="left" style="text-align:left">Start Date:</td><td align="left" style="text-align:left"><input type="text"  name="txtStartDate" class="form-control validate required"  data-index="1" id="txtStartDate" PlaceHolder="Click Here to pick date" style="width: 170px"/></td><td class="headings" align="left" style="text-align:left">End Date</td><td><input type="text"  name="txtEndDate" class="form-control validate required"  data-index="1" id="txtEndDate" PlaceHolder="Click Here to pick date" style="width: 170px"/></td></tr>
                      <tr><td class="headings" align="left" style="text-align:left">Start Time:</td><td align="left" style="text-align:left"><input type="text"  name="txtStartTime" class="form-control validate required"  data-index="1" id="txtStartTime" PlaceHolder="Click Here to pick Time" style="width: 170px"/></td><td class="headings" align="left" style="text-align:left">End Time</td><td align="left" style="text-align:left"><input type="text"  name="txtEndTime" class="form-control validate required " PlaceHolder="Click Here to pick Time"  data-index="1" id="txtEndTime" style="width:170px"/></td></tr>
                       <tr style="display:none"><td class="headings"><label id = "lbldiscount" style="display:none">Discount Amt:</label> </td><td>  <input type="text"  name="txtDiscountAmt" class="form-control validate valNumber " value="0" data-index="1" id="txtDiscountAmt" style="width:150px;display:none"/></td></tr>
                                           
                      
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>Add</div></td>
                                            <td><div id="btnUpdate"   class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i>Update</div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" >

<i class="fa fa-trash m-right-xs"></i>Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Discounts</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>

     <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete" style="display:none;" class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i>Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

            </div>


 
</form>


<script type="text/javascript">
                function BindGrid(Group) {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageDisocunts.ashx?DiscountType=' + DiscountType,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Discount Id', 'Description','Category','StartDate','EndDate','StartTime','EndTime','FullDay','Discont(%)','DiscountAmount','CompanyId','GroupId','SubgroupId','ItemId'],
                        colModel: [
                                    { name: 'Discount_ID', key: true, index: 'Discount_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Description', index: 'Description', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Category', index: 'Category', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'strSD', index: 'strSD', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'strED', index: 'strED', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'START_TIME', index: 'START_TIME', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'END_TIME', index: 'END_TIME', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'FULLDAY', index: 'FULLDAY', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'Discount_Per', index: 'Discount_Per', width: 200, stype: 'text', sortable: true,hidden:true, editable: true, editrules: { required: true } },
                                    { name: 'Discount_Amt', index: 'Discount_Amt', width: 200, stype: 'text', sortable: true,hidden:true, editable: true, editrules: { required: true } },
                       
                       
                        { name: 'Company_ID', index: 'Company_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                   { name: 'Group_ID', index: 'Group_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                   { name: 'SGroup_ID', index: 'SGroup_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                     { name: 'Item_ID', index: 'Item_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  



                       
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Discount_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "SubGroups List",

                        editurl: 'handlers/ManageDisocunts.ashx',



                     ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_DiscountID = 0;
                    validateForm("detach");
                   

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                      $("#btnUpdate").css({ "display": "none" });
                      $("#btnReset").css({ "display": "none" });
                      $("#btnAdd").css({ "display": "none" });
                  

                      for (var i = 0; i < arrRole.length; i++) {

                          if (arrRole[i] == 1) {

                              $("#btnAdd").css({ "display": "block" });
                          }

                          if (arrRole[i] == 3) {
                              m_DiscountID = $('#jQGridDemo').jqGrid('getCell', rowid, 'Discount_ID');


                              var Category = $('#jQGridDemo').jqGrid('getCell', rowid, 'Category');
                              $('#ddlDiscountType option[value=' + Category + ']').prop('selected', 'selected');

                              if (Category == "CompanyDiscount") {
                                  $('#ddlDiscountOn option[value=' + $('#jQGridDemo').jqGrid('getCell', rowid, 'Company_ID') + ']').prop('selected', 'selected');
                              }
                              else if (Category == "GroupDiscount") {
                                  $('#ddlDiscountOn option[value=' + $('#jQGridDemo').jqGrid('getCell', rowid, 'Group_ID') + ']').prop('selected', 'selected');
                              }
                              else if (Category == "SubGroupDiscount") {
                                  $('#ddlDiscountOn option[value=' + $('#jQGridDemo').jqGrid('getCell', rowid, 'SGroup_ID') + ']').prop('selected', 'selected');
                              }

                              else if (Category == "ItemDiscount") {


                                  $("#txtddlItems").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));
                                  $("#ddlItems").html("<option selected=selected value='" + $('#jQGridDemo').jqGrid('getCell', rowid, 'Item_ID') + "'>" + $('#jQGridDemo').jqGrid('getCell', rowid, 'Description') + "</option>");



                              }


                              $("#txtDiscountPer").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Discount_Per'));


                              var fullday = $('#jQGridDemo').jqGrid('getCell', rowid, 'FULLDAY');

                              if (fullday == "true") {
                                  $('#chkFullDay').prop('checked', true);
                              }
                              else {
                                  $('#chkFullDay').prop('checked', false);
                              }

                              $("#txtStartDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strSD'));
                              $("#txtEndDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strED'));
                              $("#txtStartTime").val($('#jQGridDemo').jqGrid('getCell', rowid, 'START_TIME'));
                              $("#txtEndTime").val($('#jQGridDemo').jqGrid('getCell', rowid, 'END_TIME'));

                              $("#btnAdd").css({ "display": "none" });
                              $("#btnUpdate").css({ "display": "block" });
                              $("#btnReset").css({ "display": "block" });
                          }

                      }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }



       $(document).ready(
       function()
       {
         $("#dvOuter_ddlItems").css({ "display": "none" });
       }
       );

    </script>
</asp:Content>

