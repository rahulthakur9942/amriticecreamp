﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_managebillgridoptions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBranches();
        }
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BILLSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }


    void BindBranches()
    {

        ddlBGBranch.DataSource = new BranchBLL().GetAll();
        ddlBGBranch.DataValueField = "BranchId";
        ddlBGBranch.DataTextField = "BranchName";
        ddlBGBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBGBranch.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string FillGridSettings(string Type,int Branch)
    {
        BillGridOptions ObjSettings = new BillGridOptions() {Type=Type };
       

        ObjSettings.BranchId = Branch;
        new BillGridOptionsBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string InsertGridOptions(BillGridOptions objSettings)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
      
        objSettings.UserId = Id;
        
        int status = new BillGridOptionsBLL().UpdateBasicSettings(objSettings);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}