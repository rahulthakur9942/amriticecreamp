﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintBill.aspx.cs" Inherits="PrintBill" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Libre+Barcode+128:400' rel='stylesheet' type='text/css'>
</head>
<body>

    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic);
      @import url(https://fonts.googleapis.com/css?family=Libre+Barcode+128:400);
        body {background-color: #e4e4e4; font-family: 'Roboto', sans-serif;}
        .print_bill_main {width: 259px; margin: 0 auto;padding: 10px 15px; background-color: #fff;}
        .top_header p {text-align: center; font-size: 12px; font-weight: 600;}
        .table_one {width: 100%; border-bottom: 2px dashed; padding: 10px 0px;}
        .table_one td {font-size: 12px; color: #000;}
        .table_one td:first-child {text-align: left;}
        .table_one td:last-child {text-align: right;}
        .table_two {border-bottom: 2px dashed; margin: 0px 0 0;width: 100%;}
        .table_two th {text-align: center; font-size: 12px; border-bottom: 2px dashed; padding: 6px 5px;}
        .table_two td {font-size: 12px; text-align: center;padding: 6px 5px;}
        .table_three {padding: 0;}
        .table_three td {padding: 5px 0;}
        /*.table_three tr:nth-child(5) td {font-size: 14px; border-top: 2px dashed; border-bottom: 2px dashed; padding: 6px 0;}
        .table_three tr:nth-child(6) td {font-size: 14px; padding: 6px 0;}*/
        .order_net_amount td{font-size: 14px; border-top: 2px dashed; padding: 6px 0;}
        .tax_table {width: 100%; padding: 0px; border: 1px solid; margin: 15px 0 0;}
        .tax_table td {text-align: center; font-size: 12px;}
        .tax_table tr:first-child td {border-right: 1px solid; padding: 4px 4px; border-bottom: 1px solid;}
        .tax_table tr:first-child td:last-child{border-right:none}
        .tax_table tr:last-child td {padding: 0px;}
        .tax_table tr:last-child td:last-child{border-right:none}
        .tax_table p {margin: 0px; border-right: 1px solid #000; padding: 4px 0;}
        .tax_table td:last-child p {border: none !important;}
        .tax_table p span {margin: 0 0 0 4px; padding: 4px 0 4px 4px; border-left: 1px solid;}
    </style>

    <form id="form1" runat="server">
    <div class="print_bill_main" id="staticPrintDiv">
    
        <div class="top_header">
            <p runat="server" id="header1"></p>
               <p runat="server" id="header2"></p>
               <p runat="server" id="header3"></p>
                 <p runat="server" id="header4"></p>
              <p runat="server" id="header5"></p>
        </div>

        <table class="table_one">
            <tbody>
                <tr>
                    <td>T.No: <span runat="server" id="Tableno"></span></td>
                    <td runat="server" id="Remarks"></td>
                    <td runat="server" id="Billmode"></td>
                </tr>
                <tr>
                    <td>Bill No: <span runat="server" id="BillNo"></span></td>
                    <td></td>
                    <td>Date:<span runat="server" id="BillDate"></span></td>
                </tr>
                <tr>
                    <td>User: <span runat="server" id="UserName"></span></td>
                    <td></td>
                    <td>Time: <span runat="server" id="BillTime"></span></td>
                </tr>
            </tbody>
        </table>

        <table class="table_two">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Qty</th>
                    <th>Rate</th>
                    <th>Amt</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="RtpItemList" runat="server">
                    <ItemTemplate>
                <tr>
                    <td><%#Eval("Item_name") %></td>
                    <td><%#Eval("Qty") %></td>
                    <td><%#Eval("Rate") %></td>
                    <td><%#Eval("Amount") %></td>
                </tr>
                        </ItemTemplate>
      </asp:Repeater>
            </tbody>
        </table>

        <table class="table_one table_three">
            <tbody>
                <tr>
                    <td><b>No Of Items:</b></td>
                    <td></td>
                    <td runat="server" id="NoOfItems"></td>
                </tr>
                <tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td  runat="server" id="Billvalue"></td>
                </tr>
                <tr runat="server" id="trdiscount" visible="false">
                    <td><b>Discount:</b></td>
                    <td><span runat="server" id="DiscountPer"></span>&nbsp;%</td>
                    <td runat="server" id="DiscountAmt"></td>
                </tr>
                <tr>
                    <td><b>Round Off</b></td>
                    <td></td>
                    <td runat="server" id="RoundAmount"></td>
                </tr>
                <tr class="order_net_amount">
                    <td><b>Net Amount:</b></td>
                    <td></td>
                    <td runat="server" id="NetAmount"></td>
                </tr>
                <tr class="order_net_amount">
                    <td><b>Order Through:</b></td>
                    <td></td>
                    <td runat="server" id="Orderthrough"></td>
                </tr>
            </tbody>
        </table>

        <table class="tax_table">
            <tbody>
                <tr>
                    <td>HSN</td>
                    <td>TxbleAmt</td>
                    <td>SGST</td>
                    <td>CGST</td>
                </tr>
                <asp:Repeater ID="RptSubreport" runat="server">
                    <ItemTemplate>
                <tr>
                    <td><p><%#Eval("HSNCode") %></p></td>
                    <td><p><%#Eval("Taxableval") %></p></td>
                    <td><p><%#Eval("SGSTPer") %><span><%#Eval("SGSTAmt") %></span></p></td>
                    <td><p><%#Eval("CGSTPer") %><span><%#Eval("CGSTAmt") %></span></p></td>
                </tr>
                        </ItemTemplate>
                    </asp:Repeater>
            </tbody>
        </table>

        <div class="top_header">
            <p runat="server" id="footer1"></p>
            <p runat="server" id="footer2"></p>
            <p runat="server" id="footer3"></p>
        </div>
        <div runat="server" id="BillNoBarcode"  style="text-align: center;font-family: 'Libre Barcode 128', cursive;font-size: 360%;"></div>

    </div>
    </form>
</body>
        <script type="text/javascript">
            printDiv("staticPrintDiv");
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = originalContents;
            window.print();

        }
    </script>
</html>
