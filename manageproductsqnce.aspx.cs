﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class manageproductsqnce : System.Web.UI.Page
{
    managesqence ms = new managesqence();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {

            bind_dd_items();
        }
    }
    protected void bindlist(int prop_id)
    {
        ms.req = "bindlist_group";
        ms.PROP_ID = prop_id;
        DataTable dt = ms.bind_item_dd();
        rpt_itemlist.DataSource = dt;
        rpt_itemlist.DataBind();
        if (dt.Rows.Count > 0)
        {

            btnsave.Visible = true;
        }

    }
    public void bind_dd_items()
    {
        ms.req = "bind_dd_item";
        DataTable dt = ms.bind_item_dd();
        dd_group.DataSource = dt;
        dd_group.DataTextField = "prop_name";
        dd_group.DataValueField = "prop_id";
        dd_group.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select Group-", "0");
        dd_group.Items.Insert(0, listItem1);

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
     
        foreach (RepeaterItem item in rpt_itemlist.Items)
        {
            int prodctid = Convert.ToInt32(((Label)item.FindControl("lblproductid")).Text);
            string lvl = Convert.ToString(((System.Web.UI.HtmlControls.HtmlInputControl)item.FindControl("tblvl")).Value);
            if (!string.IsNullOrEmpty(lvl)) {
                ms.req = "update_item";
            ms.itemid = prodctid;
            ms.lvl = Convert.ToInt32(lvl);
            ms.insert_update_sequence();
            }


        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);
    }

    protected void dd_group_SelectedIndexChanged(object sender, EventArgs e)
    {
        string ddval = dd_group.SelectedItem.Value;
        if (ddval == "0")
        {
            btnsave.Visible = false;

        }
        else
        {

            bindlist(Convert.ToInt32(dd_group.SelectedValue));
        }
    }
}