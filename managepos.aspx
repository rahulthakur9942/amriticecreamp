﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managepos.aspx.cs" Inherits="managepos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        th {
            text-align:center;
        }
            .table table  tbody  tr  td a ,
.table table  tbody  tr  td  span {
position: relative;
float: left;
padding: 6px 12px;
margin-left: -1px;
line-height: 1.42857143;
color: #337ab7;
text-decoration: none;
background-color: #fff;
border: 1px solid #ddd;
}

.table table > tbody > tr > td > span {
z-index: 3;
color: #fff;
cursor: default;
background-color: #337ab7;
border-color: #337ab7;
}

.table table > tbody > tr > td:first-child > a,
.table table > tbody > tr > td:first-child > span {
margin-left: 0;
border-top-left-radius: 4px;
border-bottom-left-radius: 4px;
}

.table table > tbody > tr > td:last-child > a,
.table table > tbody > tr > td:last-child > span {
border-top-right-radius: 4px;
border-bottom-right-radius: 4px;
}

.table table > tbody > tr > td > a:hover,
.table   table > tbody > tr > td > span:hover,
.table table > tbody > tr > td > a:focus,
.table table > tbody > tr > td > span:focus {
z-index: 2;
color: #23527c;
background-color: #eee;
border-color: #ddd;
}
    </style>
     <form   runat="server" id="formID" method="post">
         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="updateProgress" runat="server">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
                                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                                        ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>POS</h3>
                        </div>
              <%--          <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>--%>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit POS</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                              
                             <tr>
                  <td class="headings">Title:</td>
                                 <td> <input type="text" required  name="tbName" class="form-control validate required alphanumeric" runat="server"  data-index="1" id="tbtitle" style="width: 213px"/></td>

                             </tr>
                     <tr><td class="headings">Status:</td><td>



                         <asp:DropDownList ID="ddstatus" class="form-control" runat="server" style="height: 30px;">

                             <asp:ListItem Value="Y">Active</asp:ListItem>
                               <asp:ListItem Value="N">Deactive</asp:ListItem>
                         </asp:DropDownList>
                                                          </td></tr>
                         
 
                                   <tr>
                                        <td></td>
                                       <td>
                                           <asp:Button ID="btnsave"  class="btn btn-primary btn-small" runat="server" Text="Submit" OnClick="btnsave_Click" />

                                       </td>
                                   </tr>
             

                     </table>  
                       
                 
                          
                             

                        </div>
           
                    </div>


     <div class="x_panel">

                        <div class="x_content">

                               <div class="youhave"  >

             <div class="col-md-4">
                        <asp:GridView ID="gv_display" runat="server" OnPageIndexChanging="OnPaging"
                                EnableModelValidation="True" AllowPaging="True"
                                PagerSettings-PageButtonCount="10" PagerStyle-HorizontalAlign="Right" OnRowCommand="gv_display_OnRowCommand" AutoGenerateColumns="false" 
                            class="table table-striped table-bordered table-hover" PageSize="13">

                                <Columns>
                                    <asp:BoundField DataField="title" HeaderText="Title" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                          <asp:BoundField DataField="status" HeaderText="Is Active" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
               
                                       <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lnkedit" runat="server" CommandName="select" CommandArgument='<%#Eval("posid") %>'><i class="fa fa-edit" style="font-size:20px;color:red"></i></asp:LinkButton>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                <%--    <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lnkdel" runat="server"  CommandName="del" CommandArgument='<%#Eval("posid") %>'><i class="material-icons">delete_forever</i></asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>--%>
                                    </Columns>


                        </asp:GridView></div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
          <%--     <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>--%>
                <!-- /footer content -->

            </div>

</ContentTemplate>
                     </asp:UpdatePanel>
         </form>
</asp:Content>

