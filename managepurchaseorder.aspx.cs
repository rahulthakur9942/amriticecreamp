﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Data;
using System.Collections;

public partial class backoffice_managepurchaseorder : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropDownList();
            hdntodaydate.Value = DateTime.Now.ToShortDateString();
            ddlSupplier.DataSource = new PurchaseBLL().GetAllSupplier();
            ddlSupplier.DataTextField = "CName";
            ddlSupplier.DataValueField = "CCode";
            ddlSupplier.DataBind();
            ListItem li = new ListItem();
            li.Text = "--SELECT--";
            li.Value = "0";
            ddlSupplier.Items.Insert(0, li);
            ddlGodown.DataSource = new GodownsBLL().GetAll();
            ddlGodown.DataTextField = "Godown_Name";
            ddlGodown.DataValueField = "Godown_Id";
            ddlGodown.DataBind();
            ddlGodown.Items.Insert(0, li);
            

                Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
                string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

                if (strDate == "")
                {

                    Response.Redirect("index.aspx?DayOpen=Close");
                }

            
        }

        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASEORDER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    public void BindDropDownList()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new ProductBLL().GetPurchaseOptions2(hdnProducts, Branch);
    }


    //[WebMethod]
    //public static string BindControls()
    //{
    //    string supplierData = new SupplierBLL().GetSupplierOptions();
    //    string godownData = new GodownsBLL().GetOptions();

    //    JavaScriptSerializer ser = new JavaScriptSerializer();


    //    var JsonData = new
    //    {
    //        SupplierOptions = supplierData,
    //        GodownOptions = godownData,


    //    };
    //    return ser.Serialize(JsonData);
    //}

    [WebMethod]
    public static string BindProducts()
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string productData = new ProductBLL().GetPurchaseOptions(Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ProductOptions = productData,


        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Insert(string OrderDate, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer,decimal dis3per,decimal dis3amt,bool dis3afterdis1dis2,decimal dis1amt, decimal dis2amt,decimal billvalue,decimal taxp,decimal taxamt, string codeArr, string qtyArr, string amountArr,
        string freeArr, string dis1Arr, string dis2Arr,string dis3Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr,string taxaamtarr
        )
    {


        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASEORDER));

        string[] arrRoles = sesRoles.Split(',');


       
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }


        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);

        PurchaseOrder objPurchaseOrder = new PurchaseOrder()
        {

            OrderDate = Convert.ToDateTime(OrderDate),
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3P = dis3per,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2  = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            TaxAmt = taxamt,
            BranchId = Branch,
            UserNo = UserNo,


        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');
        string[] freeData = freeArr.Split(',');
        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] dis3Data = dis3Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ProductId");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");

        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("TaxP");

        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");
      

        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["OrderNo"] = Convert.ToInt16(1);
            dr["ProductId"] = Convert.ToInt16(pidData[i]);
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = Convert.ToDecimal(dis3Data[i]);
            dr["TaxP"] = Convert.ToDecimal(taxData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);

            dr["Free"] = Convert.ToDecimal(freeData[i]);
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);


            dt.Rows.Add(dr);

        }

        status = new PurchaseOrderBLL().InsertUpdate(objPurchaseOrder, dt);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            PurchaseOrder = objPurchaseOrder

        };
        return ser.Serialize(JsonData);
    }




    [WebMethod]
    public static string Update(int OrderNo, string OrderDate, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer, decimal dis3per, decimal dis3amt, bool dis3afterdis1dis2, decimal dis1amt, decimal dis2amt, decimal billvalue, decimal taxp, decimal taxamt, string codeArr, string qtyArr, string amountArr,
        string freeArr, string dis1Arr, string dis2Arr, string dis3Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr, string taxaamtarr
        )
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASEORDER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -11;
        }
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        PurchaseOrder objPurchaseOrderOrder = new PurchaseOrder()
        {
            OrderNo = Convert.ToInt32(OrderNo),
            OrderDate = Convert.ToDateTime(OrderDate),
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3P = dis3per,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2 = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            TaxAmt = taxamt,
            BranchId = Branch,
            UserNo = UserNo,



        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');
        string[] freeData = freeArr.Split(',');
        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] dis3Data = dis3Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ProductId");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");

        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("TaxP");

        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");



        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["OrderNo"] = Convert.ToInt16(1);
            dr["ProductId"] = Convert.ToInt16(pidData[i]);
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = Convert.ToDecimal(dis3Data[i]);
            dr["TaxP"] = Convert.ToDecimal(amountData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);

            dr["Free"] = Convert.ToDecimal(freeData[i]);
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);

            dt.Rows.Add(dr);

        }

        status = new PurchaseOrderBLL().InsertUpdate(objPurchaseOrderOrder, dt);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            PurchaseOrder = objPurchaseOrderOrder

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindPurchaseOrderDetail(int pid)
    {
        int cntr = 0;
        string serviceData = new PurchaseOrderBLL().GetPurchaseOrderDetailOptions(pid, out cntr);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = serviceData,
            Counter = cntr

        };
        return ser.Serialize(JsonData);
    }


    //[WebMethod]
    //public static string BindServices()
    //{
    //    string serviceData = new ServicesBLL().GetActiveOptions();


    //    JavaScriptSerializer ser = new JavaScriptSerializer();
    //    ser.MaxJsonLength = int.MaxValue;

    //    var JsonData = new
    //    {
    //        ServiceOptions = serviceData,


    //    };
    //    return ser.Serialize(JsonData);
    //}


    //[WebMethod]
    //public static string InsertSupplier(string Supplier, string Address, string Address1, int City, int State, string ContactNumber, string Contactperson, string CSTNo, string TINNo, bool isActive)
    //{

    //    Suppliers objSupplier = new Suppliers()
    //    {

    //        Supplier = Supplier.Trim(),
    //        Address = Address.Trim(),
    //        Address1 = Address1.Trim(),
    //        City = City,
    //        State = State,
    //        ContactNumber = ContactNumber,
    //        ContactPerson = Contactperson,
    //        CSTNo = CSTNo,
    //        TINNo = TINNo,
    //        IsActive = isActive

    //    };
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    int status = new SupplierBLL().InsertUpdate(objSupplier);
    //    var JsonData = new
    //    {
    //        Supplier = objSupplier,
    //        Status = status
    //    };
    //    return ser.Serialize(JsonData);

    //}


    //[WebMethod]
    //public static string InsertGodown(string title)
    //{

    //    Godowns objGodowns = new Godowns()
    //    {
    //        Title = title.Trim()

    //    };
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    int status = new GodownsBLL().InsertUpdate(objGodowns);
    //    var JsonData = new
    //    {
    //        Godown = objGodowns,
    //        Status = status
    //    };
    //    return ser.Serialize(JsonData);

    //}
}