﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_StockAdjustment : System.Web.UI.Page
{
    public Int32 RefNo { get { return Request.QueryString["RefNo"] != null ? Convert.ToInt32(Request.QueryString["RefNo"]) : 0; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        Int16 BranchId = Convert.ToInt16(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        RptStockAdjustment objBreakageExpiry = new RptStockAdjustment(RefNo, BranchId);
        ReportViewer1.Report = objBreakageExpiry;
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}