﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for rptRetailBill
/// </summary>
public class rptRetailBill : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
	private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private XRTable xrTable1;
    private XRTableRow xrTableRow2;
    private XRTableCell clOrderID;
    private XRTableCell clCustomerID;
    private XRTableCell clSalesperson;
    private XRTableCell clOrderDate;
    private XRLabel xrLabel31;
    private XRLabel xrLabel30;
    private PageHeaderBand PageHeader;
    private XRLabel xrLabel14;
    private XRLabel xrLabel15;
    private XRLabel xrLabel2;
    private XRLabel xrLabel25;
    private XRLabel xrLabel3;
    private XRLabel xrLabel11;
    private GroupHeaderBand GroupHeader1;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell4;
    private XRLine xrLine1;
    private XRLabel xrLabel12;
    private XRLabel xrLabel23;
    private XRLabel xrLabel6;
    private XRLabel xrLabel28;
    private XRLabel xrLabel27;
    private XRLabel xrLabel13;
    private XRLabel lbShipAddress;
    private XRLabel lbBillTo;
    private XRLabel xrLabel21;
    private XRLabel lbCountry;
    private GroupFooterBand GroupFooter1;
    private XRLabel xrLabel33;
    private XRLabel xrLabel7;
    private XRLabel xrLabel29;
    private XRLabel xrLabel32;
    private XRLabel xrLabel18;
    private XRLabel xrLabel1;
    private XRLabel xrLabel4;
    private XRLabel xrLabel34;
    private XRLabel xrLabel35;
    private XRLabel xrLabel16;
    private XRLabel xrLabel17;
    private XRLabel xrLabel26;
    private XRLabel xrLabel24;
    private XRLabel xrLabel19;
    private XRLabel xrLabel8;
    private XRLabel xrLabel22;
    private XRLabel xrLabel10;
    private XRLabel xrLabel5;
    private XRLine xrLine2;
    private XRLine xrLine5;
    private XRLine xrLine4;
    private XRLine xrLine3;
    private XRTable xrTable2;
    private XRTableRow xrTableRow20;
    private XRTableCell lblFooter1;
    private XRTableRow xrTableRow21;
    private XRTableCell lblFooter2;
    private XRTableRow xrTableRow22;
    private XRTableCell lblFooter3;
    private XRTableRow xrTableRow23;
    private XRTableCell lblFooter4;
    private XRTableRow xrTableRow24;
    private XRTableCell lblFooter5;
    private XRLabel xrLabel9;
    private XRLabel xrLabel20;
    private XRTableCell xrTableCell6;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell7;
    private XRLine xrLine6;
    private XRTableCell xrTableCell8;
    private XRLine xrLine7;
    private XRTableCell xrTableCell9;
    private XRLine xrLine8;
    private XRTableCell xrTableCell10;
    private XRLine xrLine9;
    private XRTableCell xrTableCell11;
    private XRLine xrLine10;
    private XRTableCell xrTableCell12;
    private XRLine xrLine11;
    private XRTableCell xrTableCell13;
    private XRLine xrLine12;
    private XRTableCell xrTableCell14;
    private XRLine xrLine13;
    private XRLine xrLine14;
    private RetailBillDataSet retailBillDataSet1;
    private RetailBillDataSetTableAdapters.pos_sp_retailbillreportTableAdapter pos_sp_retailbillreportTableAdapter1;
    private XRLabel xrLabel36;
    private RetailBillDataSet retailBillDataSet2;
    private XRLabel xrLabel37;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

    public rptRetailBill(string BillNowPrefix)
	{
        Connection con = new Connection();
        InitializeComponent();
        pos_sp_retailbillreportTableAdapter1 .Connection = new System.Data.SqlClient.SqlConnection(con.sqlDataString);
        pos_sp_retailbillreportTableAdapter1.Fill(retailBillDataSet2.pos_sp_retailbillreport, BillNowPrefix);
        retailBillDataSet2.EnforceConstraints = false;
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "rptRetailBill.resx";
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
        this.clOrderID = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
        this.clCustomerID = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
        this.clSalesperson = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
        this.clOrderDate = new DevExpress.XtraReports.UI.XRTableCell();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.lbShipAddress = new DevExpress.XtraReports.UI.XRLabel();
        this.lbBillTo = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.lbCountry = new DevExpress.XtraReports.UI.XRLabel();
        this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.retailBillDataSet1 = new RetailBillDataSet();
        this.pos_sp_retailbillreportTableAdapter1 = new RetailBillDataSetTableAdapters.pos_sp_retailbillreportTableAdapter();
        this.retailBillDataSet2 = new RetailBillDataSet();
        this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.retailBillDataSet1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.retailBillDataSet2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
        this.Detail.HeightF = 15.70834F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 5, 5, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable1
        // 
        this.xrTable1.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(2.083309F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable1.SizeF = new System.Drawing.SizeF(267.9167F, 15.70834F);
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
        this.xrTableRow2.BorderColor = System.Drawing.Color.White;
        this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell11,
            this.clOrderID,
            this.xrTableCell12,
            this.clCustomerID,
            this.xrTableCell13,
            this.clSalesperson,
            this.xrTableCell14,
            this.clOrderDate});
        this.xrTableRow2.Font = new System.Drawing.Font("Tahoma", 8.25F);
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow2.Weight = 0.25842696629213485D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Bill_No")});
        this.xrTableCell6.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.StylePriority.UseFont = false;
        this.xrTableCell6.StylePriority.UseTextAlignment = false;
        xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell6.Summary = xrSummary1;
        this.xrTableCell6.Text = "xrTableCell6";
        this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell6.Weight = 0.039266800549962462D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine10});
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.Text = "xrTableCell11";
        this.xrTableCell11.Weight = 0.0042016282384442268D;
        // 
        // xrLine10
        // 
        this.xrLine10.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(2.145767E-06F, 0F);
        this.xrLine10.Name = "xrLine10";
        this.xrLine10.SizeF = new System.Drawing.SizeF(2.083332F, 15.70834F);
        // 
        // clOrderID
        // 
        this.clOrderID.BackColor = System.Drawing.Color.White;
        this.clOrderID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Item_Name")});
        this.clOrderID.Font = new System.Drawing.Font("Calibri", 8F);
        this.clOrderID.Name = "clOrderID";
        this.clOrderID.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clOrderID.StylePriority.UseBackColor = false;
        this.clOrderID.StylePriority.UseFont = false;
        this.clOrderID.StylePriority.UsePadding = false;
        this.clOrderID.StylePriority.UseTextAlignment = false;
        this.clOrderID.Text = "clOrderID";
        this.clOrderID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.clOrderID.Weight = 0.23388129247643119D;
        // 
        // xrTableCell12
        // 
        this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine11});
        this.xrTableCell12.Name = "xrTableCell12";
        this.xrTableCell12.Text = "xrTableCell12";
        this.xrTableCell12.Weight = 0.0042793505789222412D;
        // 
        // xrLine11
        // 
        this.xrLine11.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(4.053116E-06F, 0F);
        this.xrLine11.Name = "xrLine11";
        this.xrLine11.SizeF = new System.Drawing.SizeF(2.166656F, 15.70834F);
        // 
        // clCustomerID
        // 
        this.clCustomerID.BackColor = System.Drawing.Color.White;
        this.clCustomerID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Qty", "{0:.000}")});
        this.clCustomerID.Font = new System.Drawing.Font("Calibri", 8F);
        this.clCustomerID.Name = "clCustomerID";
        this.clCustomerID.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clCustomerID.StylePriority.UseBackColor = false;
        this.clCustomerID.StylePriority.UseFont = false;
        this.clCustomerID.StylePriority.UsePadding = false;
        this.clCustomerID.StylePriority.UseTextAlignment = false;
        this.clCustomerID.Text = "clCustomerID";
        this.clCustomerID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clCustomerID.Weight = 0.0718035445569727D;
        // 
        // xrTableCell13
        // 
        this.xrTableCell13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine12});
        this.xrTableCell13.Name = "xrTableCell13";
        this.xrTableCell13.Text = "xrTableCell13";
        this.xrTableCell13.Weight = 0.011822450540657919D;
        // 
        // xrLine12
        // 
        this.xrLine12.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(1.988932F, 0F);
        this.xrLine12.Name = "xrLine12";
        this.xrLine12.SizeF = new System.Drawing.SizeF(3.338898F, 15.70834F);
        // 
        // clSalesperson
        // 
        this.clSalesperson.BackColor = System.Drawing.Color.White;
        this.clSalesperson.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Rate")});
        this.clSalesperson.Font = new System.Drawing.Font("Calibri", 8F);
        this.clSalesperson.Name = "clSalesperson";
        this.clSalesperson.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clSalesperson.StylePriority.UseBackColor = false;
        this.clSalesperson.StylePriority.UseFont = false;
        this.clSalesperson.StylePriority.UsePadding = false;
        this.clSalesperson.StylePriority.UseTextAlignment = false;
        this.clSalesperson.Text = "clSalesperson";
        this.clSalesperson.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clSalesperson.Weight = 0.081787502749700786D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine13});
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.Text = "xrTableCell14";
        this.xrTableCell14.Weight = 0.0084935280620486563D;
        // 
        // xrLine13
        // 
        this.xrLine13.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(4.053116E-06F, 0F);
        this.xrLine13.Name = "xrLine13";
        this.xrLine13.SizeF = new System.Drawing.SizeF(4.042999F, 15.70834F);
        // 
        // clOrderDate
        // 
        this.clOrderDate.BackColor = System.Drawing.Color.White;
        this.clOrderDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Amount", "{0:#.00}")});
        this.clOrderDate.Font = new System.Drawing.Font("Calibri", 8F);
        this.clOrderDate.Name = "clOrderDate";
        this.clOrderDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clOrderDate.StylePriority.UseBackColor = false;
        this.clOrderDate.StylePriority.UseFont = false;
        this.clOrderDate.StylePriority.UsePadding = false;
        this.clOrderDate.StylePriority.UseTextAlignment = false;
        this.clOrderDate.Text = "clOrderDate";
        this.clOrderDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clOrderDate.Weight = 0.0947896523400099D;
        // 
        // TopMargin
        // 
        this.TopMargin.HeightF = 1F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 180F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLine5
        // 
        this.xrLine5.LineWidth = 3;
        this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(2.083309F, 206.917F);
        this.xrLine5.Name = "xrLine5";
        this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine5.SizeF = new System.Drawing.SizeF(258.3333F, 8F);
        // 
        // xrLabel31
        // 
        this.xrLabel31.BackColor = System.Drawing.Color.White;
        this.xrLabel31.BorderColor = System.Drawing.Color.White;
        this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Net_Amount")});
        this.xrLabel31.Font = new System.Drawing.Font("Tahoma", 10F);
        this.xrLabel31.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(158.091F, 180.6253F);
        this.xrLabel31.Name = "xrLabel31";
        this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel31.SizeF = new System.Drawing.SizeF(105.3174F, 18.29169F);
        this.xrLabel31.StylePriority.UseBackColor = false;
        this.xrLabel31.StylePriority.UseFont = false;
        this.xrLabel31.StylePriority.UsePadding = false;
        this.xrLabel31.StylePriority.UseTextAlignment = false;
        this.xrLabel31.Text = "xrLabel31";
        this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel30
        // 
        this.xrLabel30.BackColor = System.Drawing.Color.White;
        this.xrLabel30.BorderColor = System.Drawing.Color.White;
        this.xrLabel30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel30.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel30.ForeColor = System.Drawing.Color.Black;
        this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(49.59785F, 180.6253F);
        this.xrLabel30.Name = "xrLabel30";
        this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel30.SizeF = new System.Drawing.SizeF(108.4931F, 18.29169F);
        this.xrLabel30.StylePriority.UseBackColor = false;
        this.xrLabel30.StylePriority.UseFont = false;
        this.xrLabel30.Text = "Net Amount:";
        this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel14,
            this.xrLabel15,
            this.xrLabel2,
            this.xrLabel25,
            this.xrLabel3,
            this.xrLabel11});
        this.PageHeader.HeightF = 163.125F;
        this.PageHeader.Name = "PageHeader";
        // 
        // xrLabel36
        // 
        this.xrLabel36.BackColor = System.Drawing.Color.White;
        this.xrLabel36.BorderColor = System.Drawing.Color.White;
        this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Printdup")});
        this.xrLabel36.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel36.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(84.80296F, 0F);
        this.xrLabel36.Name = "xrLabel36";
        this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel36.SizeF = new System.Drawing.SizeF(95.09853F, 20F);
        this.xrLabel36.StylePriority.UseBackColor = false;
        this.xrLabel36.StylePriority.UseFont = false;
        this.xrLabel36.StylePriority.UsePadding = false;
        this.xrLabel36.StylePriority.UseTextAlignment = false;
        this.xrLabel36.Text = "xrLabel36";
        this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel14
        // 
        this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Header4")});
        this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 95.66658F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.Text = "xrLabel14";
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel15
        // 
        this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Header5")});
        this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 118.6666F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(260F, 23.00002F);
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.Text = "[Header5]";
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel2
        // 
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.header1")});
        this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.66666F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.Text = "xrLabel2";
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel25
        // 
        this.xrLabel25.BackColor = System.Drawing.Color.White;
        this.xrLabel25.BorderColor = System.Drawing.Color.White;
        this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Remarks")});
        this.xrLabel25.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel25.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(92.64011F, 143.125F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(75.08363F, 20F);
        this.xrLabel25.StylePriority.UseBackColor = false;
        this.xrLabel25.StylePriority.UseFont = false;
        this.xrLabel25.StylePriority.UsePadding = false;
        this.xrLabel25.StylePriority.UseTextAlignment = false;
        this.xrLabel25.Text = "xrLabel25";
        this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel3
        // 
        this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.header2")});
        this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 49.66665F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel3.StylePriority.UseFont = false;
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        this.xrLabel3.Text = "xrLabel3";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel11
        // 
        this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Header3")});
        this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 72.6666F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel11.StylePriority.UseFont = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.Text = "xrLabel11";
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.xrLabel9,
            this.xrLine2,
            this.xrTable3,
            this.xrLine1,
            this.xrLabel12,
            this.xrLabel23,
            this.xrLabel6,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel13,
            this.lbShipAddress,
            this.lbBillTo,
            this.xrLabel21,
            this.lbCountry});
        this.GroupHeader1.HeightF = 122.8333F;
        this.GroupHeader1.Name = "GroupHeader1";
        // 
        // xrLabel20
        // 
        this.xrLabel20.BackColor = System.Drawing.Color.White;
        this.xrLabel20.BorderColor = System.Drawing.Color.White;
        this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.BillTime")});
        this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel20.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(197.5083F, 21.70827F);
        this.xrLabel20.Name = "xrLabel20";
        this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel20.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel20.SizeF = new System.Drawing.SizeF(63.77623F, 18.29172F);
        this.xrLabel20.StylePriority.UseBackColor = false;
        this.xrLabel20.StylePriority.UseFont = false;
        this.xrLabel20.StylePriority.UseTextAlignment = false;
        this.xrLabel20.Text = "xrLabel20";
        this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel9
        // 
        this.xrLabel9.BackColor = System.Drawing.Color.White;
        this.xrLabel9.BorderColor = System.Drawing.Color.White;
        this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.BillTimeName")});
        this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel9.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(157.0873F, 21.7083F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel9.SizeF = new System.Drawing.SizeF(39.55307F, 18.29172F);
        this.xrLabel9.StylePriority.UseBackColor = false;
        this.xrLabel9.StylePriority.UseFont = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.Text = "xrLabel9";
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLine2
        // 
        this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 114.8333F);
        this.xrLine2.Name = "xrLine2";
        this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine2.SizeF = new System.Drawing.SizeF(270F, 8F);
        // 
        // xrTable3
        // 
        this.xrTable3.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 88.00002F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(270F, 26.83328F);
        this.xrTable3.StylePriority.UseFont = false;
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(167)))), ((int)(((byte)(73)))));
        this.xrTableRow3.BorderColor = System.Drawing.Color.White;
        this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrTableCell1,
            this.xrTableCell8,
            this.xrTableCell2,
            this.xrTableCell9,
            this.xrTableCell3,
            this.xrTableCell10,
            this.xrTableCell4});
        this.xrTableRow3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableRow3.ForeColor = System.Drawing.Color.White;
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow3.StylePriority.UseBackColor = false;
        this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow3.Weight = 0.25842696629213485D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.BackColor = System.Drawing.Color.White;
        this.xrTableCell5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableCell5.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.StylePriority.UseBackColor = false;
        this.xrTableCell5.StylePriority.UseFont = false;
        this.xrTableCell5.StylePriority.UseForeColor = false;
        this.xrTableCell5.StylePriority.UseTextAlignment = false;
        this.xrTableCell5.Text = "No";
        this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell5.Weight = 0.041494233870359609D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.BackColor = System.Drawing.Color.Black;
        this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine6});
        this.xrTableCell7.ForeColor = System.Drawing.Color.White;
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.StylePriority.UseBackColor = false;
        this.xrTableCell7.StylePriority.UseForeColor = false;
        this.xrTableCell7.Text = "xrTableCell7";
        this.xrTableCell7.Weight = 0.00442024202546254D;
        // 
        // xrLine6
        // 
        this.xrLine6.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(5.534344F, 2.288818E-05F);
        this.xrLine6.Name = "xrLine6";
        this.xrLine6.SizeF = new System.Drawing.SizeF(2.083332F, 23F);
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.BackColor = System.Drawing.Color.White;
        this.xrTableCell1.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell1.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell1.StylePriority.UseBackColor = false;
        this.xrTableCell1.StylePriority.UseFont = false;
        this.xrTableCell1.StylePriority.UseForeColor = false;
        this.xrTableCell1.StylePriority.UseTextAlignment = false;
        this.xrTableCell1.Text = "Description";
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell1.Weight = 0.23331068947098022D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.BackColor = System.Drawing.Color.White;
        this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine7});
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.StylePriority.UseBackColor = false;
        this.xrTableCell8.Text = "xrTableCell8";
        this.xrTableCell8.Weight = 0.021613619306261891D;
        // 
        // xrLine7
        // 
        this.xrLine7.ForeColor = System.Drawing.Color.Black;
        this.xrLine7.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(7.916687F, 2.288818E-05F);
        this.xrLine7.Name = "xrLine7";
        this.xrLine7.SizeF = new System.Drawing.SizeF(2.083328F, 26.83326F);
        this.xrLine7.StylePriority.UseForeColor = false;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.BackColor = System.Drawing.Color.White;
        this.xrTableCell2.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell2.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell2.StylePriority.UseBackColor = false;
        this.xrTableCell2.StylePriority.UseFont = false;
        this.xrTableCell2.StylePriority.UseForeColor = false;
        this.xrTableCell2.Text = "Qty";
        this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell2.Weight = 0.066175375831562272D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.BackColor = System.Drawing.Color.White;
        this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine8});
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.StylePriority.UseBackColor = false;
        this.xrTableCell9.Text = "xrTableCell9";
        this.xrTableCell9.Weight = 0.030991346993832963D;
        // 
        // xrLine8
        // 
        this.xrLine8.ForeColor = System.Drawing.Color.Black;
        this.xrLine8.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(6.327769F, 0F);
        this.xrLine8.Name = "xrLine8";
        this.xrLine8.SizeF = new System.Drawing.SizeF(2.083344F, 26.83327F);
        this.xrLine8.StylePriority.UseForeColor = false;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.Color.White;
        this.xrTableCell3.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell3.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseFont = false;
        this.xrTableCell3.StylePriority.UseForeColor = false;
        this.xrTableCell3.Text = "Rate";
        this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell3.Weight = 0.076903878492179931D;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.BackColor = System.Drawing.Color.White;
        this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine9});
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.StylePriority.UseBackColor = false;
        this.xrTableCell10.Text = "xrTableCell10";
        this.xrTableCell10.Weight = 0.0089355571686561171D;
        // 
        // xrLine9
        // 
        this.xrLine9.BackColor = System.Drawing.Color.White;
        this.xrLine9.ForeColor = System.Drawing.Color.Black;
        this.xrLine9.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(1.916671F, 2.288818E-05F);
        this.xrLine9.Name = "xrLine9";
        this.xrLine9.SizeF = new System.Drawing.SizeF(2.126389F, 26.29156F);
        this.xrLine9.StylePriority.UseBackColor = false;
        this.xrLine9.StylePriority.UseForeColor = false;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.BackColor = System.Drawing.Color.White;
        this.xrTableCell4.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell4.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell4.StylePriority.UseBackColor = false;
        this.xrTableCell4.StylePriority.UseFont = false;
        this.xrTableCell4.StylePriority.UseForeColor = false;
        this.xrTableCell4.StylePriority.UseTextAlignment = false;
        this.xrTableCell4.Text = "Amt";
        this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell4.Weight = 0.099722105234107086D;
        // 
        // xrLine1
        // 
        this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 80.00002F);
        this.xrLine1.Name = "xrLine1";
        this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine1.SizeF = new System.Drawing.SizeF(270F, 8.000015F);
        // 
        // xrLabel12
        // 
        this.xrLabel12.BackColor = System.Drawing.Color.White;
        this.xrLabel12.BorderColor = System.Drawing.Color.White;
        this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.NameField")});
        this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel12.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 39.99999F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(7, 0, 0, 0, 100F);
        this.xrLabel12.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel12.SizeF = new System.Drawing.SizeF(47.41644F, 20F);
        this.xrLabel12.StylePriority.UseBackColor = false;
        this.xrLabel12.StylePriority.UseFont = false;
        this.xrLabel12.StylePriority.UsePadding = false;
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.Text = "xrLabel12";
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel23
        // 
        this.xrLabel23.BackColor = System.Drawing.Color.White;
        this.xrLabel23.BorderColor = System.Drawing.Color.White;
        this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.UserName")});
        this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel23.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(47.41643F, 20.00001F);
        this.xrLabel23.Name = "xrLabel23";
        this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel23.SizeF = new System.Drawing.SizeF(108.803F, 20F);
        this.xrLabel23.StylePriority.UseBackColor = false;
        this.xrLabel23.StylePriority.UseFont = false;
        this.xrLabel23.StylePriority.UsePadding = false;
        this.xrLabel23.StylePriority.UseTextAlignment = false;
        this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel6
        // 
        this.xrLabel6.BackColor = System.Drawing.Color.White;
        this.xrLabel6.BorderColor = System.Drawing.Color.White;
        this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.CName")});
        this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel6.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(47.41644F, 39.99999F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel6.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel6.SizeF = new System.Drawing.SizeF(210.5003F, 20F);
        this.xrLabel6.StylePriority.UseBackColor = false;
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UsePadding = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.Text = "xrLabel6";
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel28
        // 
        this.xrLabel28.BackColor = System.Drawing.Color.White;
        this.xrLabel28.BorderColor = System.Drawing.Color.White;
        this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Address")});
        this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel28.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(59.91644F, 60.00001F);
        this.xrLabel28.Multiline = true;
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel28.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel28.SizeF = new System.Drawing.SizeF(198.0003F, 20F);
        this.xrLabel28.StylePriority.UseBackColor = false;
        this.xrLabel28.StylePriority.UseFont = false;
        this.xrLabel28.StylePriority.UsePadding = false;
        this.xrLabel28.StylePriority.UseTextAlignment = false;
        this.xrLabel28.Text = "xrLabel28";
        this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel27
        // 
        this.xrLabel27.BackColor = System.Drawing.Color.White;
        this.xrLabel27.BorderColor = System.Drawing.Color.White;
        this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.AddressField")});
        this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel27.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.00001F);
        this.xrLabel27.Multiline = true;
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(7, 0, 0, 0, 100F);
        this.xrLabel27.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel27.SizeF = new System.Drawing.SizeF(59.91644F, 20F);
        this.xrLabel27.StylePriority.UseBackColor = false;
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.StylePriority.UsePadding = false;
        this.xrLabel27.StylePriority.UseTextAlignment = false;
        this.xrLabel27.Text = "xrLabel27";
        this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel13
        // 
        this.xrLabel13.BackColor = System.Drawing.Color.White;
        this.xrLabel13.BorderColor = System.Drawing.Color.White;
        this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel13.ForeColor = System.Drawing.Color.Black;
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(7, 0, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(47.41644F, 20F);
        this.xrLabel13.StylePriority.UseBackColor = false;
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UsePadding = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.Text = "Bill No:";
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbShipAddress
        // 
        this.lbShipAddress.BackColor = System.Drawing.Color.White;
        this.lbShipAddress.BorderColor = System.Drawing.Color.White;
        this.lbShipAddress.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbShipAddress.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.BillNoWPrefix")});
        this.lbShipAddress.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbShipAddress.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbShipAddress.LocationFloat = new DevExpress.Utils.PointFloat(47.41643F, 0F);
        this.lbShipAddress.Name = "lbShipAddress";
        this.lbShipAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.lbShipAddress.SizeF = new System.Drawing.SizeF(104.9205F, 20F);
        this.lbShipAddress.StylePriority.UseBackColor = false;
        this.lbShipAddress.StylePriority.UseFont = false;
        this.lbShipAddress.StylePriority.UsePadding = false;
        this.lbShipAddress.StylePriority.UseTextAlignment = false;
        this.lbShipAddress.Text = "lbShipAddress";
        this.lbShipAddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbBillTo
        // 
        this.lbBillTo.BackColor = System.Drawing.Color.White;
        this.lbBillTo.BorderColor = System.Drawing.Color.White;
        this.lbBillTo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lbBillTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbBillTo.ForeColor = System.Drawing.Color.Black;
        this.lbBillTo.LocationFloat = new DevExpress.Utils.PointFloat(156.2194F, 0F);
        this.lbBillTo.Name = "lbBillTo";
        this.lbBillTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbBillTo.SizeF = new System.Drawing.SizeF(40.42099F, 20F);
        this.lbBillTo.StylePriority.UseBackColor = false;
        this.lbBillTo.StylePriority.UseFont = false;
        this.lbBillTo.StylePriority.UseTextAlignment = false;
        this.lbBillTo.Text = "Date";
        this.lbBillTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel21
        // 
        this.xrLabel21.BackColor = System.Drawing.Color.White;
        this.xrLabel21.BorderColor = System.Drawing.Color.White;
        this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel21.ForeColor = System.Drawing.Color.Black;
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20.00001F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(7, 0, 0, 0, 100F);
        this.xrLabel21.SizeF = new System.Drawing.SizeF(44.22566F, 20F);
        this.xrLabel21.StylePriority.UseBackColor = false;
        this.xrLabel21.StylePriority.UseFont = false;
        this.xrLabel21.StylePriority.UsePadding = false;
        this.xrLabel21.StylePriority.UseTextAlignment = false;
        this.xrLabel21.Text = "User:";
        this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbCountry
        // 
        this.lbCountry.BackColor = System.Drawing.Color.White;
        this.lbCountry.BorderColor = System.Drawing.Color.White;
        this.lbCountry.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbCountry.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Bill_Date", "{0:dd-MMM-yy}")});
        this.lbCountry.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbCountry.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbCountry.LocationFloat = new DevExpress.Utils.PointFloat(197.5083F, 0F);
        this.lbCountry.Name = "lbCountry";
        this.lbCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.lbCountry.SizeF = new System.Drawing.SizeF(66.4917F, 20F);
        this.lbCountry.StylePriority.UseBackColor = false;
        this.lbCountry.StylePriority.UseFont = false;
        this.lbCountry.StylePriority.UsePadding = false;
        this.lbCountry.StylePriority.UseTextAlignment = false;
        this.lbCountry.Text = "lbCountry";
        this.lbCountry.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // GroupFooter1
        // 
        this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine14,
            this.xrLine3,
            this.xrTable2,
            this.xrLine4,
            this.xrLabel33,
            this.xrLabel7,
            this.xrLabel29,
            this.xrLabel32,
            this.xrLabel18,
            this.xrLabel1,
            this.xrLabel4,
            this.xrLabel34,
            this.xrLabel35,
            this.xrLabel16,
            this.xrLabel17,
            this.xrLabel26,
            this.xrLabel24,
            this.xrLabel19,
            this.xrLabel8,
            this.xrLabel22,
            this.xrLabel10,
            this.xrLabel5,
            this.xrLabel30,
            this.xrLabel31,
            this.xrLine5});
        this.GroupFooter1.HeightF = 293.4169F;
        this.GroupFooter1.Name = "GroupFooter1";
        // 
        // xrLine14
        // 
        this.xrLine14.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(2.083309F, 0F);
        this.xrLine14.Name = "xrLine14";
        this.xrLine14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine14.SizeF = new System.Drawing.SizeF(267.9167F, 8F);
        // 
        // xrLine3
        // 
        this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 198.917F);
        this.xrLine3.Name = "xrLine3";
        this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine3.SizeF = new System.Drawing.SizeF(258.3333F, 8F);
        // 
        // xrTable2
        // 
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(2.083309F, 214.917F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24});
        this.xrTable2.SizeF = new System.Drawing.SizeF(258.3333F, 69.94048F);
        this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow20
        // 
        this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter1});
        this.xrTableRow20.Name = "xrTableRow20";
        this.xrTableRow20.Weight = 0.2247191011235955D;
        // 
        // lblFooter1
        // 
        this.lblFooter1.CanShrink = true;
        this.lblFooter1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer1")});
        this.lblFooter1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblFooter1.Name = "lblFooter1";
        this.lblFooter1.StylePriority.UseFont = false;
        this.lblFooter1.StylePriority.UseTextAlignment = false;
        this.lblFooter1.Text = "lblFooter1";
        this.lblFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter1.Weight = 0.71165200838753107D;
        // 
        // xrTableRow21
        // 
        this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter2});
        this.xrTableRow21.Name = "xrTableRow21";
        this.xrTableRow21.Weight = 0.2247191011235955D;
        // 
        // lblFooter2
        // 
        this.lblFooter2.CanShrink = true;
        this.lblFooter2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer2")});
        this.lblFooter2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblFooter2.Name = "lblFooter2";
        this.lblFooter2.StylePriority.UseFont = false;
        this.lblFooter2.StylePriority.UseTextAlignment = false;
        this.lblFooter2.Text = "lblFooter2";
        this.lblFooter2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter2.Weight = 0.71165200838753107D;
        // 
        // xrTableRow22
        // 
        this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter3});
        this.xrTableRow22.Name = "xrTableRow22";
        this.xrTableRow22.Weight = 0.2247191011235955D;
        // 
        // lblFooter3
        // 
        this.lblFooter3.CanShrink = true;
        this.lblFooter3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer3")});
        this.lblFooter3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblFooter3.Name = "lblFooter3";
        this.lblFooter3.StylePriority.UseFont = false;
        this.lblFooter3.StylePriority.UseTextAlignment = false;
        this.lblFooter3.Text = "[footer3]";
        this.lblFooter3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter3.Weight = 0.71165200838753107D;
        // 
        // xrTableRow23
        // 
        this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter4});
        this.xrTableRow23.Name = "xrTableRow23";
        this.xrTableRow23.Weight = 0.2247191011235955D;
        // 
        // lblFooter4
        // 
        this.lblFooter4.CanShrink = true;
        this.lblFooter4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer4")});
        this.lblFooter4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblFooter4.Name = "lblFooter4";
        this.lblFooter4.StylePriority.UseFont = false;
        this.lblFooter4.StylePriority.UseTextAlignment = false;
        this.lblFooter4.Text = "lblFooter4";
        this.lblFooter4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter4.Weight = 0.71165200838753107D;
        // 
        // xrTableRow24
        // 
        this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter5});
        this.xrTableRow24.Name = "xrTableRow24";
        this.xrTableRow24.Weight = 0.2247191011235955D;
        // 
        // lblFooter5
        // 
        this.lblFooter5.CanShrink = true;
        this.lblFooter5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer5")});
        this.lblFooter5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblFooter5.Name = "lblFooter5";
        this.lblFooter5.StylePriority.UseFont = false;
        this.lblFooter5.StylePriority.UseTextAlignment = false;
        this.lblFooter5.Text = "lblFooter5";
        this.lblFooter5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter5.Weight = 0.71165200838753107D;
        // 
        // xrLine4
        // 
        this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 172.6253F);
        this.xrLine4.Name = "xrLine4";
        this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine4.SizeF = new System.Drawing.SizeF(258.3333F, 8F);
        // 
        // xrLabel33
        // 
        this.xrLabel33.BackColor = System.Drawing.Color.White;
        this.xrLabel33.BorderColor = System.Drawing.Color.White;
        this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.SBCAmount")});
        this.xrLabel33.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel33.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(179.9015F, 95.58334F);
        this.xrLabel33.Name = "xrLabel33";
        this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel33.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel33.SizeF = new System.Drawing.SizeF(83.23058F, 17F);
        this.xrLabel33.StylePriority.UseBackColor = false;
        this.xrLabel33.StylePriority.UseFont = false;
        this.xrLabel33.StylePriority.UsePadding = false;
        this.xrLabel33.StylePriority.UseTextAlignment = false;
        this.xrLabel33.Text = "xrLabel33";
        this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel7
        // 
        this.xrLabel7.BackColor = System.Drawing.Color.White;
        this.xrLabel7.BorderColor = System.Drawing.Color.White;
        this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.TaxName")});
        this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel7.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(26.69131F, 116.4585F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel7.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel7.SizeF = new System.Drawing.SizeF(81.19377F, 18.29172F);
        this.xrLabel7.StylePriority.UseBackColor = false;
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.StylePriority.UseTextAlignment = false;
        this.xrLabel7.Text = "xrLabel7";
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel29
        // 
        this.xrLabel29.BackColor = System.Drawing.Color.White;
        this.xrLabel29.BorderColor = System.Drawing.Color.White;
        this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.KKCAmount")});
        this.xrLabel29.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel29.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(179.9015F, 78.58334F);
        this.xrLabel29.Name = "xrLabel29";
        this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel29.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel29.SizeF = new System.Drawing.SizeF(83.23058F, 17F);
        this.xrLabel29.StylePriority.UseBackColor = false;
        this.xrLabel29.StylePriority.UseFont = false;
        this.xrLabel29.StylePriority.UsePadding = false;
        this.xrLabel29.StylePriority.UseTextAlignment = false;
        this.xrLabel29.Text = "xrLabel29";
        this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel32
        // 
        this.xrLabel32.BackColor = System.Drawing.Color.White;
        this.xrLabel32.BorderColor = System.Drawing.Color.White;
        this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.SBCName")});
        this.xrLabel32.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel32.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(26.60798F, 98.16679F);
        this.xrLabel32.Name = "xrLabel32";
        this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel32.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel32.SizeF = new System.Drawing.SizeF(81.19377F, 18.29172F);
        this.xrLabel32.StylePriority.UseBackColor = false;
        this.xrLabel32.StylePriority.UseFont = false;
        this.xrLabel32.StylePriority.UseTextAlignment = false;
        this.xrLabel32.Text = "xrLabel32";
        this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel18
        // 
        this.xrLabel18.BackColor = System.Drawing.Color.White;
        this.xrLabel18.BorderColor = System.Drawing.Color.White;
        this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.TaxAmount")});
        this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel18.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(190.849F, 116.4585F);
        this.xrLabel18.Name = "xrLabel18";
        this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel18.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel18.SizeF = new System.Drawing.SizeF(72.55934F, 17.00001F);
        this.xrLabel18.StylePriority.UseBackColor = false;
        this.xrLabel18.StylePriority.UseFont = false;
        this.xrLabel18.StylePriority.UsePadding = false;
        this.xrLabel18.StylePriority.UseTextAlignment = false;
        this.xrLabel18.Text = "xrLabel18";
        this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel1
        // 
        this.xrLabel1.BackColor = System.Drawing.Color.White;
        this.xrLabel1.BorderColor = System.Drawing.Color.White;
        this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel1.ForeColor = System.Drawing.Color.Black;
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(26.60798F, 154.3336F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(77.96463F, 18.29168F);
        this.xrLabel1.StylePriority.UseBackColor = false;
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.Text = "Round Off:";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel4
        // 
        this.xrLabel4.BackColor = System.Drawing.Color.White;
        this.xrLabel4.BorderColor = System.Drawing.Color.White;
        this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Round_Amount")});
        this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel4.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(179.9015F, 153.0419F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(84.0985F, 19.58334F);
        this.xrLabel4.StylePriority.UseBackColor = false;
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UsePadding = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.Text = "xrLabel4";
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel34
        // 
        this.xrLabel34.BackColor = System.Drawing.Color.White;
        this.xrLabel34.BorderColor = System.Drawing.Color.White;
        this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.DeliveryName")});
        this.xrLabel34.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel34.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(26.69131F, 134.7502F);
        this.xrLabel34.Name = "xrLabel34";
        this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel34.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel34.SizeF = new System.Drawing.SizeF(116.6826F, 18.29172F);
        this.xrLabel34.StylePriority.UseBackColor = false;
        this.xrLabel34.StylePriority.UseFont = false;
        this.xrLabel34.StylePriority.UseTextAlignment = false;
        this.xrLabel34.Text = "xrLabel34";
        this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel35
        // 
        this.xrLabel35.BackColor = System.Drawing.Color.White;
        this.xrLabel35.BorderColor = System.Drawing.Color.White;
        this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.DeliveryAmount")});
        this.xrLabel35.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel35.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(196.0488F, 134.7502F);
        this.xrLabel35.Name = "xrLabel35";
        this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel35.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel35.SizeF = new System.Drawing.SizeF(67.3596F, 17F);
        this.xrLabel35.StylePriority.UseBackColor = false;
        this.xrLabel35.StylePriority.UseFont = false;
        this.xrLabel35.StylePriority.UsePadding = false;
        this.xrLabel35.StylePriority.UseTextAlignment = false;
        this.xrLabel35.Text = "xrLabel35";
        this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel16
        // 
        this.xrLabel16.BackColor = System.Drawing.Color.White;
        this.xrLabel16.BorderColor = System.Drawing.Color.White;
        this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel16.ForeColor = System.Drawing.Color.Black;
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(26.69131F, 24.99994F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(82.37578F, 18.29169F);
        this.xrLabel16.StylePriority.UseBackColor = false;
        this.xrLabel16.StylePriority.UseFont = false;
        this.xrLabel16.Text = "Total:";
        this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel17
        // 
        this.xrLabel17.BackColor = System.Drawing.Color.White;
        this.xrLabel17.BorderColor = System.Drawing.Color.White;
        this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Bill_Value")});
        this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel17.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(179.9015F, 24.99994F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel17.SizeF = new System.Drawing.SizeF(83.50685F, 17F);
        this.xrLabel17.StylePriority.UseBackColor = false;
        this.xrLabel17.StylePriority.UseFont = false;
        this.xrLabel17.StylePriority.UsePadding = false;
        this.xrLabel17.StylePriority.UseTextAlignment = false;
        this.xrLabel17.Text = "xrLabel17";
        this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel26
        // 
        this.xrLabel26.BackColor = System.Drawing.Color.White;
        this.xrLabel26.BorderColor = System.Drawing.Color.White;
        this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.KKCName")});
        this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel26.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(26.69131F, 79.87507F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel26.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel26.SizeF = new System.Drawing.SizeF(81.19377F, 18.29172F);
        this.xrLabel26.StylePriority.UseBackColor = false;
        this.xrLabel26.StylePriority.UseFont = false;
        this.xrLabel26.StylePriority.UseTextAlignment = false;
        this.xrLabel26.Text = "xrLabel26";
        this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel24
        // 
        this.xrLabel24.BackColor = System.Drawing.Color.White;
        this.xrLabel24.BorderColor = System.Drawing.Color.White;
        this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Qty")});
        this.xrLabel24.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel24.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(126.3393F, 7.999929F);
        this.xrLabel24.Name = "xrLabel24";
        this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel24.SizeF = new System.Drawing.SizeF(43.46779F, 17F);
        this.xrLabel24.StylePriority.UseBackColor = false;
        this.xrLabel24.StylePriority.UseFont = false;
        this.xrLabel24.StylePriority.UsePadding = false;
        this.xrLabel24.StylePriority.UseTextAlignment = false;
        xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrLabel24.Summary = xrSummary2;
        this.xrLabel24.Text = "xrLabel24";
        this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel19
        // 
        this.xrLabel19.BackColor = System.Drawing.Color.White;
        this.xrLabel19.BorderColor = System.Drawing.Color.White;
        this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.DiscountName")});
        this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel19.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(26.69131F, 43.29163F);
        this.xrLabel19.Name = "xrLabel19";
        this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel19.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel19.SizeF = new System.Drawing.SizeF(81.19377F, 18.29172F);
        this.xrLabel19.StylePriority.UseBackColor = false;
        this.xrLabel19.StylePriority.UseFont = false;
        this.xrLabel19.StylePriority.UseTextAlignment = false;
        this.xrLabel19.Text = "xrLabel19";
        this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel8
        // 
        this.xrLabel8.BackColor = System.Drawing.Color.White;
        this.xrLabel8.BorderColor = System.Drawing.Color.White;
        this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel8.CanShrink = true;
        this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.SurVal")});
        this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel8.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(197.6812F, 61.58335F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel8.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel8.SizeF = new System.Drawing.SizeF(65.7272F, 16.99999F);
        this.xrLabel8.StylePriority.UseBackColor = false;
        this.xrLabel8.StylePriority.UseFont = false;
        this.xrLabel8.StylePriority.UsePadding = false;
        this.xrLabel8.StylePriority.UseTextAlignment = false;
        this.xrLabel8.Text = "xrLabel8";
        this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel22
        // 
        this.xrLabel22.BackColor = System.Drawing.Color.White;
        this.xrLabel22.BorderColor = System.Drawing.Color.White;
        this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel22.ForeColor = System.Drawing.Color.Black;
        this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(26.69131F, 7.999929F);
        this.xrLabel22.Name = "xrLabel22";
        this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel22.SizeF = new System.Drawing.SizeF(99.64799F, 16.99998F);
        this.xrLabel22.StylePriority.UseBackColor = false;
        this.xrLabel22.StylePriority.UseFont = false;
        this.xrLabel22.Text = "Total QTY:";
        this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel10
        // 
        this.xrLabel10.BackColor = System.Drawing.Color.White;
        this.xrLabel10.BorderColor = System.Drawing.Color.White;
        this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel10.CanShrink = true;
        this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Discount")});
        this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel10.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(197.6812F, 43.29163F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel10.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel10.SizeF = new System.Drawing.SizeF(65.7272F, 16.99999F);
        this.xrLabel10.StylePriority.UseBackColor = false;
        this.xrLabel10.StylePriority.UseFont = false;
        this.xrLabel10.StylePriority.UsePadding = false;
        this.xrLabel10.StylePriority.UseTextAlignment = false;
        this.xrLabel10.Text = "xrLabel10";
        this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel5
        // 
        this.xrLabel5.BackColor = System.Drawing.Color.White;
        this.xrLabel5.BorderColor = System.Drawing.Color.White;
        this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.SurValName")});
        this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel5.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(26.60798F, 61.58335F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel5.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel5.SizeF = new System.Drawing.SizeF(81.19377F, 18.29172F);
        this.xrLabel5.StylePriority.UseBackColor = false;
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.StylePriority.UseTextAlignment = false;
        this.xrLabel5.Text = "xrLabel5";
        this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // retailBillDataSet1
        // 
        this.retailBillDataSet1.DataSetName = "RetailBillDataSet";
        this.retailBillDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // pos_sp_retailbillreportTableAdapter1
        // 
        this.pos_sp_retailbillreportTableAdapter1.ClearBeforeFill = true;
        // 
        // retailBillDataSet2
        // 
        this.retailBillDataSet2.DataSetName = "RetailBillDataSet";
        this.retailBillDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // xrLabel37
        // 
        this.xrLabel37.BackColor = System.Drawing.Color.White;
        this.xrLabel37.BorderColor = System.Drawing.Color.White;
        this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.BillMode")});
        this.xrLabel37.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel37.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(185.333F, 141.6666F);
        this.xrLabel37.Name = "xrLabel37";
        this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel37.SizeF = new System.Drawing.SizeF(75.08363F, 20F);
        this.xrLabel37.StylePriority.UseBackColor = false;
        this.xrLabel37.StylePriority.UseFont = false;
        this.xrLabel37.StylePriority.UsePadding = false;
        this.xrLabel37.StylePriority.UseTextAlignment = false;
        this.xrLabel37.Text = "xrLabel37";
        this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // rptRetailBill
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1});
        this.DataAdapter = this.pos_sp_retailbillreportTableAdapter1;
        this.DataMember = "pos_sp_retailbillreport";
        this.DataSource = this.retailBillDataSet2;
        this.Margins = new System.Drawing.Printing.Margins(0, 0, 1, 180);
        this.PageWidth = 275;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.retailBillDataSet1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.retailBillDataSet2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
