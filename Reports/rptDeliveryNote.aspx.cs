﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_rptDeliveryNote : System.Web.UI.Page
{
    public DateTime Date { get { return Request.QueryString["RefNo"] != null ? Convert.ToDateTime(Request.QueryString["RefNo"]) : DateTime.Now; } }
    protected void Page_Load(object sender, EventArgs e)
    {

        Int16 BranchId = Convert.ToInt16(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        RptDeliveryNote objBreakageExpiry = new RptDeliveryNote(Date, BranchId);
        ReportViewer1.Report = objBreakageExpiry;
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}