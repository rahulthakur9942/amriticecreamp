﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_companycust_rpt : System.Web.UI.Page
{
    mst_customer_rate msr = new mst_customer_rate();
    companycust_rpt_cls CompanyCustReport = new companycust_rpt_cls();
    managesqence ms = new managesqence();
    protected void Page_Load(object sender, EventArgs e)
    {
        txtDateFrom.Text = DateTime.Now.ToShortDateString();
        txtDateTo.Text = DateTime.Now.ToShortDateString();
        if (IsPostBack == false)
        {

            ddcustomertype();
            bind_dd_pos();


        }
    }
    public void bind_dd_pos()
    {
        ms.req = "bind_dd";
        DataTable dt = ms.bind_item_dd();
        dd_pos.DataSource = dt;
        dd_pos.DataTextField = "title";
        dd_pos.DataValueField = "posid";
        dd_pos.DataBind();
      
    

    }
    public void ddcustomertype()
    {
        msr.req = "bind_ddcustomer";
        DataTable dt = msr.bind_item_dd();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "customer_name";
        dd_customername.DataValueField = "cst_id";
        dd_customername.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Shop", "0");
        dd_customername.Items.Insert(0, listItem1);

    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
        CompanyCustReport.StartDate = txtDateFrom.Text.Trim();
        CompanyCustReport.EndDate = txtDateTo.Text.Trim(); 
        CompanyCustReport.BillMode = dd_bill_mode.SelectedValue;
        CompanyCustReport.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        CompanyCustReport.pos_id = Convert.ToInt32( dd_pos.SelectedValue);
        DataTable dt = CompanyCustReport.GetReport();
        gvUserInfo.DataSource = dt;
        gvUserInfo.DataBind();

    }
    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
}