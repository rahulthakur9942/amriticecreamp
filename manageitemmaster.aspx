﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true"
    CodeFile="manageitemmaster.aspx.cs" Inherits="manageitemmaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    
    <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
    <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <link href="<%=CommonFunctions.SiteUrl()%>css/tabcontent.css" rel="stylesheet" type="text/css" />
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>
     <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
     
    <script language="javascript" type="text/javascript">

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }


                $(document).on("click", "#btnDel", function (event) {

                    var RowIndex = Number($(this).closest('tr').index());


                    NutritionFactCollection.splice(RowIndex, 1);
                    BindRows();


                });




                var m_ItemId = 0;

                $(document).keypress(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        var $this = $(event.target);
                        var index = parseFloat($this.attr('data-index'));
                        $('[data-index="' + (index + 1).toString() + '"]').focus();


                        $("#ddlIMDepartment").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMGroup").focus();
                            }
                        });


                        $("#ddlIMGroup").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMSubGroup").focus();
                            }
                        });

                        $("#ddlIMSubGroup").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMCompany").focus();
                            }
                        });

                        $("#ddlIMCompany").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMCategory").focus();
                            }
                        });

                        $("#ddlIMCategory").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMTax").focus();
                            }
                        });


                        $("#ddlIMTax").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMItemType").focus();
                            }
                        });

                        $("#ddlIMItemType").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMExicise").focus();
                            }
                        });

                        $("#ddlIMExicise").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMPurRate").focus();
                            }
                        });

                        $("#ddlIMRackShelf").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMDeiveryNoteRate").focus();
                            }
                        });

                        $("#ddlIMSaleUnit").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMWgtLtr").focus();
                            }
                        });

                        $("#ddlIMWgtLtr").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMTransactionType").focus();
                            }
                        });

                        $("#ddlIMCurQtyRate").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMDealerMargin").focus();
                            }
                        });

                        $("#txtIMComments").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#btnIMAdd").click();
                            }
                        });
                        

                    }
                });



                $(document).ready(
    function () {



      




        $('#formID').on('keydown', 'input', function (event) {

            if (event.which == 13) {

                event.preventDefault();

                var $this = $(event.target);

                var index = parseFloat($this.attr('data-index'));


                $('[data-index="' + (index + 1).toString() + '"]').focus();








            }
        });





        BindGrid();


        $("#btnDiscontinue").click(
      function () {

          BindGridDiscontinuedItems();
      });


        $("#btnRefresh").click(
     function () {

         BindGrid();
     });


        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {

                    $("#btnAdd").show();
                    $("#btnAdd").click(
       function () {

           var Id = 0;
           $.ajax({
               type: "POST",
               data: '{"Id":"' + Id + '"}',
               url: "manageitemmaster.aspx/LoadUserControl",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   $("#dvIMDialog").remove();
                   $("body").append("<div id='dvIMDialog'/>");
                   $("#dvIMDialog").html(msg.d).dialog({ modal: true, width: 1115, height: 630, });
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {

               }
           });
       }

       );

                }
                else if (arrRole[i] == "3") {

                    $("#btnEdit").show();
                    $("#btnEdit").click(
        function () {

            $.uiLock('');

            var Id = m_ItemId;
            $.ajax({
                type: "POST",
                data: '{"Id":"' + Id + '"}',
                url: "manageitemmaster.aspx/LoadUserControl",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    $("#dvIMDialog").remove();
                    $("body").append("<div id='dvIMDialog'/>");
                    $("#dvIMDialog").html(msg.d).dialog({ modal: true, width: 1115, height: 630, });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                BindGrid();
                    $.uiUnlock();
                }
            });

        }
        );

                }
                else if (arrRole[i] == "2") {


                    $("#btnDelete").show();
                    $("#btnDelete").click(
    function () {

        var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
        if ($.trim(SelectedRow) == "") {
            alert("No Item is selected to Delete");
            return;
        }

        var ItemId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ItemID')
        if (confirm("Are You sure to delete this record")) {
            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{"ItemID":"' + ItemId + '"}',
                url: "manageitemmaster.aspx/Delete",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.status == -1) {
                        alert("Deletion Failed. Item is in Use.");
                        return
                    }


                    BindGrid();
                    alert("Item is Deleted successfully.");




                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }


    });




                }
            }
        }




    }
    );


    </script>
    <style type="text/css">
        .table
        {
            margin: 5px;
        }
    </style>
 
    
   <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
      <asp:HiddenField ID="hdnDate" runat="server"/>
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Items</h3>
                        </div>
                       
                    

                    <div class="x_panel">
                         <div class="form-group">
                                
                              
                <div class="youhave" >
                     <table cellspacing="0" cellpadding="0">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnDiscontinue"  class="btn btn-primary" ><i class="fa fa-external-link"></i> Disconitued Items</div></td>
                                        <td style="padding-top:5px"> <div id="btnRefresh"  class="btn btn-success" ><i class="fa fa-edit m-right-xs"></i> Refresh</div></td>
                                       
                                            </tr>
                                            </table>



      	          <table id="jQGridDemo">
    </table>
       <table cellspacing="0" cellpadding="0">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnAdd" style="display:none;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Add</div></td>
                                        <td style="padding-top:5px"> <div id="btnEdit" style="display:none;"  class="btn btn-success" ><i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                        <td style="padding-top:5px"> <div id="btnDelete" style="display:none;" class="btn btn-danger" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
                </div>


                        
                    </div>


                    </div>

                     
                </div>
                <!-- /page content -->

               

            </div>

       </div>


 
</form>


            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageProducts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ItemId','MasterCode', 'ItemCode', 'ItemName', 'Barcode',   'SalesInUnit', 'PurchaseRate', 'SaleRate', 'MRP'],
                           
                        colModel: [
                                    { name: 'ItemID', key: true, index: 'ItemID', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                    { name: 'Master_Code', index: 'Master_Code', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Code', index: 'Item_Code', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                    { name: 'Bar_Code', index: 'Bar_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    
                                    
                                    { name: 'Unit', index: 'Unit', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                     { name: 'Purchase_Rate', index: 'Purchase_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    { name: 'Sale_Rate', index: 'Sale_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ItemID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Items List",

                        editurl: 'handlers/ManageProducts.ashx',
                         ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {

                onSelectRow: function (rowid, iRow, iCol, e) {

                    m_ItemId = 0;
              
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                   $("#btnUpdate").css({ "display": "none" });
                   $("#btnReset").css({ "display": "none" });
                   $("#btnAdd").css({ "display": "none" });
                  

                   for (var i = 0; i < arrRole.length; i++) {

                       if (arrRole[i] == 1) {

                           $("#btnAdd").css({ "display": "block" });
                       }
                       if (arrRole[i] == 2) {

                           $("#btnDelete").css({ "display": "block" });
                       }
                       if (arrRole[i] == 3) {
                           $("#btnEdit").css({ "display": "block" });


                           m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');

                       }

                   }

                  
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>



        <script type="text/javascript">
            function BindGridDiscontinuedItems() {
                jQuery("#jQGridDemo").GridUnload();
                jQuery("#jQGridDemo").jqGrid({
                    url: 'handlers/ManageDiscontinuedProducts.ashx',
                    ajaxGridOptions: { contentType: "application/json" },
                    datatype: "json",

                    colNames: ['ItemId', 'MasterCode', 'ItemCode', 'ItemName', 'Barcode', 'SalesInUnit', 'PurchaseRate', 'SaleRate', 'MRP'],

                    colModel: [
                                { name: 'ItemID', key: true, index: 'ItemID', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                { name: 'Master_Code', index: 'Master_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Item_Code', index: 'Item_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                { name: 'Bar_Code', index: 'Bar_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },


                                { name: 'Unit', index: 'Unit', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                 { name: 'Purchase_Rate', index: 'Purchase_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Sale_Rate', index: 'Sale_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },


                    ],
                    rowNum: 10,

                    mtype: 'GET',
                    loadonce: true,
                    rowList: [10, 20, 30],
                    pager: '#jQGridDemoPager',
                    sortname: 'ItemID',
                    viewrecords: true,
                    height: "100%",
                    width: "400px",
                    sortorder: 'asc',
                    caption: "Items List",

                    editurl: 'handlers/ManageProducts.ashx',
                    ignoreCase: true,
                    toolbar: [true, "top"],


                });


                var $grid = $("#jQGridDemo");
                // fill top toolbar
                $('#t_' + $.jgrid.jqID($grid[0].id))
                    .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                $("#globalSearchText").keypress(function (e) {
                    var key = e.charCode || e.keyCode || 0;
                    if (key === $.ui.keyCode.ENTER) { // 13
                        $("#globalSearch").click();
                    }
                });
                $("#globalSearch").button({
                    icons: { primary: "ui-icon-search" },
                    text: false
                }).click(function () {
                    var postData = $grid.jqGrid("getGridParam", "postData"),
                        colModel = $grid.jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#globalSearchText").val(),
                        l = colModel.length,
                        i,
                        cm;
                    for (i = 0; i < l; i++) {
                        cm = colModel[i];
                        if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                            rules.push({
                                field: cm.name,
                                op: "cn",
                                data: searchText
                            });
                        }
                    }
                    postData.filters = JSON.stringify({
                        groupOp: "OR",
                        rules: rules
                    });
                    $grid.jqGrid("setGridParam", { search: true });
                    $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                    return false;
                });








                $("#jQGridDemo").jqGrid('setGridParam',
        {

            onSelectRow: function (rowid, iRow, iCol, e) {

                m_ItemId = 0;

                var arrRole = [];
                arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                    $("#btnUpdate").css({ "display": "none" });
                    $("#btnReset").css({ "display": "none" });
                    $("#btnAdd").css({ "display": "none" });


                    for (var i = 0; i < arrRole.length; i++) {

                        if (arrRole[i] == 1) {

                            $("#btnAdd").css({ "display": "block" });
                        }
                        if (arrRole[i] == 2) {

                            $("#btnDelete").css({ "display": "block" });
                        }
                        if (arrRole[i] == 3) {
                            $("#btnEdit").css({ "display": "block" });


                            m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');

                        }

                    }


                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

 
</asp:Content>
