﻿function validateForm(form) {


    var IsValid = true;
    var FirstTime = true;
    var firstCntrlId = "";
    var ErrorMessages = "";
    var ShowAlert = false;

    if (form == "detach") {

        $(".CustomErrorClass").removeClass("CustomErrorClass");
        return;
    }


    $("#" + form + " .validate").each(
                function () {


                    var first = true;

                    if ($(this).hasClass("required")) {

                        //                        var controlType = $(this).prop('type');

                        //                        if (controlType == "text") {

                        if (jQuery.trim($(this).val()) == "") {


                            var fName = $(this).attr("valname");
                            first = false;
                            ErrorMessages = ErrorMessages + "* Please enter " + fName + "\n";
                            $(this).addClass("CustomErrorClass");

                            if (FirstTime) {
                                firstCntrlId = $(this);
                            }

                            FirstTime = false;
                            IsValid = false;

                        }
                        else {


                            $(this).removeClass("CustomErrorClass")
                        }
                        //                        }

                    }


                    if ($(this).hasClass("ddlrequired")) {

                        //                        var controlType = $(this).prop('type');

                        //                        if (controlType == "text") {

                        if (jQuery.trim($(this).val()) == "" || jQuery.trim($(this).val()) == "0") {


                            var fName = $(this).attr("valname");
                            first = false;
                            ErrorMessages = ErrorMessages + "* Please enter " + fName + "\n";
                            $(this).addClass("CustomErrorClass");

                            if (FirstTime) {
                                firstCntrlId = $(this);
                            }

                            FirstTime = false;
                            IsValid = false;

                        }
                        else {


                            $(this).removeClass("CustomErrorClass")
                        }
                        //                        }

                    }
                 





                    if ($(this).hasClass("alphanumeric") && first == true) {

                        if ($(this).val() != "") {

                            var reg = /^[0-9a-zA-Z\s]+$/;



                            var value = jQuery.trim($(this).val());




                            if (reg.test(value)) {


                                $(this).removeClass("CustomErrorClass");

                            }
                            else {
                                var fName = $(this).attr("valname");
                                first = false;
                                ErrorMessages = ErrorMessages + "* " + fName + " cannot contain Special Symbols.\n";
                                $(this).addClass("CustomErrorClass");
                                if (FirstTime) {
                                    firstCntrlId = $(this);
                                }

                                FirstTime = false;
                                IsValid = false;
                            }
                        }
                        else {
                            $(this).removeClass("CustomErrorClass");

                        }

                    }




                    if ($(this).hasClass("Qty") && first == true) {

                        if ($(this).val() != "") {
                             



                            var value = jQuery.trim($(this).val());


                            if (value!=0) {


                                $(this).removeClass("CustomErrorClass")

                            }
                            else {
                                var fName = $(this).attr("valname");
                                first = false;
                                ErrorMessages = ErrorMessages + "* Please enter valid " + fName + ".\n";
                                $(this).addClass("CustomErrorClass");
                                if (FirstTime) {
                                    firstCntrlId = $(this);
                                }

                                FirstTime = false;
                                IsValid = false;
                            }
                        }
                        else {

                            $(this).removeClass("CustomErrorClass")
                        }

                    }






                    if ($(this).hasClass("float") && first == true) {

                        if ($(this).val() != "") {
                            var reg = /^\d+(\.\d{1,2})?$/;




                            var value = jQuery.trim($(this).val());


                            if (reg.test(value)) {


                                $(this).removeClass("CustomErrorClass")

                            }
                            else {
                                var fName = $(this).attr("valname");
                                first = false;
                                ErrorMessages = ErrorMessages + "* Please enter valid " + fName + ".\n";
                                $(this).addClass("CustomErrorClass");
                                if (FirstTime) {
                                    firstCntrlId = $(this);
                                }

                                FirstTime = false;
                                IsValid = false;
                            }
                        }
                        else {

                            $(this).removeClass("CustomErrorClass")
                        }

                    }






                    if ($(this).hasClass("valNumber") && first == true) {

                        if ($(this).val() != "") {

                            var reg = /^[0-9]\d*$/;


                            var value = jQuery.trim($(this).val());


                            if (reg.test(value)) {


                                $(this).removeClass("CustomErrorClass");

                            }
                            else {
                                var fName = $(this).attr("valname");
                                first = false;
                                ErrorMessages = ErrorMessages + "* Please enter valid " + fName + ".\n";

                                IsValid = false;
                                if (FirstTime) {
                                    firstCntrlId = $(this);
                                }

                                FirstTime = false;
                                $(this).addClass("CustomErrorClass");

                            }
                        }
                        else {
                            $(this).removeClass("CustomErrorClass");
                        
                        }
                    }






                }
                ); 
                if (firstCntrlId != "") {
                    firstCntrlId.focus();
                }

                if (ErrorMessages != "" && ShowAlert) {
                    alert(ErrorMessages);
                }
                return IsValid;

}