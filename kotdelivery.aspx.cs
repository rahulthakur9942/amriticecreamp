﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;

public partial class kotdelivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }
    }

    [WebMethod]
    public static Processing[] FetchProducts()
    {


        var data = new ProcessingBLL().GetForDelivery();
        return data.ToArray();
    }


    
    [WebMethod]
    public static Processing[] MarkDelivered(int ProcessingId,string TableNo)
    {
        var data = new ProcessingBLL().MarkDeliverd(ProcessingId,TableNo);
        return data.ToArray();

    }

}