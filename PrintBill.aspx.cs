﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class PrintBill : System.Web.UI.Page
{
    Connection conn = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {

        GetPrintData(Request.QueryString["q"]);

    }

    public void  GetPrintData(string BillNowPrefix)
    {
      
        //List<string> GetPrintlist = new List<string>();

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("pos_sp_retailbillreport", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BillNowPrefix", BillNowPrefix);
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adb.Fill(dt);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                string[]bill_date_time= rd["Bill_date"].ToString().Split(' ');
                header1.InnerText = rd["Header1"].ToString();
                header2.InnerText = rd["Header2"].ToString();
                header3.InnerText = rd["Header3"].ToString();
                header4.InnerText = rd["Header4"].ToString();
                header5.InnerText = rd["Header5"].ToString();
                try
                {
                    Tableno.InnerText = rd["Tableno"].ToString();
                }
                catch 
                {

                    Tableno.InnerText = "0";
                }
             
                Remarks.InnerText = rd["Remarks"].ToString();
                try
                {
                    Billmode.InnerText = rd["Billmode"].ToString();
                }
                catch
                {

                    Billmode.InnerText = "";
                }

               
                BillNo.InnerText = rd["BillNowPrefix"].ToString();
                BillDate.InnerText = bill_date_time[0];
                UserName.InnerText = rd["UserName"].ToString();
                try
                {
                    BillTime.InnerText = rd["Billtime"].ToString();
                }
                catch 
                {

                    BillTime.InnerText = "00:00";
                }
             
                NoOfItems.InnerText = dt.Rows.Count.ToString();
                try
                {
                    NetAmount.InnerText = rd["net_amount"].ToString();
                }
                catch 
                {

                    NetAmount.InnerText = "0.00";
                }
                try
                {
                    RoundAmount.InnerText = rd["Round_amount"].ToString();
                }
                catch 
                {

                    RoundAmount.InnerText = "0.00";
                }
                try
                {
                    Billvalue.InnerText = rd["bill_value"].ToString();
                }
                catch 
                {

                    Billvalue.InnerText = "0.00";
                }
            

                try
                {
                    Orderthrough.InnerText = rd["cstname"].ToString();
                }
                catch 
                {

                    Orderthrough.InnerText = "Shop";
                }

                try
                {
                    BillNoBarcode.InnerText = rd["BillNowPrefix"].ToString();
                }
                catch (Exception)
                {

                    BillNoBarcode.InnerText = "--";
                }
                try
                {
                    if (!string.IsNullOrEmpty(rd["DisPer"].ToString()))
                    {
                        trdiscount.Visible = true;
                        DiscountPer.InnerText = rd["DisPer"].ToString();
                        DiscountAmt.InnerText = rd["Discount"].ToString();
                    }
                    else
                    {
                        trdiscount.Visible = false;
                    }
                }
                catch 
                {

                   
                }
               
              

                footer1.InnerText = rd["footer1"].ToString();
                footer2.InnerText = rd["footer2"].ToString();
                footer3.InnerText = rd["footer3"].ToString();

            }

            RtpItemList.DataSource = dt;
            RtpItemList.DataBind();
            GetSubPrintData(BillNowPrefix);

        }

    }

    public void GetSubPrintData(string BillNowPrefix)
    {
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Report_sp_ExciseChallanReportSubReport", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@branchId", 1);
            cmd.Parameters.AddWithValue("@BillNowPrefix", BillNowPrefix);
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adb.Fill(dt);
            
            RptSubreport.DataSource = dt;
            RptSubreport.DataBind();
        }
    }
    }