﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
public partial class TotalBill : System.Web.UI.Page
{

    public string Id { get { return Request.QueryString["Id"] != null ? Convert.ToString(Request.QueryString["Id"]) : string.Empty; } }

    public string tblno { get { return Request.QueryString["tblno"] != null ? Convert.ToString(Request.QueryString["tblno"]) : string.Empty; } }

    public string Status { get { return Request.QueryString["Status"] != null ? Convert.ToString(Request.QueryString["Status"]) : string.Empty; } }


    public string cashcustcode { get { return Request.QueryString["cashcustcode"] != null ? Convert.ToString(Request.QueryString["cashcustcode"]) : string.Empty; } }

    public string cashcustName { get { return Request.QueryString["cashcustName"] != null ? Convert.ToString(Request.QueryString["cashcustName"]) : string.Empty; } }
    public string cashcustmob { get { return Request.QueryString["cashcustmob"] != null ? Convert.ToString(Request.QueryString["cashcustmob"]) : string.Empty; } }
    public string companycustid { get { return Request.QueryString["compnycustid"] != null ? Convert.ToString(Request.QueryString["compnycustid"]) : string.Empty; } }
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
      
            //Session.Remove("cst_id");
            Session["RetValue"] = -22;
            Response.Cookies[Constants.posid].Value = "3";
            hdntbleno.Value = tblno;
            hdndisplay.Value = Id;
            hdndd.Value = Status;
            hdncashcustid.Value = cashcustcode;
            hdncashcustname.Value = cashcustName;
            lblcstname.Text = cashcustName;
            lblcstmob.Text = cashcustmob;
            hdncompnycustid.Value = companycustid;
            hdnDate.Value = DateTime.Now.ToShortDateString();
            ltDateTime.Text = "<span style='font-weight:bold;'>Today - " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now) + "</span>";


            //string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            //if (strDate == "")
            //{

            //    Response.Redirect("index.aspx?DayOpen=Close");
            //}



        }

  
    }
}