﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccountSettingDAL
/// </summary>
public class AccountSettingDAL:Connection
{
    public SqlDataReader GetAccountSettings(AccountSettings objAccountSettings)
    {
        SqlParameter[] objParam = new SqlParameter[0];



        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetAccountSettings", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateAccountSettings(AccountSettings objAccountSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[42];

        objParam[0] = new SqlParameter("@CN_Acc2DebitCode", objAccountSettings.CN_Acc2DebitCode);
        objParam[1] = new SqlParameter("@CN_Acc2DebitName", objAccountSettings.CN_Acc2DebitName);
        objParam[2] = new SqlParameter("@Comp_AccCode", objAccountSettings.Comp_AccCode);
        objParam[3] = new SqlParameter("@Comp_AccName", objAccountSettings.Comp_AccName);
        objParam[4] = new SqlParameter("@OSPurc_AccCode", objAccountSettings.OSPurc_AccCode);
        objParam[5] = new SqlParameter("@OSPurc_AccName", objAccountSettings.OSPurc_AccName);
        objParam[6] = new SqlParameter("@OSPurc_TaxCode", objAccountSettings.OSPurc_TaxCode);
        objParam[7] = new SqlParameter("@OSPurc_TaxName", objAccountSettings.OSPurc_TaxName);
        objParam[8] = new SqlParameter("@CST_ACCCode", objAccountSettings.CST_ACCCode);
        objParam[9] = new SqlParameter("@CST_ACCName", objAccountSettings.CST_ACCName);
        objParam[10] = new SqlParameter("@CST_TaxCode", objAccountSettings.CST_TaxCode);
        objParam[11] = new SqlParameter("@CST_TaxName", objAccountSettings.CST_TaxName);
        objParam[12] = new SqlParameter("@Disp_AccCode", objAccountSettings.Disp_AccCode);
        objParam[13] = new SqlParameter("@Disp_AccName", objAccountSettings.Disp_AccName);
        objParam[14] = new SqlParameter("@Adj_AccCode", objAccountSettings.Adj_AccCode);
        objParam[15] = new SqlParameter("@Adj_AccName", objAccountSettings.Adj_AccName);
        objParam[16] = new SqlParameter("@DebitNote_AccCode", objAccountSettings.DebitNote_AccCode);
        objParam[17] = new SqlParameter("@DebitNote_AccName", objAccountSettings.DebitNote_AccName);
        objParam[18] = new SqlParameter("@Disc_AccCode", objAccountSettings.Disc_AccCode);
        objParam[19] = new SqlParameter("@Disc_AccName", objAccountSettings.Disc_AccName);
        objParam[20] = new SqlParameter("@tcs_accName", objAccountSettings.tcs_accName);
        objParam[21] = new SqlParameter("@tcs_accCode", objAccountSettings.tcs_accCode);
        objParam[22] = new SqlParameter("@Round_accCode", objAccountSettings.Round_accCode);
        objParam[23] = new SqlParameter("@Round_accname", objAccountSettings.Round_accname);
        objParam[24] = new SqlParameter("@OrderSaleAccCode", objAccountSettings.OrderSaleAccCode);
        objParam[25] = new SqlParameter("@OrderSaleAccName", objAccountSettings.OrderSaleAccName);
        objParam[26] = new SqlParameter("@OrderAdvAccCode", objAccountSettings.OrderAdvAccCode);
        objParam[27] = new SqlParameter("@OrderAdvAccName", objAccountSettings.OrderAdvAccName);
        objParam[28] = new SqlParameter("@ServiceTaxAccCode", objAccountSettings.ServiceTaxAccCode);
        objParam[29] = new SqlParameter("@ServiceTaxAccName", objAccountSettings.ServiceTaxAccName);
        objParam[30] = new SqlParameter("@ServiceChgAccCode", objAccountSettings.ServiceChgAccCode);
        objParam[31] = new SqlParameter("@ServiceChgAccName", objAccountSettings.ServiceChgAccName);
        objParam[32] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[32].Direction = ParameterDirection.ReturnValue;
        objParam[33] = new SqlParameter("@UserId", objAccountSettings.UserId);
        objParam[34] = new SqlParameter("@EduCess_AccCode", objAccountSettings.EduCess_AccCode);
        objParam[35] = new SqlParameter("@EduCess_AccName", objAccountSettings.EduCess_AccName);
        objParam[36] = new SqlParameter("@HECess_AccCode", objAccountSettings.HECess_AccCode);
        objParam[37] = new SqlParameter("@HECess_AccName", objAccountSettings.HECess_AccName);

        objParam[38] = new SqlParameter("@ExciseDuty_Code", objAccountSettings.ExciseDuty_Code);
        objParam[39] = new SqlParameter("@ExciseDuty_AccName", objAccountSettings.ExciseDuty_AccName);
        objParam[40] = new SqlParameter("@BranchTfrSale_Code", objAccountSettings.BranchTfrSale_Code);
        objParam[41] = new SqlParameter("@BranchTfrSale_AccName", objAccountSettings.BranchTfrSale_AccName);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertAccountSettings", objParam);
            retValue = Convert.ToInt16(objParam[32].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}