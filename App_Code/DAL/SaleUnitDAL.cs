﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for SaleUnitDAL
/// </summary>
public class SaleUnitDAL:Connection
{
    public SqlDataReader GetById(SaleUnits objSaleUnit)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Unit_Id", objSaleUnit.Unit_Id);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_SaleUnitGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        List<SaleUnits> DepartmentList = new List<SaleUnits>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_SaleUnitGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(SaleUnits objSaleUnits)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@Unit_Id ", objSaleUnits.Unit_Id);
        objParam[1] = new SqlParameter("@Unit_Name", objSaleUnits.Unit_Name);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objSaleUnits.IsActive);
        objParam[4] = new SqlParameter("@UserId", objSaleUnits.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SaleUnitInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objSaleUnits.Unit_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public int Delete(SaleUnits objSaleUnit)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Unit_Id", objSaleUnit.Unit_Id);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SaleUnitDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objSaleUnit.Unit_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}