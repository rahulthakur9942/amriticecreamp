﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for BankDAL
/// </summary>
public class SubGroupDAL:Connection
{
    public SqlDataReader GetByGroup(int GroupId)
    {

        SqlParameter[] objParam = new SqlParameter[1]; 
        objParam[0] = new SqlParameter("@GroupId", GroupId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_SubGroupsGetByGroupId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}