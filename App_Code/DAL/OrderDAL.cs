﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for OrderDAL
/// </summary>
public class OrderDAL:Connection
{
    public SqlDataReader GetFavourites()
    {

        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetFavouritesForMenuCard", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int32 LikeProduct(int ProductId)
    {
        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@ProductId", ProductId);
        ObjParam[1] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[1].Direction = ParameterDirection.ReturnValue;
       

        try
        {

            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure, "pos_sp_PackingBelongsLikeItem", ObjParam);
            retval = Convert.ToInt32(ObjParam[1].Value);
          
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    
    }

    public Int32 InsertUpdate(Orders objOrders, DataTable dt)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[24];
        ObjParam[0] = new SqlParameter("@Customer_ID", objOrders.Customer_ID);
        ObjParam[1] = new SqlParameter("@Customer_Name", objOrders.Customer_Name);
        ObjParam[2] = new SqlParameter("@Order_Value", objOrders.Order_Value);
       
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objOrders.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objOrders.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objOrders.Net_Amount);

        ObjParam[6] = new SqlParameter("@CreditBank", objOrders.CreditBank);

        ObjParam[7] = new SqlParameter("@Cash_Amount", objOrders.Cash_Amount);
        ObjParam[8] = new SqlParameter("@Credit_Amount", objOrders.Credit_Amount);
        ObjParam[9] = new SqlParameter("@CrCard_Amount", objOrders.CrCard_Amount);
        ObjParam[10] = new SqlParameter("@Round_Amount", objOrders.Round_Amount);

        ObjParam[11] = new SqlParameter("@Passing", objOrders.Passing);
        ObjParam[12] = new SqlParameter("@CashCust_Code", objOrders.CashCust_Code);
        ObjParam[13] = new SqlParameter("@CashCust_Name", objOrders.CashCust_Name);
        ObjParam[14] = new SqlParameter("@Tax_Per", objOrders.Tax_Per);
        ObjParam[15] = new SqlParameter("@R_amount", objOrders.R_amount);

        ObjParam[16] = new SqlParameter("@tableno", objOrders.tableno);
        ObjParam[17] = new SqlParameter("@remarks", objOrders.remarks);
        ObjParam[18] = new SqlParameter("@servalue", objOrders.servalue);
        ObjParam[19] = new SqlParameter("@ReceiviedGRNNo", objOrders.ReceiviedGRNNo);
        ObjParam[20] = new SqlParameter("@EmpCode", objOrders.EmpCode);
        ObjParam[21] = new SqlParameter("@BillDetail", dt);
       
        ObjParam[22] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[22].Direction = ParameterDirection.ReturnValue;
        ObjParam[23] = new SqlParameter("@tokenno", objOrders.tokenno);
        

        try
        {

            objOrders.Order_No = Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "pos_sp_OrderInsertUpdate", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[22].Value);
            objOrders.Order_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

    public int Delete(Orders objOrder)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@OrderNo", objOrder.Order_No);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_cancelOrder", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objOrder.Order_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}