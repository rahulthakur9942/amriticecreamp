﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for SettingExPrintingDAL
/// </summary>
public class SettingExPrintingDAL:Connection
{


    public SqlDataReader GetAll()
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_GetDepartmentWisePrinter", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public int UpdateDepartmentPrinter(string DepartmentName,string PrintCom)
    {
        int retval = 0;
        SqlParameter[] objparam = new SqlParameter[3];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@DeptName", DepartmentName);
        objparam[1] = new SqlParameter("@PrintCom", PrintCom);
        objparam[2] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[2].Direction = ParameterDirection.ReturnValue;

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_UpdateDepartmentPrinter", objparam);
            retval = Convert.ToInt32(objparam[2].Value);

        }

        finally
        {
            objparam = null;
        }
        return retval;
    }

}