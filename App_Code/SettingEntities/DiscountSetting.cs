﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DiscountSetting
/// </summary>
public class DiscountSetting
{

    public bool Allow_Dis_on_Billing { get; set; }
    public bool Enable_Dis_Col { get; set; }
    public bool Dis_Bill_Value { get; set; }
    public bool Back_End_Discount { get; set; }
    public bool Enable_Dis_Amt { get; set; }
    public bool Enable_Cust_Dis { get; set; }
    public string Type { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }

	public DiscountSetting()
	{
        Allow_Dis_on_Billing = false;
        Enable_Dis_Col = false;
        Dis_Bill_Value = false;
        Back_End_Discount = false;
        Enable_Dis_Amt = false;
        Enable_Cust_Dis = false;
        Type = "";
        UserId = 0;
        BranchId = 0;
	}
}