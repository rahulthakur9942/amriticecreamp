﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DiscountMaster
/// </summary>
public class DiscountMaster
{
    public int Discount_ID { get; set; }
    public string Description { get; set; }
    public string Category { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public int Company_ID { get; set; }
    public int Group_ID { get; set; }
    public int Item_ID { get; set; }
    public decimal Discount_Per { get; set; }
    public decimal Discount_Amt { get; set; }
    public bool FULLDAY { get; set; }
    public string START_TIME { get; set; }
    public string END_TIME { get; set; }
    public int SGroup_ID { get; set; }
    public string strSD { get { return Start_Date.ToString("d"); } }
    public string strED { get { return End_Date.ToString("d"); } }
    public int UserId { get; set; }
    public bool IsActive { get; set; }

	public DiscountMaster()
	{
        Discount_ID = 0;
        Description = "";
        Category = "";
        Start_Date = DateTime.Now;
        End_Date = DateTime.Now;
        Company_ID = 0;
        Group_ID = 0;
        Item_ID = 0;
        Discount_Per = 0;
        Discount_Amt = 0;
        FULLDAY = false;
        START_TIME = "";
        END_TIME = "";
        SGroup_ID = 0;
        UserId = 0;
        IsActive = false;
	}
}