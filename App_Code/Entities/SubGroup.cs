﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Banks
/// </summary>
public class SubGroup
{

    public int SubGroupId { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public bool ActiveLink { get; set; }
    public bool InActiveLink { get; set; }
    public int SGroup_Id { get; set; }
    public string SGroup_Name { get; set; }
    public SubGroup()
	{
      
        InActiveLink = true;
        ActiveLink = false;
        SubGroupId = 0;
        Title = string.Empty;
        IsActive = false;
        SGroup_Id = 0;
        SGroup_Name = string.Empty;
	}
}