﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NutritionFacts
/// </summary>
public class NutritionFacts
{
    public int SrNo { get; set; }

    public string Field1 { get; set; }
    public string Field2 { get; set; }
    public string Field3 { get; set; }
    public bool Bold { get; set; }
    public string Ingredients { get; set; }
    public string OtherInformation1 { get; set; }
    public string OtherInformation2 { get; set; }
}