﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RRGodowns
/// </summary>
public class RGodowns
{

    public int Godown_Id { get; set; }
    public string Godown_Name { get; set; }
    public string Divison { get; set; }
    public string FinacialYear { get; set; }
    public string Caption { get; set; }

    public bool IsActive { get; set; }
     
	public RGodowns()
	{
        Godown_Id = 0;
        Godown_Name = string.Empty;
        Divison = string.Empty;
        FinacialYear = string.Empty;
        Caption = string.Empty;
	}
}