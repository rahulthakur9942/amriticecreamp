﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for holdbilldetail
/// </summary>
public class holdbilldetail:Connection
{

    public int Hold_No { get; set; }
    public DateTime Bill_Date { get; set; }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Qty { get; set; }
    public decimal Dis_Per { get; set; }
    public decimal Dis_Amt { get; set; }
    public decimal Amount { get; set; }

    public decimal BillMode { get; set; }
    public decimal Tax { get; set; }
    public decimal TaxAmount { get; set; }
    public string Unit { get; set; }
    public decimal Stock { get; set; }
    public string MASTER_CODE { get; set; }
    public decimal QTY_TO_LESS { get; set; }

	public holdbilldetail()
	{
        Hold_No = 0;
        Bill_Date = DateTime.Now;
        Item_Code = "";
        Item_Name = "";
        
        MRP = 0;
        Rate = 0;
        Qty = 0;
        Dis_Per = 0;
        Dis_Amt = 0;
        Amount = 0;
        BillMode = 0;
        Tax = 0;
        TaxAmount = 0;
        Unit = "";
        Stock = 0;
        MASTER_CODE = "";
        QTY_TO_LESS = 0;




	}
}