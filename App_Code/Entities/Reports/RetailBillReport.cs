﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for RetailBillReport
/// </summary>
public class RetailBillReport : DevExpress.XtraReports.UI.XtraReport
{
    private DetailBand Detail;
    private XRTable xrTable1;
    private XRTableRow xrTableRow2;
    private XRTableCell clOrderID;
    private XRTableCell clCustomerID;
    private XRTableCell clSalesperson;
    private XRTableCell clOrderDate;
    private GroupHeaderBand GroupHeader0;
    private XRLabel xrLabel6;
    private XRLabel xrLabel5;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell4;
    private XRLine xrLine1;
    private XRLabel lbShipCountry;
    private XRLabel lbBillTo;
    private XRLabel lbShipAddress;
    private XRLabel lbShipCityRegionPostalCode;
    private GroupFooterBand GroupFooter0;
    private XRTable xrTable2;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell14;
    private XRLabel xrLabel16;
    private XRLabel xrLabel17;
    private XRTableRow xrTableRow15;
    private XRTableCell xrTableCell15;
    private XRLabel xrLabel18;
    private XRLabel xrLabel19;
    private XRLabel xrLabel20;
    private XRTableRow xrTableRow17;
    private XRTableCell xrTableCell16;
    private XRTableRow xrTableRow18;
    private XRTableCell xrTableCell17;
    private XRLabel xrLabel22;
    private XRTableRow xrTableRow19;
    private XRTableCell xrTableCell18;
    private XRLine xrLine5;
    private XRTableRow xrTableRow20;
    private XRTableCell lblFooter1;
    private XRTableRow xrTableRow21;
    private XRTableCell lblFooter2;
    private XRTableRow xrTableRow22;
    private XRTableCell lblFooter3;
    private XRTableRow xrTableRow23;
    private XRTableCell lblFooter4;
    private XRTableRow xrTableRow24;
    private XRTableCell lblFooter5;
    private PageHeaderBand PageHeader;
    private XRLabel lblHeader4;
    private XRLabel lblHeader5;
    private XRLabel lblHeader3;
    private XRLabel lbHeader1;
    private XRLabel lblHeader2;
    private TopMarginBand topMarginBand1;
    private BottomMarginBand BottomMargin;
    private ReportHeaderBand ReportHeader;
    private XRPictureBox xrPictureBox1;
   
    string[] HeaderStyle1;
    string[] HeaderStyle2;
    string[] HeaderStyle3;
    string[] HeaderStyle4;
    string[] HeaderStyle5;


    string[] FooterStyle1;
    string[] FooterStyle2;
    string[] FooterStyle3;
    string[] FooterStyle4;
    string[] FooterStyle5;
  

    private XRLabel xrLabel8;

    private XRLabel xrLabel10;
    private XRLabel xrLabel9;
    private XRLabel xrLabel7;
    private XRPageInfo xrPageInfo3;

    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell5;
    private XRLabel xrLabel4;
    private XRLabel xrLabel1;
    private XRLine xrLine2;
    private XRLabel xrLabel11;

    private XRLabel xrLabel12;
    private dsBilling dsBilling1;
    private dsBillingTableAdapters.pos_sp_retailbillreportTableAdapter pos_sp_retailbillreportTableAdapter1;
    private XRLabel xrLabel29;
    private XRLabel xrLabel28;
    private XRLabel xrLabel27;
    private XRLabel xrLabel26;
    private XRLabel xrLabel25;
    private XRLabel xrLabel24;
    private XRLabel xrLabel23;
    private XRLabel xrLabel21;
    private XRLabel lbCountry;
    private XRLabel xrLabel13;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell6;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell8;
    private XRLabel xrLabel31;
    private XRLabel xrLabel30;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell9;
    private XRLabel xrLabel32;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell10;
    private XRLine xrLine3;
    private XRTableRow xrTableRow8;
    private XRTableCell xrTableCell11;
    private XRLine xrLine4;
    private dsBilling dsBilling2;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

    public RetailBillReport(string BillNowPrefix)
	{
        //PropAppSetting objPropAppSetting = new PropAppSetting();
        //new PropAppSettingBLL().GetSettings(objPropAppSetting);
        //HeaderStyle1 = objPropAppSetting.Header1Style.Split(',');
        //HeaderStyle2 = objPropAppSetting.Header2Style.Split(',');
        //HeaderStyle3 = objPropAppSetting.Header3Style.Split(',');
        //HeaderStyle4 = objPropAppSetting.Header4Style.Split(',');
        //HeaderStyle5 = objPropAppSetting.Header5Style.Split(',');

        //FooterStyle1 = objPropAppSetting.Footer1Style.Split(',');
        //FooterStyle2 = objPropAppSetting.Footer2Style.Split(',');
        //FooterStyle3 = objPropAppSetting.Footer3Style.Split(',');
        //FooterStyle4 = objPropAppSetting.Footer4Style.Split(',');
        //FooterStyle5 = objPropAppSetting.Footer5Style.Split(',');


        Connection con = new Connection();
		InitializeComponent();
       // salon_sp_retailbillreportTableAdapter3.Connection = new System.Data.SqlClient.SqlConnection(con.sqlDataString);
        dsBilling1.EnforceConstraints = false;

        pos_sp_retailbillreportTableAdapter1.Fill(dsBilling1.pos_sp_retailbillreport, BillNowPrefix);
        //xrLabel11.Text = objPropAppSetting.PCName;
		//
		// TODO: Add constructor logic here
		//
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "RetailBillReport.resx";
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.clOrderID = new DevExpress.XtraReports.UI.XRTableCell();
        this.clCustomerID = new DevExpress.XtraReports.UI.XRTableCell();
        this.clSalesperson = new DevExpress.XtraReports.UI.XRTableCell();
        this.clOrderDate = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupHeader0 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.lbCountry = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
        this.lbShipCountry = new DevExpress.XtraReports.UI.XRLabel();
        this.lbBillTo = new DevExpress.XtraReports.UI.XRLabel();
        this.lbShipAddress = new DevExpress.XtraReports.UI.XRLabel();
        this.lbShipCityRegionPostalCode = new DevExpress.XtraReports.UI.XRLabel();
        this.GroupFooter0 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblFooter5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblHeader4 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblHeader5 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblHeader3 = new DevExpress.XtraReports.UI.XRLabel();
        this.lbHeader1 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblHeader2 = new DevExpress.XtraReports.UI.XRLabel();
        this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.xrPageInfo3 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
        this.dsBilling1 = new dsBilling();
        this.pos_sp_retailbillreportTableAdapter1 = new dsBillingTableAdapters.pos_sp_retailbillreportTableAdapter();
        this.dsBilling2 = new dsBilling();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsBilling1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsBilling2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
        this.Detail.HeightF = 25.08335F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable1
        // 
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable1.SizeF = new System.Drawing.SizeF(369F, 23F);
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
        this.xrTableRow2.BorderColor = System.Drawing.Color.White;
        this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.clOrderID,
            this.clCustomerID,
            this.clSalesperson,
            this.clOrderDate,
            this.xrTableCell7});
        this.xrTableRow2.Font = new System.Drawing.Font("Tahoma", 8.25F);
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow2.Weight = 0.25842696629213485D;
        // 
        // clOrderID
        // 
        this.clOrderID.BackColor = System.Drawing.Color.White;
        this.clOrderID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Item_Name")});
        this.clOrderID.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.clOrderID.Name = "clOrderID";
        this.clOrderID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.clOrderID.StylePriority.UseBackColor = false;
        this.clOrderID.StylePriority.UseFont = false;
        this.clOrderID.StylePriority.UseTextAlignment = false;
        this.clOrderID.Text = "clOrderID";
        this.clOrderID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.clOrderID.Weight = 0.25688746873380242D;
        // 
        // clCustomerID
        // 
        this.clCustomerID.BackColor = System.Drawing.Color.White;
        this.clCustomerID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Qty", "{0:#.00}")});
        this.clCustomerID.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.clCustomerID.Name = "clCustomerID";
        this.clCustomerID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.clCustomerID.StylePriority.UseBackColor = false;
        this.clCustomerID.StylePriority.UseFont = false;
        this.clCustomerID.Text = "clCustomerID";
        this.clCustomerID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clCustomerID.Weight = 0.079638339155210069D;
        // 
        // clSalesperson
        // 
        this.clSalesperson.BackColor = System.Drawing.Color.White;
        this.clSalesperson.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Rate")});
        this.clSalesperson.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.clSalesperson.Name = "clSalesperson";
        this.clSalesperson.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.clSalesperson.StylePriority.UseBackColor = false;
        this.clSalesperson.StylePriority.UseFont = false;
        this.clSalesperson.Text = "clSalesperson";
        this.clSalesperson.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clSalesperson.Weight = 0.11740597938263146D;
        // 
        // clOrderDate
        // 
        this.clOrderDate.BackColor = System.Drawing.Color.White;
        this.clOrderDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Amount", "{0:#.00}")});
        this.clOrderDate.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.clOrderDate.Name = "clOrderDate";
        this.clOrderDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.clOrderDate.StylePriority.UseBackColor = false;
        this.clOrderDate.StylePriority.UseFont = false;
        this.clOrderDate.StylePriority.UseTextAlignment = false;
        this.clOrderDate.Text = "clOrderDate";
        this.clOrderDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clOrderDate.Weight = 0.15433623953048894D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.BackColor = System.Drawing.Color.White;
        this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Tax")});
        this.xrTableCell7.Font = new System.Drawing.Font("Tahoma", 11.25F);
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.StylePriority.UseBackColor = false;
        this.xrTableCell7.StylePriority.UseFont = false;
        this.xrTableCell7.StylePriority.UseTextAlignment = false;
        this.xrTableCell7.Text = "xrTableCell7";
        this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell7.Weight = 0.10594758863087882D;
        // 
        // GroupHeader0
        // 
        this.GroupHeader0.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel29,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel21,
            this.lbCountry,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLine2,
            this.xrLabel6,
            this.xrLabel5,
            this.xrTable3,
            this.xrLine1,
            this.lbShipCountry,
            this.lbBillTo,
            this.lbShipAddress,
            this.lbShipCityRegionPostalCode});
        this.GroupHeader0.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("OrderID", DevExpress.XtraReports.UI.XRColumnSortOrder.Descending)});
        this.GroupHeader0.HeightF = 275.2501F;
        this.GroupHeader0.Name = "GroupHeader0";
        this.GroupHeader0.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.GroupHeader0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel29
        // 
        this.xrLabel29.BackColor = System.Drawing.Color.White;
        this.xrLabel29.BorderColor = System.Drawing.Color.White;
        this.xrLabel29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel29.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel29.ForeColor = System.Drawing.Color.Black;
        this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 151.2501F);
        this.xrLabel29.Name = "xrLabel29";
        this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel29.SizeF = new System.Drawing.SizeF(173.8663F, 25F);
        this.xrLabel29.StylePriority.UseBackColor = false;
        this.xrLabel29.StylePriority.UseFont = false;
        this.xrLabel29.StylePriority.UseTextAlignment = false;
        this.xrLabel29.Text = "TakeAway Order";
        this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel28
        // 
        this.xrLabel28.BackColor = System.Drawing.Color.White;
        this.xrLabel28.BorderColor = System.Drawing.Color.White;
        this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Address")});
        this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel28.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 206.7918F);
        this.xrLabel28.Multiline = true;
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel28.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.xrLabel28.StylePriority.UseBackColor = false;
        this.xrLabel28.StylePriority.UseFont = false;
        this.xrLabel28.StylePriority.UseTextAlignment = false;
        this.xrLabel28.Text = "xrLabel28";
        this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel27
        // 
        this.xrLabel27.BackColor = System.Drawing.Color.White;
        this.xrLabel27.BorderColor = System.Drawing.Color.White;
        this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel27.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 206.7918F);
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel27.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel27.StylePriority.UseBackColor = false;
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.StylePriority.UseTextAlignment = false;
        this.xrLabel27.Text = "Add.";
        this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel26
        // 
        this.xrLabel26.BackColor = System.Drawing.Color.White;
        this.xrLabel26.BorderColor = System.Drawing.Color.White;
        this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel26.ForeColor = System.Drawing.Color.Black;
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 126.2501F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel26.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel26.StylePriority.UseBackColor = false;
        this.xrLabel26.StylePriority.UseFont = false;
        this.xrLabel26.StylePriority.UseTextAlignment = false;
        this.xrLabel26.Text = "Token No:";
        this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel25
        // 
        this.xrLabel25.BackColor = System.Drawing.Color.White;
        this.xrLabel25.BorderColor = System.Drawing.Color.White;
        this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel25.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel25.ForeColor = System.Drawing.Color.Black;
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 100F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel25.StylePriority.UseBackColor = false;
        this.xrLabel25.StylePriority.UseFont = false;
        this.xrLabel25.StylePriority.UseTextAlignment = false;
        this.xrLabel25.Text = "Created On:";
        this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel24
        // 
        this.xrLabel24.BackColor = System.Drawing.Color.White;
        this.xrLabel24.BorderColor = System.Drawing.Color.White;
        this.xrLabel24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel24.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel24.ForeColor = System.Drawing.Color.Black;
        this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 75F);
        this.xrLabel24.Name = "xrLabel24";
        this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel24.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel24.StylePriority.UseBackColor = false;
        this.xrLabel24.StylePriority.UseFont = false;
        this.xrLabel24.StylePriority.UseTextAlignment = false;
        this.xrLabel24.Text = "Steward:";
        this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel23
        // 
        this.xrLabel23.BackColor = System.Drawing.Color.White;
        this.xrLabel23.BorderColor = System.Drawing.Color.White;
        this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.UserName")});
        this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel23.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 50F);
        this.xrLabel23.Name = "xrLabel23";
        this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel23.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.xrLabel23.StylePriority.UseBackColor = false;
        this.xrLabel23.StylePriority.UseFont = false;
        this.xrLabel23.StylePriority.UseTextAlignment = false;
        this.xrLabel23.Text = "xrLabel23";
        this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel21
        // 
        this.xrLabel21.BackColor = System.Drawing.Color.White;
        this.xrLabel21.BorderColor = System.Drawing.Color.White;
        this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel21.ForeColor = System.Drawing.Color.Black;
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 50F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel21.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel21.StylePriority.UseBackColor = false;
        this.xrLabel21.StylePriority.UseFont = false;
        this.xrLabel21.StylePriority.UseTextAlignment = false;
        this.xrLabel21.Text = "User:";
        this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbCountry
        // 
        this.lbCountry.BackColor = System.Drawing.Color.White;
        this.lbCountry.BorderColor = System.Drawing.Color.White;
        this.lbCountry.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbCountry.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Bill_Date", "{0:dd MMMM, yyyy}")});
        this.lbCountry.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbCountry.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbCountry.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 25F);
        this.lbCountry.Name = "lbCountry";
        this.lbCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbCountry.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.lbCountry.StylePriority.UseBackColor = false;
        this.lbCountry.StylePriority.UseFont = false;
        this.lbCountry.StylePriority.UseTextAlignment = false;
        this.lbCountry.Text = "lbCountry";
        this.lbCountry.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel13
        // 
        this.xrLabel13.BackColor = System.Drawing.Color.White;
        this.xrLabel13.BorderColor = System.Drawing.Color.White;
        this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel13.ForeColor = System.Drawing.Color.Black;
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 0F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel13.StylePriority.UseBackColor = false;
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.Text = "Bill No:";
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel12
        // 
        this.xrLabel12.BackColor = System.Drawing.Color.White;
        this.xrLabel12.BorderColor = System.Drawing.Color.White;
        this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel12.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel12.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 181.7918F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.xrLabel12.StylePriority.UseBackColor = false;
        this.xrLabel12.StylePriority.UseFont = false;
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.Text = "Name:";
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLine2
        // 
        this.xrLine2.LineWidth = 3;
        this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 266.6251F);
        this.xrLine2.Name = "xrLine2";
        this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine2.SizeF = new System.Drawing.SizeF(369F, 8F);
        // 
        // xrLabel6
        // 
        this.xrLabel6.BackColor = System.Drawing.Color.White;
        this.xrLabel6.BorderColor = System.Drawing.Color.White;
        this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.CustomerName")});
        this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel6.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 181.7918F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.xrLabel6.StylePriority.UseBackColor = false;
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.Text = "xrLabel6";
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel5
        // 
        this.xrLabel5.BackColor = System.Drawing.Color.White;
        this.xrLabel5.BorderColor = System.Drawing.Color.White;
        this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.tokenno")});
        this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel5.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 126.2501F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel5.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.xrLabel5.StylePriority.UseBackColor = false;
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.StylePriority.UseTextAlignment = false;
        this.xrLabel5.Text = "xrLabel5";
        this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTable3
        // 
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 239.7918F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(369F, 26.83327F);
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(167)))), ((int)(((byte)(73)))));
        this.xrTableRow3.BorderColor = System.Drawing.Color.White;
        this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell6});
        this.xrTableRow3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableRow3.ForeColor = System.Drawing.Color.White;
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow3.StylePriority.UseBackColor = false;
        this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow3.Weight = 0.25842696629213485D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.BackColor = System.Drawing.Color.White;
        this.xrTableCell1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTableCell1.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell1.StylePriority.UseBackColor = false;
        this.xrTableCell1.StylePriority.UseFont = false;
        this.xrTableCell1.StylePriority.UseForeColor = false;
        this.xrTableCell1.Text = "Description";
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell1.Weight = 0.25688746520787614D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.BackColor = System.Drawing.Color.White;
        this.xrTableCell2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTableCell2.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell2.StylePriority.UseBackColor = false;
        this.xrTableCell2.StylePriority.UseFont = false;
        this.xrTableCell2.StylePriority.UseForeColor = false;
        this.xrTableCell2.Text = "Qty";
        this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell2.Weight = 0.079638343936502931D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.Color.White;
        this.xrTableCell3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTableCell3.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseFont = false;
        this.xrTableCell3.StylePriority.UseForeColor = false;
        this.xrTableCell3.Text = "Rate";
        this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell3.Weight = 0.11740600400931416D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.BackColor = System.Drawing.Color.White;
        this.xrTableCell4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTableCell4.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell4.StylePriority.UseBackColor = false;
        this.xrTableCell4.StylePriority.UseFont = false;
        this.xrTableCell4.StylePriority.UseForeColor = false;
        this.xrTableCell4.Text = "Amount";
        this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell4.Weight = 0.15433619705541163D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.BackColor = System.Drawing.Color.White;
        this.xrTableCell6.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
        this.xrTableCell6.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.StylePriority.UseBackColor = false;
        this.xrTableCell6.StylePriority.UseFont = false;
        this.xrTableCell6.StylePriority.UseForeColor = false;
        this.xrTableCell6.StylePriority.UseTextAlignment = false;
        this.xrTableCell6.Text = "Tax%";
        this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell6.Weight = 0.10594760522390678D;
        // 
        // xrLine1
        // 
        this.xrLine1.LineWidth = 3;
        this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 231.7918F);
        this.xrLine1.Name = "xrLine1";
        this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine1.SizeF = new System.Drawing.SizeF(369F, 8F);
        // 
        // lbShipCountry
        // 
        this.lbShipCountry.BackColor = System.Drawing.Color.White;
        this.lbShipCountry.BorderColor = System.Drawing.Color.White;
        this.lbShipCountry.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbShipCountry.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Bill_Date")});
        this.lbShipCountry.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbShipCountry.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbShipCountry.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 101.2501F);
        this.lbShipCountry.Name = "lbShipCountry";
        this.lbShipCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbShipCountry.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.lbShipCountry.StylePriority.UseBackColor = false;
        this.lbShipCountry.StylePriority.UseFont = false;
        this.lbShipCountry.StylePriority.UseTextAlignment = false;
        this.lbShipCountry.Text = "lbShipCountry";
        this.lbShipCountry.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.lbShipCountry.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // lbBillTo
        // 
        this.lbBillTo.BackColor = System.Drawing.Color.White;
        this.lbBillTo.BorderColor = System.Drawing.Color.White;
        this.lbBillTo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lbBillTo.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbBillTo.ForeColor = System.Drawing.Color.Black;
        this.lbBillTo.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 25F);
        this.lbBillTo.Name = "lbBillTo";
        this.lbBillTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbBillTo.SizeF = new System.Drawing.SizeF(107.559F, 25F);
        this.lbBillTo.StylePriority.UseBackColor = false;
        this.lbBillTo.StylePriority.UseFont = false;
        this.lbBillTo.StylePriority.UseTextAlignment = false;
        this.lbBillTo.Text = "Date:";
        this.lbBillTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbShipAddress
        // 
        this.lbShipAddress.BackColor = System.Drawing.Color.White;
        this.lbShipAddress.BorderColor = System.Drawing.Color.White;
        this.lbShipAddress.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbShipAddress.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.BillNoWPrefix")});
        this.lbShipAddress.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbShipAddress.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbShipAddress.LocationFloat = new DevExpress.Utils.PointFloat(107.5591F, 0F);
        this.lbShipAddress.Name = "lbShipAddress";
        this.lbShipAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbShipAddress.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.lbShipAddress.StylePriority.UseBackColor = false;
        this.lbShipAddress.StylePriority.UseFont = false;
        this.lbShipAddress.StylePriority.UseTextAlignment = false;
        this.lbShipAddress.Text = "lbShipAddress";
        this.lbShipAddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbShipCityRegionPostalCode
        // 
        this.lbShipCityRegionPostalCode.BackColor = System.Drawing.Color.White;
        this.lbShipCityRegionPostalCode.BorderColor = System.Drawing.Color.White;
        this.lbShipCityRegionPostalCode.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbShipCityRegionPostalCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Steward")});
        this.lbShipCityRegionPostalCode.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbShipCityRegionPostalCode.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbShipCityRegionPostalCode.LocationFloat = new DevExpress.Utils.PointFloat(109.0174F, 76.25014F);
        this.lbShipCityRegionPostalCode.Name = "lbShipCityRegionPostalCode";
        this.lbShipCityRegionPostalCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbShipCityRegionPostalCode.SizeF = new System.Drawing.SizeF(251.4409F, 25F);
        this.lbShipCityRegionPostalCode.StylePriority.UseBackColor = false;
        this.lbShipCityRegionPostalCode.StylePriority.UseFont = false;
        this.lbShipCityRegionPostalCode.StylePriority.UseTextAlignment = false;
        this.lbShipCityRegionPostalCode.Text = "lbShipCityRegionPostalCode";
        this.lbShipCityRegionPostalCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // GroupFooter0
        // 
        this.GroupFooter0.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
        this.GroupFooter0.HeightF = 353.9584F;
        this.GroupFooter0.Name = "GroupFooter0";
        this.GroupFooter0.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.GroupFooter0.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
        this.GroupFooter0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable2
        // 
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow15,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow1,
            this.xrTableRow7,
            this.xrTableRow4,
            this.xrTableRow8,
            this.xrTableRow5,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24});
        this.xrTable2.SizeF = new System.Drawing.SizeF(369F, 300F);
        this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow6.Weight = 0.2247191011235955D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.xrLabel17});
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell14.Weight = 0.71421561543301171D;
        // 
        // xrLabel16
        // 
        this.xrLabel16.BackColor = System.Drawing.Color.White;
        this.xrLabel16.BorderColor = System.Drawing.Color.White;
        this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel16.ForeColor = System.Drawing.Color.Black;
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(75F, 0F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(99.05717F, 18.29169F);
        this.xrLabel16.StylePriority.UseBackColor = false;
        this.xrLabel16.StylePriority.UseFont = false;
        this.xrLabel16.Text = "Total:";
        this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel17
        // 
        this.xrLabel17.BackColor = System.Drawing.Color.White;
        this.xrLabel17.BorderColor = System.Drawing.Color.White;
        this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Bill_Value")});
        this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel17.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(234.5242F, 1.708285F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel17.SizeF = new System.Drawing.SizeF(123.0815F, 18.29169F);
        this.xrLabel17.StylePriority.UseBackColor = false;
        this.xrLabel17.StylePriority.UseFont = false;
        this.xrLabel17.StylePriority.UseTextAlignment = false;
        this.xrLabel17.Text = "xrLabel17";
        this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrTableRow15
        // 
        this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15});
        this.xrTableRow15.Name = "xrTableRow15";
        this.xrTableRow15.Weight = 0.2247191011235955D;
        // 
        // xrTableCell15
        // 
        this.xrTableCell15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel7});
        this.xrTableCell15.Name = "xrTableCell15";
        this.xrTableCell15.Weight = 0.71421561543301171D;
        // 
        // xrLabel10
        // 
        this.xrLabel10.BackColor = System.Drawing.Color.White;
        this.xrLabel10.BorderColor = System.Drawing.Color.White;
        this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Discount")});
        this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel10.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(234.5242F, 1.335144E-05F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel10.SizeF = new System.Drawing.SizeF(124.4758F, 19.99991F);
        this.xrLabel10.StylePriority.UseBackColor = false;
        this.xrLabel10.StylePriority.UseFont = false;
        this.xrLabel10.StylePriority.UseTextAlignment = false;
        this.xrLabel10.Text = "xrLabel10";
        this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel9
        // 
        this.xrLabel9.BackColor = System.Drawing.Color.White;
        this.xrLabel9.BorderColor = System.Drawing.Color.White;
        this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "salon_sp_retailbillreport.DiscountPer")});
        this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel9.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(186.8333F, 1.335144E-05F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel9.SizeF = new System.Drawing.SizeF(47.69087F, 19.99989F);
        this.xrLabel9.StylePriority.UseBackColor = false;
        this.xrLabel9.StylePriority.UseFont = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.Text = "xrLabel9";
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel7
        // 
        this.xrLabel7.BackColor = System.Drawing.Color.White;
        this.xrLabel7.BorderColor = System.Drawing.Color.White;
        this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel7.ForeColor = System.Drawing.Color.Black;
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(74.80914F, 1.335144E-05F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(99.05717F, 18.29169F);
        this.xrLabel7.StylePriority.UseBackColor = false;
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.Text = "Discount@";
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTableRow17
        // 
        this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16});
        this.xrTableRow17.Name = "xrTableRow17";
        this.xrTableRow17.Weight = 0.2247191011235955D;
        // 
        // xrTableCell16
        // 
        this.xrTableCell16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel18});
        this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTableCell16.Name = "xrTableCell16";
        this.xrTableCell16.StylePriority.UseFont = false;
        this.xrTableCell16.Weight = 0.71421561543301171D;
        // 
        // xrLabel20
        // 
        this.xrLabel20.BackColor = System.Drawing.Color.White;
        this.xrLabel20.BorderColor = System.Drawing.Color.White;
        this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel20.ForeColor = System.Drawing.Color.Black;
        this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(74.80914F, 2.670288E-05F);
        this.xrLabel20.Name = "xrLabel20";
        this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel20.SizeF = new System.Drawing.SizeF(99.05717F, 18.29169F);
        this.xrLabel20.StylePriority.UseBackColor = false;
        this.xrLabel20.StylePriority.UseFont = false;
        this.xrLabel20.Text = "Vat@";
        this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel19
        // 
        this.xrLabel19.BackColor = System.Drawing.Color.White;
        this.xrLabel19.BorderColor = System.Drawing.Color.White;
        this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "salon_sp_retailbillreport.ServTaxRate")});
        this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel19.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(186.8333F, 2.670288E-05F);
        this.xrLabel19.Name = "xrLabel19";
        this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel19.SizeF = new System.Drawing.SizeF(47.69087F, 19.99989F);
        this.xrLabel19.StylePriority.UseBackColor = false;
        this.xrLabel19.StylePriority.UseFont = false;
        this.xrLabel19.StylePriority.UseTextAlignment = false;
        this.xrLabel19.Text = "xrLabel10";
        this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel18
        // 
        this.xrLabel18.BackColor = System.Drawing.Color.White;
        this.xrLabel18.BorderColor = System.Drawing.Color.White;
        this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.TaxAmount")});
        this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel18.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(234.5242F, 1.708309F);
        this.xrLabel18.Name = "xrLabel18";
        this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel18.SizeF = new System.Drawing.SizeF(124.4758F, 18.29169F);
        this.xrLabel18.StylePriority.UseBackColor = false;
        this.xrLabel18.StylePriority.UseFont = false;
        this.xrLabel18.StylePriority.UseTextAlignment = false;
        this.xrLabel18.Text = "xrLabel18";
        this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrTableRow18
        // 
        this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
        this.xrTableRow18.Name = "xrTableRow18";
        this.xrTableRow18.Weight = 0.2247191011235955D;
        // 
        // xrTableCell17
        // 
        this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.xrLabel22});
        this.xrTableCell17.Name = "xrTableCell17";
        this.xrTableCell17.Weight = 0.71421561543301171D;
        // 
        // xrLabel8
        // 
        this.xrLabel8.BackColor = System.Drawing.Color.White;
        this.xrLabel8.BorderColor = System.Drawing.Color.White;
        this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.SurVal")});
        this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel8.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(200.0224F, 0F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel8.SizeF = new System.Drawing.SizeF(158.9776F, 18.29171F);
        this.xrLabel8.StylePriority.UseBackColor = false;
        this.xrLabel8.StylePriority.UseFont = false;
        this.xrLabel8.StylePriority.UseTextAlignment = false;
        this.xrLabel8.Text = "xrLabel8";
        this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel22
        // 
        this.xrLabel22.BackColor = System.Drawing.Color.White;
        this.xrLabel22.BorderColor = System.Drawing.Color.White;
        this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel22.ForeColor = System.Drawing.Color.Black;
        this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(74.80914F, 0F);
        this.xrLabel22.Name = "xrLabel22";
        this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel22.SizeF = new System.Drawing.SizeF(99.05717F, 18.29171F);
        this.xrLabel22.StylePriority.UseBackColor = false;
        this.xrLabel22.StylePriority.UseFont = false;
        this.xrLabel22.Text = "Sur Charge:";
        this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.Weight = 0.2247191011235955D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel1});
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.Weight = 0.71421561543301171D;
        // 
        // xrLabel4
        // 
        this.xrLabel4.BackColor = System.Drawing.Color.White;
        this.xrLabel4.BorderColor = System.Drawing.Color.White;
        this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Round_Amount")});
        this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel4.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(202.875F, 1.708309F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(157.5833F, 18.29169F);
        this.xrLabel4.StylePriority.UseBackColor = false;
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.Text = "xrLabel4";
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel1
        // 
        this.xrLabel1.BackColor = System.Drawing.Color.White;
        this.xrLabel1.BorderColor = System.Drawing.Color.White;
        this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel1.ForeColor = System.Drawing.Color.Black;
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(74.80914F, 0F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(125.2133F, 18.29168F);
        this.xrLabel1.StylePriority.UseBackColor = false;
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.Text = "Round Off:";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTableRow7
        // 
        this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
        this.xrTableRow7.Name = "xrTableRow7";
        this.xrTableRow7.Weight = 0.2247191011235955D;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3});
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.Weight = 0.71421561543301171D;
        // 
        // xrLine3
        // 
        this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLine3.Name = "xrLine3";
        this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine3.SizeF = new System.Drawing.SizeF(369F, 8F);
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.Weight = 0.2247191011235955D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel31,
            this.xrLabel30});
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.Weight = 0.71421561543301171D;
        // 
        // xrLabel31
        // 
        this.xrLabel31.BackColor = System.Drawing.Color.White;
        this.xrLabel31.BorderColor = System.Drawing.Color.White;
        this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Net_Amount")});
        this.xrLabel31.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel31.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(200.0224F, 1.708298F);
        this.xrLabel31.Name = "xrLabel31";
        this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel31.SizeF = new System.Drawing.SizeF(157.5833F, 18.2917F);
        this.xrLabel31.StylePriority.UseBackColor = false;
        this.xrLabel31.StylePriority.UseFont = false;
        this.xrLabel31.StylePriority.UseTextAlignment = false;
        this.xrLabel31.Text = "xrLabel31";
        this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel30
        // 
        this.xrLabel30.BackColor = System.Drawing.Color.White;
        this.xrLabel30.BorderColor = System.Drawing.Color.White;
        this.xrLabel30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel30.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel30.ForeColor = System.Drawing.Color.Black;
        this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(36.55608F, 1.525879E-05F);
        this.xrLabel30.Name = "xrLabel30";
        this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel30.SizeF = new System.Drawing.SizeF(163.4664F, 18.29169F);
        this.xrLabel30.StylePriority.UseBackColor = false;
        this.xrLabel30.StylePriority.UseFont = false;
        this.xrLabel30.Text = "Net Amount:";
        this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTableRow8
        // 
        this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
        this.xrTableRow8.Name = "xrTableRow8";
        this.xrTableRow8.Weight = 0.2247191011235955D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine4});
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.Weight = 0.71421561543301171D;
        // 
        // xrLine4
        // 
        this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10F);
        this.xrLine4.Name = "xrLine4";
        this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine4.SizeF = new System.Drawing.SizeF(369F, 8F);
        // 
        // xrTableRow5
        // 
        this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
        this.xrTableRow5.Name = "xrTableRow5";
        this.xrTableRow5.Weight = 0.2247191011235955D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel32});
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.Weight = 0.71421561543301171D;
        // 
        // xrLabel32
        // 
        this.xrLabel32.BackColor = System.Drawing.Color.White;
        this.xrLabel32.BorderColor = System.Drawing.Color.White;
        this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "salon_sp_retailbillreport.OutstandingAmount")});
        this.xrLabel32.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel32.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 1.708298F);
        this.xrLabel32.Name = "xrLabel32";
        this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel32.SizeF = new System.Drawing.SizeF(349F, 18.29169F);
        this.xrLabel32.StylePriority.UseBackColor = false;
        this.xrLabel32.StylePriority.UseFont = false;
        this.xrLabel32.StylePriority.UseTextAlignment = false;
        this.xrLabel32.Text = "xrLabel4";
        this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrTableRow19
        // 
        this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18});
        this.xrTableRow19.Name = "xrTableRow19";
        this.xrTableRow19.Weight = 0.2247191011235955D;
        // 
        // xrTableCell18
        // 
        this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine5});
        this.xrTableCell18.Name = "xrTableCell18";
        this.xrTableCell18.Weight = 0.71421561543301171D;
        // 
        // xrLine5
        // 
        this.xrLine5.LineWidth = 3;
        this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 2.000046F);
        this.xrLine5.Name = "xrLine5";
        this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine5.SizeF = new System.Drawing.SizeF(369F, 8F);
        // 
        // xrTableRow20
        // 
        this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter1});
        this.xrTableRow20.Name = "xrTableRow20";
        this.xrTableRow20.Weight = 0.2247191011235955D;
        // 
        // lblFooter1
        // 
        this.lblFooter1.CanShrink = true;
        this.lblFooter1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer1")});
        this.lblFooter1.Name = "lblFooter1";
        this.lblFooter1.StylePriority.UseTextAlignment = false;
        this.lblFooter1.Text = "lblFooter1";
        this.lblFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter1.Weight = 0.71421561543301171D;
        this.lblFooter1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // xrTableRow21
        // 
        this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter2});
        this.xrTableRow21.Name = "xrTableRow21";
        this.xrTableRow21.Weight = 0.2247191011235955D;
        // 
        // lblFooter2
        // 
        this.lblFooter2.CanShrink = true;
        this.lblFooter2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer2")});
        this.lblFooter2.Name = "lblFooter2";
        this.lblFooter2.StylePriority.UseTextAlignment = false;
        this.lblFooter2.Text = "lblFooter2";
        this.lblFooter2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter2.Weight = 0.71421561543301171D;
        this.lblFooter2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // xrTableRow22
        // 
        this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter3});
        this.xrTableRow22.Name = "xrTableRow22";
        this.xrTableRow22.Weight = 0.2247191011235955D;
        // 
        // lblFooter3
        // 
        this.lblFooter3.CanShrink = true;
        this.lblFooter3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer3")});
        this.lblFooter3.Name = "lblFooter3";
        this.lblFooter3.StylePriority.UseTextAlignment = false;
        this.lblFooter3.Text = "lblFooter3";
        this.lblFooter3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter3.Weight = 0.71421561543301171D;
        this.lblFooter3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // xrTableRow23
        // 
        this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter4});
        this.xrTableRow23.Name = "xrTableRow23";
        this.xrTableRow23.Weight = 0.2247191011235955D;
        // 
        // lblFooter4
        // 
        this.lblFooter4.CanShrink = true;
        this.lblFooter4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer4")});
        this.lblFooter4.Name = "lblFooter4";
        this.lblFooter4.StylePriority.UseTextAlignment = false;
        this.lblFooter4.Text = "lblFooter4";
        this.lblFooter4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter4.Weight = 0.71421561543301171D;
        this.lblFooter4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // xrTableRow24
        // 
        this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblFooter5});
        this.xrTableRow24.Name = "xrTableRow24";
        this.xrTableRow24.Weight = 0.2247191011235955D;
        // 
        // lblFooter5
        // 
        this.lblFooter5.CanShrink = true;
        this.lblFooter5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.footer5")});
        this.lblFooter5.Name = "lblFooter5";
        this.lblFooter5.StylePriority.UseTextAlignment = false;
        this.lblFooter5.Text = "lblFooter5";
        this.lblFooter5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblFooter5.Weight = 0.71421561543301171D;
        this.lblFooter5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.lblHeader4,
            this.lblHeader5,
            this.lblHeader3,
            this.lbHeader1,
            this.lblHeader2});
        this.PageHeader.HeightF = 150.0001F;
        this.PageHeader.Name = "PageHeader";
        this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLabel11
        // 
        this.xrLabel11.BackColor = System.Drawing.Color.White;
        this.xrLabel11.BorderColor = System.Drawing.Color.White;
        this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel11.CanShrink = true;
        this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 8F);
        this.xrLabel11.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 125.0001F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(369F, 25F);
        this.xrLabel11.StylePriority.UseBackColor = false;
        this.xrLabel11.StylePriority.UseFont = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrLabel11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // lblHeader4
        // 
        this.lblHeader4.BackColor = System.Drawing.Color.White;
        this.lblHeader4.BorderColor = System.Drawing.Color.White;
        this.lblHeader4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblHeader4.CanShrink = true;
        this.lblHeader4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Header4")});
        this.lblHeader4.Font = new System.Drawing.Font("Tahoma", 8F);
        this.lblHeader4.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lblHeader4.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 75F);
        this.lblHeader4.Name = "lblHeader4";
        this.lblHeader4.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lblHeader4.SizeF = new System.Drawing.SizeF(369F, 25F);
        this.lblHeader4.StylePriority.UseBackColor = false;
        this.lblHeader4.StylePriority.UseFont = false;
        this.lblHeader4.StylePriority.UseTextAlignment = false;
        this.lblHeader4.Text = "lblHeader4";
        this.lblHeader4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblHeader4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // lblHeader5
        // 
        this.lblHeader5.BackColor = System.Drawing.Color.White;
        this.lblHeader5.BorderColor = System.Drawing.Color.White;
        this.lblHeader5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblHeader5.CanShrink = true;
        this.lblHeader5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Header5")});
        this.lblHeader5.Font = new System.Drawing.Font("Tahoma", 8F);
        this.lblHeader5.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lblHeader5.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 100.0001F);
        this.lblHeader5.Name = "lblHeader5";
        this.lblHeader5.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lblHeader5.SizeF = new System.Drawing.SizeF(369F, 25F);
        this.lblHeader5.StylePriority.UseBackColor = false;
        this.lblHeader5.StylePriority.UseFont = false;
        this.lblHeader5.StylePriority.UseTextAlignment = false;
        this.lblHeader5.Text = "lblHeader5";
        this.lblHeader5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblHeader5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // lblHeader3
        // 
        this.lblHeader3.BackColor = System.Drawing.Color.White;
        this.lblHeader3.BorderColor = System.Drawing.Color.White;
        this.lblHeader3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblHeader3.CanShrink = true;
        this.lblHeader3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.Header3")});
        this.lblHeader3.Font = new System.Drawing.Font("Tahoma", 8F);
        this.lblHeader3.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lblHeader3.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 50F);
        this.lblHeader3.Name = "lblHeader3";
        this.lblHeader3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lblHeader3.SizeF = new System.Drawing.SizeF(369F, 25F);
        this.lblHeader3.StylePriority.UseBackColor = false;
        this.lblHeader3.StylePriority.UseFont = false;
        this.lblHeader3.StylePriority.UseTextAlignment = false;
        this.lblHeader3.Text = "lblHeader3";
        this.lblHeader3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblHeader3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // lbHeader1
        // 
        this.lbHeader1.BackColor = System.Drawing.Color.White;
        this.lbHeader1.BorderColor = System.Drawing.Color.White;
        this.lbHeader1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbHeader1.CanShrink = true;
        this.lbHeader1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.header1")});
        this.lbHeader1.Font = new System.Drawing.Font("Tahoma", 8F);
        this.lbHeader1.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbHeader1.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 0F);
        this.lbHeader1.Name = "lbHeader1";
        this.lbHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbHeader1.SizeF = new System.Drawing.SizeF(369F, 25F);
        this.lbHeader1.StylePriority.UseBackColor = false;
        this.lbHeader1.StylePriority.UseFont = false;
        this.lbHeader1.StylePriority.UseTextAlignment = false;
        this.lbHeader1.Text = "lbHeader1";
        this.lbHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lbHeader1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // lblHeader2
        // 
        this.lblHeader2.BackColor = System.Drawing.Color.White;
        this.lblHeader2.BorderColor = System.Drawing.Color.White;
        this.lblHeader2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblHeader2.CanShrink = true;
        this.lblHeader2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_retailbillreport.header2")});
        this.lblHeader2.Font = new System.Drawing.Font("Tahoma", 8F);
        this.lblHeader2.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lblHeader2.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 25F);
        this.lblHeader2.Name = "lblHeader2";
        this.lblHeader2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lblHeader2.SizeF = new System.Drawing.SizeF(369F, 25F);
        this.lblHeader2.StylePriority.UseBackColor = false;
        this.lblHeader2.StylePriority.UseFont = false;
        this.lblHeader2.StylePriority.UseTextAlignment = false;
        this.lblHeader2.Tag = "header";
        this.lblHeader2.Text = "lblHeader2";
        this.lblHeader2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblHeader2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lbShipCountry_BeforePrint);
        // 
        // topMarginBand1
        // 
        this.topMarginBand1.HeightF = 4.166667F;
        this.topMarginBand1.Name = "topMarginBand1";
        // 
        // BottomMargin
        // 
        this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo3});
        this.BottomMargin.HeightF = 75F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrPageInfo3
        // 
        this.xrPageInfo3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrPageInfo3.Format = "{0:g}";
        this.xrPageInfo3.LocationFloat = new DevExpress.Utils.PointFloat(222.0242F, 9.999974F);
        this.xrPageInfo3.Name = "xrPageInfo3";
        this.xrPageInfo3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
        this.xrPageInfo3.SizeF = new System.Drawing.SizeF(136.9758F, 16F);
        this.xrPageInfo3.StylePriority.UseFont = false;
        this.xrPageInfo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1});
        this.ReportHeader.Name = "ReportHeader";
        // 
        // xrPictureBox1
        // 
        this.xrPictureBox1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Image", null, "salon_sp_retailbillreport.Logo")});
        this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(75F, 12.5F);
        this.xrPictureBox1.Name = "xrPictureBox1";
        this.xrPictureBox1.SizeF = new System.Drawing.SizeF(213.2917F, 84.79165F);
        // 
        // dsBilling1
        // 
        this.dsBilling1.DataSetName = "dsBilling";
        this.dsBilling1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // pos_sp_retailbillreportTableAdapter1
        // 
        this.pos_sp_retailbillreportTableAdapter1.ClearBeforeFill = true;
        // 
        // dsBilling2
        // 
        this.dsBilling2.DataSetName = "dsBilling";
        this.dsBilling2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // RetailBillReport
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.GroupHeader0,
            this.Detail,
            this.GroupFooter0,
            this.PageHeader,
            this.BottomMargin,
            this.topMarginBand1,
            this.ReportHeader});
        this.DataAdapter = this.pos_sp_retailbillreportTableAdapter1;
        this.DataMember = "pos_sp_retailbillreport";
        this.DataSource = this.dsBilling2;
        this.DisplayName = "Invoice";
        this.Font = new System.Drawing.Font("Times New Roman", 10.75F);
        this.Margins = new System.Drawing.Printing.Margins(3, 4, 4, 75);
        this.PageWidth = 376;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.ReportPrintOptions.DetailCountAtDesignTime = 4;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsBilling1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsBilling2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion

    private void SetCustomStyle(XRLabel label)
    {

        //if (label.Name == "lbHeader1")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (HeaderStyle1[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (HeaderStyle1[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(HeaderStyle1[0], Convert.ToInt16(HeaderStyle1[1]), fontStyle);

        //}

        //if (label.Name == "xrLabel11")
        //{
        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (HeaderStyle4[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (HeaderStyle4[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(HeaderStyle4[0], Convert.ToInt16(HeaderStyle4[1]), fontStyle);
        //}


        //if (label.Name == "lblHeader2")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (HeaderStyle2[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (HeaderStyle2[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(HeaderStyle2[0], Convert.ToInt16(HeaderStyle2[1]), fontStyle);


        //}



        //if (label.Name == "lblHeader3")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (HeaderStyle3[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (HeaderStyle3[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }

        //    label.Font = new Font(HeaderStyle3[0], Convert.ToInt16(HeaderStyle3[1]), fontStyle);


        //}



        //if (label.Name == "lblHeader4")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (HeaderStyle4[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (HeaderStyle4[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(HeaderStyle4[0], Convert.ToInt16(HeaderStyle4[1]), fontStyle);


        //}


        //if (label.Name == "lblHeader5")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (HeaderStyle5[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (HeaderStyle5[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(HeaderStyle5[0], Convert.ToInt16(HeaderStyle5[1]), fontStyle);

        //}



        //if (label.Name == "lblFooter1")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (FooterStyle1[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (FooterStyle1[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(FooterStyle1[0], Convert.ToInt16(FooterStyle1[1]), fontStyle);

        //}



        //if (label.Name == "lblFooter2")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (FooterStyle2[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (FooterStyle2[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(FooterStyle2[0], Convert.ToInt16(FooterStyle2[1]), fontStyle);


        //}



        //if (label.Name == "lblFooter3")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (FooterStyle3[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (FooterStyle3[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }

        //    label.Font = new Font(FooterStyle3[0], Convert.ToInt16(FooterStyle3[1]), fontStyle);


        //}



        //if (label.Name == "lblFooter4")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (FooterStyle4[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (FooterStyle4[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(FooterStyle4[0], Convert.ToInt16(FooterStyle4[1]), fontStyle);


        //}


        //if (label.Name == "lblFooter5")
        //{

        //    FontStyle fontStyle = FontStyle.Regular;

        //    if (FooterStyle5[2] == "True")
        //    {
        //        fontStyle |= FontStyle.Bold;
        //    }
        //    if (FooterStyle5[3] == "True")
        //    {
        //        fontStyle |= FontStyle.Italic;
        //    }


        //    label.Font = new Font(FooterStyle5[0], Convert.ToInt16(FooterStyle5[1]), fontStyle);

        //}









    }

    private void lbShipCountry_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        SetCustomStyle((XRLabel)sender);
    }
}
