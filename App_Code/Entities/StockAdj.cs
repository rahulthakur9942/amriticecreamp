﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StockAdj
/// </summary>
public class StockAdj
{
    public int Ref_No { get; set; }
    public DateTime Ref_Date { get; set; }
    public string strrefdate { get { return Ref_Date.ToString("d"); } }
    public string Code { get; set; }
    public string IName { get; set; }
    public decimal MRP { get; set; }
    public decimal ActualStock { get; set; }
    public decimal StockAdjust { get; set; }
    public decimal StockAdjusted { get; set; }
    public int UserNo { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public int Godown_ID { get; set; }
    public int BranchId { get; set; }
    public string Godown { get; set; }

	public StockAdj()
	{
        Ref_No = 0;
        Ref_Date = DateTime.Now;
        Code = string.Empty;
        IName = string.Empty;
        MRP = 0;
        ActualStock = 0;
        StockAdjust = 0;
        StockAdjusted = 0;
        UserNo = 0;
        Rate = 0;
        Amount = 0;
        Godown_ID = 0;
        BranchId = 0;
        Godown = string.Empty; 
	}
}