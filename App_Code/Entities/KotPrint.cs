﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KotPrint
/// </summary>
public class KotPrint
{
    public int UserNo { get; set; }
    public string BillNowPrefix  { get; set; }
    public string  DepartmentName { get; set; }
  
	public KotPrint()
	{
        UserNo = 0;
        BillNowPrefix = string.Empty;
        DepartmentName = string.Empty;
	}
}