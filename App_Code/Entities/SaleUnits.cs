﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SaleUnits
/// </summary>
public class SaleUnits
{

    public int Unit_Id { get; set; }
    public string Unit_Name { get; set; }
    public bool IsActive { get; set; }
    public int UserId { get; set; }
	public SaleUnits()
	{
        Unit_Id = 0;
        Unit_Name = "";
        IsActive = true;
        UserId = 0;
	}
}