﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Departments
/// </summary>
public class Departments
{

    public int Prop_ID { get; set; }
    public string Prop_Name { get; set; }
    public bool IsActive { get; set; }
    public bool showinmenu { get; set; }
    public int UserId { get; set; }

    public int posid { get; set; }
    public Departments()
	{
        Prop_ID = 0;
        Prop_Name = "";
        IsActive = true;
        UserId = 0;
        posid = 0;

    }
}