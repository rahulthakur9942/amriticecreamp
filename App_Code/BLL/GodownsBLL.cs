﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
public class GodownsBLL
{
    public List<Godowns> GetAll()
    {
        List<Godowns> GodownList = new List<Godowns>();
        
        SqlDataReader dr = null;
        try
        {
            dr = new GodownsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Godowns objGodowns = new Godowns()
                    {
                        Godown_Name = dr["Godown_Name"].ToString(),
                        Divison = Convert.ToString(dr["Divison"]),
                        FinacialYear = Convert.ToString(dr["FinacialYear"]),
                        Godown_Id = Convert.ToInt16(dr["Godown_ID"]),
                        Caption = Convert.ToString(dr["Caption"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    GodownList.Add(objGodowns);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return GodownList;

    }


    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new GodownsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    //if (dr["DGID"].ToString() == dr["GodownId"].ToString())
                    //{
                    //    strBuilder.Append(string.Format("<option selected=selected value={0}>{1}</option>", dr["Godown_Id"].ToString(), dr["Godown_Name"].ToString()));
                    //}
                    //else
                    //{
                        strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Godown_Id"].ToString(), dr["Godown_Name"].ToString()));
                    
                   // }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return strBuilder.ToString();

    }


    public string GetOptionsGodownFrom(int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new GodownsDAL().GetGodownByStock(BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Godown_Id"].ToString(), dr["Godown_Name"].ToString()));


                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Godowns objGodowns)
    {

        return new GodownsDAL().InsertUpdate(objGodowns);
    }

    public Int32 DeleteGodown(Godowns objGodown)
    {
        return new GodownsDAL().Delete(objGodown);
    }

}