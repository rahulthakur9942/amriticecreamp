﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for NotificationBLL
/// </summary>
public class NotificationBLL
{
    public Int32 InsertUpdate(Notification objNotify)
    {
        return new NotificationDAL().InsertUpdate(objNotify);
    }

    public void GetSettings(Notification objNotification,int BranchId)
    {
   
        SqlDataReader dr = null;
        try
        {
            dr = new NotificationDAL().GetAll(BranchId);
            if (dr.HasRows)
            {
                dr.Read();

                objNotification.Notification1 = Convert.ToString(dr["Notification1"]);
                objNotification.Notification2 = Convert.ToString(dr["Notification2"]);
                objNotification.Declaration = Convert.ToString(dr["Declaration"]);
                objNotification.Range = Convert.ToString(dr["Range"]);
                objNotification.Division = Convert.ToString(dr["Division"]);
                objNotification.CommissionRate = Convert.ToString(dr["CommissionRate"]);
                objNotification.TINNO = Convert.ToString(dr["TINNO"]);
                objNotification.PANNO = Convert.ToString(dr["PANNO"]);
                objNotification.CERegn = Convert.ToString(dr["CERegn"]);
                objNotification.BranchId = Convert.ToInt32(dr["BranchId"]);


              
              
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}