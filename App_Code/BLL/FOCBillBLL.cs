﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FOCBillBLL
/// </summary>
public class FOCBillBLL
{



    public Int32 DeleteBill(Bill objBill)
    {
        return new FOCBillDAL().Delete(objBill);
    }
    public List<Bill> GetFOCBillByDate(DateTime FromDate, DateTime ToDate, int BranchId)
    {
        List<Bill> BillList = new List<Bill>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new FOCBillDAL().GetByDate(FromDate, ToDate,BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bill objBill = new Bill()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString()),
                        Bill_No = Convert.ToInt32(dr["Bill_No"].ToString()),
                        Bill_Prefix = Convert.ToString(dr["Bill_Prefix"]),
                        Customer_ID = Convert.ToString(dr["Customer_ID"]),
                        Customer_Name = Convert.ToString(dr["Customer_Name"].ToString()),
                        Bill_Value = Convert.ToDecimal(dr["Bill_Value"]),
                        BillMode = Convert.ToString(dr["BillMode"]),
                        Less_Dis_Amount = Convert.ToDecimal(dr["Less_Dis_Amount"]),
                        Add_Tax_Amount = Convert.ToDecimal(dr["Add_Tax_Amount"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        CreditBank = Convert.ToString(dr["CreditBank"]),
                        UserNO = Convert.ToInt32(dr["UserNO"]),
                        Bill_Type = Convert.ToInt32(dr["Bill_Type"]),
                        Cash_Amount = Convert.ToDecimal(dr["Cash_Amount"]),
                        Credit_Amount = Convert.ToDecimal(dr["Credit_Amount"]),
                        CrCard_Amount = Convert.ToDecimal(dr["CrCard_Amount"]),
                        CashCust_Code = Convert.ToInt32(dr["CashCust_Code"]),
                        CashCust_Name = Convert.ToString(dr["CashCust_Name"]),
                        Round_Amount = Convert.ToDecimal(dr["Round_Amount"]),
                        Passing = Convert.ToBoolean(dr["Passing"]),
                        Bill_Printed = Convert.ToBoolean(dr["Bill_Printed"]),

                        Tax_Per = Convert.ToDecimal(dr["Tax_Per"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]),
                        R_amount = Convert.ToDecimal(dr["R_amount"]),
                        tokenno = Convert.ToInt32(dr["tokenno"]),
                        tableno = Convert.ToInt32(dr["tableno"]),
                        remarks = Convert.ToString(dr["remarks"]),
                        servalue = Convert.ToDecimal(dr["servalue"]),
                        ReceiviedGRNNo = Convert.ToInt32(dr["ReceiviedGRNNo"]),
                        DiscountPer = Convert.ToDecimal(dr["DisPer"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                         CashCustAddress = Convert.ToString(dr["CashCustAddress"]) 
                    };


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public List<Product> GetFOCBillByBillNowPrefix(string BillNowPrefix,int BranchId)
    {
        List<Product> lstProduct = new List<Product>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new FOCBillDAL().GetFOCBillDetailByBillNo(BillNowPrefix, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Product objProduct = new Product()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),
                        Tax_Code = Convert.ToDecimal(dr["Tax_Code"]),
                        Sale_Rate = Convert.ToDecimal(dr["Rate"]),
                        SurVal = Convert.ToDecimal(dr["SurVal"]),
                        Item_Remarks = dr["Item_Remarks"].ToString(),
                    };
                    lstProduct.Add(objProduct);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return lstProduct;

    }

    public Int32 Insert(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new FOCBillDAL().Insert(objBill, dt, dt1);
    }


    public Int32 Update(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new FOCBillDAL().Update(objBill, dt, dt1);
    }

}