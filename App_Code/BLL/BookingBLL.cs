﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for BookingBLL
/// </summary>
public class BookingBLL
{
    public Int32 InsertUpdate(Booking objBooking, DataTable dt)
    {
        return new BookingDAL().InsertUpdate(objBooking, dt);
    }
    public Int32 InsertUpdateDispatch(OrderDispatch objOrderDispatch, DataTable dt,DataTable dt1)
    {
        return new BookingDAL().InsertUpdateDispatch(objOrderDispatch, dt,dt1);
    }


    public List<Booking> GetOrderBookingByDate(DateTime FromDate, DateTime ToDate,int BranchId)
    {
        List<Booking> OrderBookingList = new List<Booking>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BookingDAL().GetByDate(FromDate, ToDate,BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Booking objBooking = new Booking()
                    {
                        OrderNo = Convert.ToInt32(dr["OrderNo"].ToString()),
                        ManualOrderNo = dr["ManualOrderNo"].ToString(),
                        OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString()),
                        Customer_ID = dr["Customer_ID"].ToString(),
                        CustomerName = Convert.ToString(dr["CustomerName"]),
                       
                        Address = Convert.ToString(dr["Address"]),
                        DeliveryType = Convert.ToString(dr["DeliveryType"].ToString()),
                        DeliveryTime = Convert.ToDateTime(dr["DeliveryTime"]),
                        DeliveryAddress = Convert.ToString(dr["DeliveryAddress"]),
                        Advance = Convert.ToDecimal(dr["Advance"]),
                        PaymentMode = Convert.ToString(dr["AdvMode"]),
                        LeftPayRecd = Convert.ToDecimal(dr["LeftPayRecd"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        VatAmount = Convert.ToDecimal(dr["VatAmount"]),
                        DisPer = Convert.ToDecimal(dr["DiscountPer"]),
                        Employee = Convert.ToInt16(dr["Ecode"]),
                    };
                    OrderBookingList.Add(objBooking);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return OrderBookingList;

    }




    public List<BookingDetail> GetBookingByOrderNo(Booking objBooking,int BranchId)
    {

        SqlDataReader dr = null;
        List<BookingDetail> objDetail = new List<BookingDetail>();
        try
        {
            dr = new BookingDAL().GetByOrderNo(objBooking.OrderNo,BranchId);



            if (dr.HasRows)
            {
                dr.Read();

                objBooking.OrderNo = Convert.ToInt32(dr["OrderNo"].ToString());
                objBooking.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
                objBooking.Customer_ID = Convert.ToString(dr["Customer_ID"].ToString());
                objBooking.CustomerName = Convert.ToString(dr["CustomerName"]);
                objBooking.MobileNo = Convert.ToString(dr["Contact_No"]);
                objBooking.Address = dr["Address"].ToString();
                objBooking.DeliveryType = Convert.ToString(dr["DeliveryType"]);
                objBooking.DeliveryTime = Convert.ToDateTime(dr["DeliveryTime"]);
                objBooking.DeliveryAddress = Convert.ToString(dr["DeliveryAddress"].ToString());
                objBooking.Advance = Convert.ToDecimal(dr["Advance"]);
                objBooking.PaymentMode = Convert.ToString(dr["AdvMode"]);
                objBooking.LeftPayRecd = Convert.ToDecimal(dr["LeftPayRecd"]);
                objBooking.Remarks = Convert.ToString(dr["Remarks"]);
                objBooking.NetAmount = Convert.ToDecimal(dr["NetAmount"]);
                objBooking.DisAmt = Convert.ToDecimal(dr["DisAmt"]);
                objBooking.VatAmount = Convert.ToDecimal(dr["VatAmount"]);
                objBooking.Employee = Convert.ToInt32(dr["Ecode"]);
                objBooking.DisPer = Convert.ToDecimal(dr["DiscountPer"]);
                objBooking.DelTime = Convert.ToString(dr["DelTime"]);
                objBooking.CreditCardNumber = Convert.ToString(dr["CreditCardNumber"]);
                objBooking.SerialNo = Convert.ToString(dr["SerialNo"]);
                objBooking.ManualOrderNo = Convert.ToString(dr["ManualOrderNo"]);


               // objBooking.AdvancePaid = Convert.ToDecimal(dr["AdvancePaid"]);
            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    BookingDetail objBD = new BookingDetail();


                    objBD.Code = Convert.ToString(dr["Item_Code"].ToString());
                    objBD.Name = Convert.ToString(dr["Item_Name"].ToString());
                    objBD.Weight = Convert.ToDecimal(dr["Weight"]);
                    objBD.Qty = Convert.ToDecimal(dr["Qty"]);
                    objBD.MRP = Convert.ToDecimal(dr["MRP"]);
                    objBD.Rate = Convert.ToDecimal(dr["Rate"]);
                    objBD.Amount = Convert.ToDecimal(dr["Amount"]);
                    objBD.Vat = Convert.ToDecimal(dr["VatAmt"]);
                    objBD.TaxPer = Convert.ToDecimal(dr["Tax_Code"]);
                    objBD.Surcharge = Convert.ToDecimal(dr["SurValue"]);
                    objDetail.Add(objBD);
                
                
                }
            
            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }



    public List<BookingDetail> GetDispatchDetailByOrderNo(Booking objBooking,int BranchId)
    {

        SqlDataReader dr = null;
        List<BookingDetail> objDetail = new List<BookingDetail>();
        try
        {
            dr = new BookingDAL().GetDispatchDetailByOrderNo(objBooking.OrderNo, BranchId);



            if (dr.HasRows)
            {
                dr.Read();

                objBooking.OrderNo = Convert.ToInt32(dr["OrderNo"].ToString());

                objBooking.CCODE = Convert.ToString(dr["CCODE"]);
                objBooking.CNAME = Convert.ToString(dr["CNAME"]);


                objBooking.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
                objBooking.Customer_ID = Convert.ToString(dr["Customer_ID"].ToString());
                objBooking.CustomerName = Convert.ToString(dr["CustomerName"]);
                objBooking.MobileNo = Convert.ToString(dr["Contact_No"]);
                objBooking.Address = dr["Address"].ToString();
                objBooking.DeliveryType = Convert.ToString(dr["DeliveryType"]);
                objBooking.DeliveryTime = Convert.ToDateTime(dr["DeliveryTime"]);
                objBooking.DeliveryAddress = Convert.ToString(dr["DeliveryAddress"].ToString());
                objBooking.Advance = Convert.ToDecimal(dr["AdvancePaid"]);
                objBooking.PaymentMode = Convert.ToString(dr["BillMode"]);
               
                objBooking.LeftPayRecd = Convert.ToDecimal(dr["LeftPayRecd"]);
                objBooking.Remarks = Convert.ToString(dr["Remarks"]);
                objBooking.NetAmount = Convert.ToDecimal(dr["NetAmount"]);
                objBooking.DisAmt = Convert.ToDecimal(dr["DisAmt"]);
                objBooking.VatAmount = Convert.ToDecimal(dr["VatAmount"]);
                objBooking.DisPer = Convert.ToDecimal(dr["DiscountPer"]);
                objBooking.Ecode = Convert.ToInt32(dr["Ecode"]);
                objBooking.AdvancePaid = Convert.ToDecimal(dr["AdvancePaid"]);
                objBooking.TotalDispatchedAmt = Convert.ToDecimal(dr["TotalDispatchedAmt"]);
               
            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    BookingDetail objBD = new BookingDetail();


                    objBD.Code = Convert.ToString(dr["Item_Code"].ToString());
                    objBD.Name = Convert.ToString(dr["Item_Name"].ToString());
                    objBD.Weight = Convert.ToDecimal(dr["Weight"]);
                    objBD.Qty = Convert.ToDecimal(dr["Qty"]);
                    objBD.MRP = Convert.ToDecimal(dr["MRP"]);
                    objBD.Rate = Convert.ToDecimal(dr["Rate"]);
                    objBD.Amount = Convert.ToDecimal(dr["Amount"]);
                    objBD.Vat = Convert.ToDecimal(dr["VatAmt"]);
                    objBD.TaxPer = Convert.ToDecimal(dr["Tax_Code"]);
                    objBD.Surcharge = Convert.ToDecimal(dr["SurValue"]);
                    objBD.DisptchdQty = Convert.ToDecimal(dr["DispatchedQty"]);
                    objDetail.Add(objBD);


                }

            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }
}