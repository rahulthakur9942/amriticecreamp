﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for RolesBLL
/// </summary>
public class RolesBLL
{
    public DataSet GetAllDataSet()
    {
        return new RolesDAL().GetAllDataSet();

    }

    public string GetRolesByDesignationAndPage(int DesignationId, int PageId)
    {
        return new RolesDAL().GetRolesByDesignationAndPage(DesignationId, PageId);
    }

    public string GetRolesByDesignation(int DesignationId)
    {
        StringBuilder strBuilder = new StringBuilder();
        DataSet ds = new RolesDAL().GetRolesByDesignation(DesignationId);
        strBuilder.Append("<table width='100%'  style='border-collapse:collapse;'>");
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            bool IsFirstTime = true;
            if (IsFirstTime)
            {
                strBuilder.Append("<tr><td colspan='100%' style='background-color:gray;color:White;font-weight:bold'><img name='imgPlusMinus' onclick='javascript:ShowHide(" + ds.Tables[0].Rows[i]["MembershipPageId"].ToString() + ")' id='imgShowHide" + ds.Tables[0].Rows[i]["MembershipPageId"].ToString() + "' src='images/icoplus.gif'>&nbsp;" + ds.Tables[0].Rows[i]["Title"].ToString() + "</td></tr><tr ><td><table id='tr" + ds.Tables[0].Rows[i]["MembershipPageId"].ToString() + "' width='100%' style='display:none'>");
                IsFirstTime = false;
            }


            DataView dvRoles = ds.Tables[1].DefaultView;
            dvRoles.RowFilter = "MembershipPageId=" + ds.Tables[0].Rows[i]["MembershipPageId"].ToString();
            DataTable dtRoles = dvRoles.ToTable();

            for (int j = 0; j < dtRoles.Rows.Count; j++)
            {
                string checkedStatus = dtRoles.Rows[j]["RoleInDesignation"].ToString() == "1" ? "checked=checked" : "";

                if (j % 2 == 0)
                {
                    strBuilder.Append("<tr style='background-color:Silver;color:Black;border-bottom:solid 1px gray;font-weight:bold'><td><input type='checkbox'  " + checkedStatus + "  name='Roles' value='" + dtRoles.Rows[j]["RoleId"].ToString() + "'   pid='" + dtRoles.Rows[j]["MembershipPageId"].ToString() + "'   /></td><td>" + dtRoles.Rows[j]["Title"].ToString() + "</td></tr>");
                }
                else
                {
                    strBuilder.Append("<tr  style='background-color:#E4E4E4;color:Black;border-bottom:solid 1px gray;font-weight:bold'><td><input type='checkbox'   " + checkedStatus + "  value='" + dtRoles.Rows[j]["RoleId"].ToString() + "'   pid='" + dtRoles.Rows[j]["MembershipPageId"].ToString() + "'    name='Roles'/></td><td>" + dtRoles.Rows[j]["Title"].ToString() + "</td></tr>");
                }
                if (j == dtRoles.Rows.Count - 1)
                {
                    strBuilder.Append("</td></tr></table>");
                }

            }
        }
        return strBuilder.ToString();


    }


    public List<Roles> GetAll()
    {
        List<Roles> RoleList = new List<Roles>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new RolesDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Roles objRoles = new Roles()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        RoleId = Convert.ToInt16(dr["RoleId"]),

                    };
                    RoleList.Add(objRoles);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return RoleList;

    }




    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new RolesDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["RoleId"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose(); 
        }
        return strBuilder.ToString();

    }


    public Int16 AssignRoles(int DesignationId, DataTable dt)
    {
        return new RolesDAL().AssignRoles(DesignationId, dt);
    }


    public Int16 InsertUpdate(Roles objRoles)
    {

        return new RolesDAL().InsertUpdate(objRoles);
    }

}