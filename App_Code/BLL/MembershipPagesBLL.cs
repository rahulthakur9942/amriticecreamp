﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
public class MembershipPagesBLL
{
 


     public List<MembershipPages> GetAll()
     {
         List<MembershipPages> pageList = new List<MembershipPages>();
         SqlParameter[] objParam = new SqlParameter[0];
         SqlDataReader dr = null;
         try
         {
             dr = new MembershipPagesDAL().GetAll();
             if (dr.HasRows)
             {
                 while (dr.Read())
                 {
                     MembershipPages objMembershipPages = new MembershipPages()
                     {
                         Title = dr["Title"].ToString(),
                         IsActive = Convert.ToBoolean(dr["IsActive"]),
                         MembershipPageId = Convert.ToInt16(dr["MembershipPageId"]),
                         PagePrefix = Convert.ToString(dr["PagePrefix"]),
                         Roles=dr["Roles"].ToString()

                     };
                     pageList.Add(objMembershipPages);
                 }
             }

         }

         finally
         {
             objParam = null;
             dr.Close();
             dr.Dispose();
         }
         return pageList;

     }




     public string GetOptions()
     {
         StringBuilder strBuilder = new StringBuilder();

         SqlDataReader dr = null;
         try
         {
             dr = new MembershipPagesDAL().GetAll();
             if (dr.HasRows)
             {
                 strBuilder.Append("<option></option>");
                 while (dr.Read())
                 {

                     strBuilder.Append(string.Format("<option value='{0}'>{1}</option>", dr["MembershipPageId"].ToString(), dr["Title"].ToString()));
                 }
             }

         }

         finally
         {
             dr.Close();
             dr.Dispose();
         }
         return strBuilder.ToString();

     }





     public int InsertUpdate(MembershipPages objMembershipPages,DataTable dt)
    {

        return new MembershipPagesDAL().InsertUpdate(objMembershipPages,dt);
    }

     
}