﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
public class CategoriesBLL
{
  

  public string GetCategoriesHTML(Settings objSettings,out int CategoryId)
     {
         StringBuilder strBuilder = new StringBuilder();
         CategoryId = 0;
      
         SqlDataReader dr = null;
         try
         {
             dr = new CategoriesDAL().GetAll();
             if (dr.HasRows)
             {
             
                 while (dr.Read())
                 {
                     string style = "class=ancBasic";
                     if (CategoryId == 0)
                     {
                           style = "class=ancSelected";
                         CategoryId = Convert.ToInt16(dr["Group_ID"]);
                     }

                     strBuilder.Append(string.Format("<li onclick='javascript:Search({0},\"\")'><a href='#' " + style + " name='categories'>{1}</a></li>", dr["Group_ID"].ToString(), dr["Group_Name"].ToString()));
                 }
             }

             dr.NextResult();
             if (dr.HasRows)
             {

                 dr.Read();
                 objSettings.SerTax = dr["SerTax"].ToString().Trim() == "" ? Convert.ToDecimal(0) : Convert.ToDecimal(dr["SerTax"]);
                 objSettings.TakeAway =  dr["TakeAway"].ToString().Trim() == "" ? Convert.ToInt16(0) : Convert.ToInt16(dr["TakeAway"]);
                 objSettings.TakeAwayDefault = dr["TakeAwayDefault"].ToString().Trim() == "" ? Convert.ToInt16(0) : Convert.ToInt16(dr["TakeAwayDefault"]);  
                
             }

         }

         finally
         {
             dr.Close();
             dr.Dispose();
         }
         return strBuilder.ToString();

     }

  public string GetOptions()
  {
      StringBuilder strBuilder = new StringBuilder();
       

      SqlDataReader dr = null;
      try
      {
          dr = new CategoriesDAL().GetAll();
          if (dr.HasRows)
          {

              while (dr.Read())
              {
             

                  strBuilder.Append(string.Format("<option value='{0}'>{1}</option>",dr["Group_ID"].ToString(), dr["Group_Name"].ToString()));
              }
          }

      

      }

      finally
      {
          dr.Close();
          dr.Dispose();
      }
      return strBuilder.ToString();

  }



 
     
}