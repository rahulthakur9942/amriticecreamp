﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ExciseBLL
/// </summary>
public class ExciseBLL
{
    public List<Excise> GetAll()
    {
        List<Excise> ExciseList = new List<Excise>();

        SqlDataReader dr = null;
        try
        {
            dr = new ExciseDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Excise objExcise = new Excise()
                    {
                        Excise_Head = dr["Excise_Head"].ToString(),
                        Teriff_No = dr["Teriff_No"].ToString(),
                        Excise_ID = Convert.ToInt16(dr["Excise_ID"]),
                        ExciseDuty = dr["ExciseDuty"].ToString(),
                        Cr_AccCode = dr["Cr_AccCode"].ToString(),
                        Dr_AccCode = dr["Dr_AccCode"].ToString(),
                        Cr_AccCName = dr["Cr_AccCName"].ToString(),
                        Dr_AccCName = dr["Dr_AccCName"].ToString(),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        Abatement = Convert.ToDecimal(dr["Abatement"]),
                    };
                    ExciseList.Add(objExcise);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ExciseList;

    }





    public Int32 DeleteExcise(Excise objExcise)
    {
        return new ExciseDAL().Delete(objExcise);
    }




    public Int16 InsertUpdate(Excise objExcise)
    {

        return new ExciseDAL().InsertUpdate(objExcise);
    }

}