﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for DeliveryReport
/// </summary>
public class DeliveryReport
{
  SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
  public DataSet GetDeliveryReport()
  {
    var Param = new SqlParameter[1];
    Param[0] = new SqlParameter("@req", "gethomedel_detail");
    DataSet dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "Strp_deliveryreport", Param);
    return dt;
  }
  public DataSet GetByDateReport(DateTime FromDate,DateTime ToDate)
  {
    var Param = new SqlParameter[3];
    Param[0] = new SqlParameter("@FromDate", FromDate);
    Param[1] = new SqlParameter("@ToDate", FromDate);
    Param[2] = new SqlParameter("@req", "getfilters");
    DataSet dt = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "Strp_deliveryreport", Param);
    return dt;
  }
}