﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


/// <summary>
/// Summary description for designation
/// </summary>
public class designation
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public designation()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable ManageDesignation() {

        con.Open();
        SqlCommand cmd = new SqlCommand("select desgname,desgid from [dbo].[Designation]  where IsActive=1", con);
        cmd.CommandType = CommandType.Text;


        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }
}