﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SoftKeyboard
/// </summary>
public  class SoftKeyboard
{

    public static int  IsKeyboardEnable =0;
   
    public void getvalue()
    {
        Connection getcon = new Connection();
        using (SqlConnection con = new SqlConnection(getcon.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select enablesoftkeyboard from Mastersetting_Basic where BranchId=@BranchId", con);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@BranchId", HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                IsKeyboardEnable = Convert.ToInt16(rd["enablesoftkeyboard"]);
            }

        }

    }

}