﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for customClasses
/// </summary>
public class mst_customer
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);

    public string req { get; set; }
    public string customer_name { get; set; }
    
    public string status { get; set; }
    public int cst_id { get; set; }
    public string remark { get; set; }
    public string discount { get; set; }
    public string userid { get; set; }


    public mst_customer()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void insert_update_customer()
    {

       
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_mst_customer", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@cst_id", cst_id);
        cmd.Parameters.AddWithValue("@customer_name", customer_name);
        cmd.Parameters.AddWithValue("@status", status);
        cmd.Parameters.AddWithValue("@discount", discount);
        cmd.Parameters.AddWithValue("@remark", remark);
        cmd.Parameters.AddWithValue("@userid", userid);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }


    public void get_customer()
    {


        con.Open();
        SqlCommand cmd = new SqlCommand("strp_mst_customer", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@cst_id", cst_id);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read())
        {

            customer_name = rd["customer_name"].ToString();
            status = rd["status"].ToString();
            remark= rd["remark"].ToString();
            discount= rd["discount"].ToString();

        }
        con.Close();


    }



    public void del_customer()
    {


        con.Open();
        SqlCommand cmd = new SqlCommand("strp_mst_customer", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@cst_id", cst_id);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }

    public DataTable bindgride()
    {

        con.Open();
        SqlCommand cmd = new SqlCommand("strp_mst_customer", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }


}