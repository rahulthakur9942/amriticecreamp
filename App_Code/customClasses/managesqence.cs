﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


/// <summary>
/// Summary description for managesqence
/// </summary>
public class managesqence
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public string req { get; set; }
    public int posid { get; set; }
    public int PROP_ID { get; set; }
    public int lvl { get; set; }
    public int itemid { get; set; }
    public managesqence()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable bind_item_dd()
    {

        con.Open();
        SqlCommand cmd = new SqlCommand("strp_managesequence", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@posid", posid);
        cmd.Parameters.AddWithValue("@PROP_ID", PROP_ID);

        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }

    public void insert_update_sequence()
    {


        con.Open();
        SqlCommand cmd = new SqlCommand("strp_managesequence", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@PROP_ID", PROP_ID);
        cmd.Parameters.AddWithValue("@lvl", lvl);
        cmd.Parameters.AddWithValue("@itemid", itemid);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }
}