﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PamentMode
/// </summary>
public class PaymentMode
{
    public string OtherPaymentModeID { get; set; }
    public string OtherPaymentName { get; set; }
}