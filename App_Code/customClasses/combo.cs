﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for combo
/// </summary>
public class combo
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public string req { get; set; }
    public string comboid { get; set; }
    public string itemid { get; set; }
    public decimal amount { get; set; }
    public decimal itemqty { get; set; }
    public decimal comborate { get; set; }
    public string combosess_id { get; set; }
    public string ItemName { get; set; }
    public int No_Ofitem { get; set; }
    public string billnowprefix { get; set; }
    
    public combo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void insert_update_combo()
    {


        con.Open();
        SqlCommand cmd = new SqlCommand("strp_combo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@comboid", comboid);
        cmd.Parameters.AddWithValue("@itemid", itemid);
        cmd.Parameters.AddWithValue("@amount", amount);
        cmd.Parameters.AddWithValue("@itemqty", itemqty);
        cmd.Parameters.AddWithValue("@comborate", comborate); 
        cmd.Parameters.AddWithValue("@combosess_id", combosess_id);
        cmd.Parameters.AddWithValue("@NoItem", No_Ofitem);
        cmd.Parameters.AddWithValue("@billnowprefix", billnowprefix);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }


    public void update_combo(string combid)
    {


        con.Open();
        SqlCommand cmd = new SqlCommand("delete from combo_detail where combo_sess_id=@combosess_id", con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@combosess_id", combid);
        cmd.ExecuteNonQuery();
        con.Close();


    }

    //public string update_combo(string combid)
    //{


    //    con.Open();
    //    SqlCommand cmd = new SqlCommand("delete from combo_detail where combo_sess_id=@combosess_id", con);
    //    cmd.CommandType = CommandType.Text;
    //    cmd.Parameters.AddWithValue("@combosess_id", combid);
    //    cmd.ExecuteNonQuery();
    //    con.Close();


    //}
    public void get_combo()
    {


        con.Open();
        SqlCommand cmd = new SqlCommand("strp_combo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@combosess_id", combosess_id);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read())
        {
            comboid = rd["comboid"].ToString();
            itemid = rd["itemid"].ToString();
            amount = Convert.ToInt32(rd["rate"]);
            itemqty = Convert.ToDecimal(rd["qty"]);
            combosess_id = rd["combo_sess_id"].ToString();
        }

        con.Close();


    }



}