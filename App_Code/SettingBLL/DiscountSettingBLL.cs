﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DiscountSettingBLL
/// </summary>
/// 


public class DiscountDetail
{
    public decimal StartValue { get; set; }
    public decimal EndValue { get; set; }
    public decimal DisPer { get; set; }
}

public class DiscountSettingBLL
{
    public Int16 UpdateBasicSettings(DiscountSetting objSettings,DataTable dt)
    {

        return new DiscountSettingDAL().UpdateBasicSettings(objSettings,dt);
    }



    public List<DiscountDetail> GetSettings(DiscountSetting objSettings)
    {

        List<DiscountDetail> lst = new List<DiscountDetail>();
       

        SqlDataReader dr = null;
        try
        {
            dr = new DiscountSettingDAL().GetMasterSettingsByType(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.Allow_Dis_on_Billing = Convert.ToBoolean(dr["Allow_Dis_on_Billing"]);
                objSettings.Dis_Bill_Value = Convert.ToBoolean(dr["Dis_Bill_Value"]);
                objSettings.Enable_Dis_Col = Convert.ToBoolean(dr["Enable_Dis_Col"]);
                objSettings.Back_End_Discount = Convert.ToBoolean(dr["Back_End_Discount"]);
                objSettings.Enable_Cust_Dis = Convert.ToBoolean(dr["Enable_Cust_Dis"]);
                objSettings.Enable_Dis_Amt = Convert.ToBoolean(dr["Enable_Dis_Amt"]);
                objSettings.Type = Convert.ToString(dr["Type"]);
                objSettings.UserId = Convert.ToInt32(dr["UserId"]);
                objSettings.BranchId = Convert.ToInt32(dr["BranchId"]);
             
            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DiscountDetail obj = new DiscountDetail();
                    obj.StartValue = Convert.ToDecimal(dr["Start_Value"]);
                    obj.EndValue = Convert.ToDecimal(dr["End_Value"]);
                    obj.DisPer = Convert.ToDecimal(dr["Disper"]);
                    lst.Add(obj);
                }
            }
            return lst;

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}