﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="stewardlogin.aspx.cs" Inherits="stewardlogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">

<head>

    <title>AMRIT'S</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/steward-login.css" rel="stylesheet" />


    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->

    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet"> 

</head>
<body class="login_body">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper" class="login_wraper">
            <div id="login" class="animate form all_login_main">
                <section class="login_content">
                    <h2 class="login_heading">steward login</h2>
                    <form id="form1" runat="server" class="form_contant">
                        <%--<h1>Login Form</h1>--%>
                        

                         <div>
                            <asp:DropDownList class="form-control" id="ddlLocation" runat="server" placeholder="Choose Location" AutoPostBack="true" onselectedindexchanged="itemSelected">
                             <asp:ListItem Value="0" Text="Choose Location"></asp:ListItem> 
                             <asp:ListItem Value="1" Text="Connect Any Branch"></asp:ListItem>  
                          <%--   <asp:ListItem Value="2" Text="Connect Gelato"></asp:ListItem>   --%>
                             </asp:DropDownList>                        
                        </div>

                        <div>
                          <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>                        
                        </div>
                        
                        <div>                      
                          <input type="text" id="txtUserName" class="form-control" runat="server" placeholder="Username" required="" />
                        </div>

                        <div>                        
                           <input type="password" id="txtPassword" class="form-control" runat="server" placeholder="Password" required="" />
                        </div>

                        <div>
                         <asp:Button id="btnLogin"  class="btn btn-default submit" Text="Log In" OnClick="btnLogin_Click" runat="server"/>                         
                        </div>

                        <div class="clearfix"></div>                       
                    </form>
                    <!-- form -->

                </section>
                <!-- content -->
            </div>
          
        </div>
    </div>



    <%-- script --%>

    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>
    <script src="js/jquery.min.js"></script>

</body>

</html>
