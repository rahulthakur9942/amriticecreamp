﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GenerateCookies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        if (ddlDepartments.SelectedIndex > 0)
        {
            if (txtPassword.Text == "pa55word")
            {

                //create a cookie
                HttpCookie myCookie = new HttpCookie("ck");

                //Add key-values in the cookie
                myCookie.Values.Add("Department", ddlDepartments.SelectedValue);

                //set cookie expiry date-time. Made it to last for next 12 hours.
                myCookie.Expires = DateTime.Now.AddYears(1);

                //Most important, write the cookie to client.
                Response.Cookies.Add(myCookie);

            }
        }
    }
}