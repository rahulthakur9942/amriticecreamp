﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_managebillseries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLocation();
            BindGodown();
        }
        CheckRole();
    }


    public void CheckRole()
    {

        


        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BILLSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    void BindLocation()
    {

        ddlBSLocation.DataSource = new BranchBLL().GetAll();
        ddlBSLocation.DataValueField = "BranchId";
        ddlBSLocation.DataTextField = "BranchName";
        ddlBSLocation.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBSLocation.Items.Insert(0, li1);

    }

    void BindGodown()
    {

        ddlBSGodown.DataSource = new GodownsBLL().GetAll();
        ddlBSGodown.DataValueField = "Godown_ID";
        ddlBSGodown.DataTextField = "Godown_Name";
        ddlBSGodown.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Godown--";
        li1.Value = "0";
        ddlBSGodown.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string Insert(BillSeriesSetting objSettings)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objSettings.LUser = Id.ToString();
        
        int status = new BillSeriesSettingBLL().UpdateBasicSettings(objSettings);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}