﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class BreakageExpiry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }

        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BREAKAGEEXPIRY));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() 
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string BindControls()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string godownData = new GodownsBLL().GetOptions();
        string godownFrom = new GodownsBLL().GetOptionsGodownFrom(Branch);
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            GodownOptions = godownData,
            GodownFromOptions = godownFrom

        };
        return ser.Serialize(JsonData);
    }


  
    [WebMethod]
    public static string BindProductsByGodown(int godownId)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<ProductInGodown> lst = new BreakageExpiryBLL().GetProductListByGodownId(godownId, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ProductOptions = lst,


        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Insert(int BreakageId, string DateOfBreakage, int GodownId,
          string stockArr, string pidArr, string mrpArr, string qtyArr, string godownName)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BREAKAGEEXPIRY));

        string[] arrRoles = sesRoles.Split(',');


        if (BreakageId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }


        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        StockExpiry objBreakageExpiry = new StockExpiry()
        {
            Ref_No = BreakageId,
            Ref_Date = Convert.ToDateTime(DateOfBreakage),
            Godown_ID = GodownId,
            Godown = godownName,
            BranchId = Branch,
            UserNo = UserNo,
        };

        string[] pidData = pidArr.Split(',');
        //  string[] stockData = stockArr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] qtyData = qtyArr.Split(',');




        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("StockInHand");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Mrp");




        for (int i = 0; i < pidData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ItemCode"] = Convert.ToString(pidData[i]);
            dr["StockInHand"] = 0;// Convert.ToInt32(stockData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["Mrp"] = Convert.ToDecimal(mrpData[i]);



            dt.Rows.Add(dr);

        }

        status = 0;

        if (BreakageId == 0)
        {
            status = new BreakageExpiryBLL().Insert(objBreakageExpiry, dt);
        }
        else
        {
            status = new BreakageExpiryBLL().Update(objBreakageExpiry, dt);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            BreakageData = objBreakageExpiry

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindBreakageDetail(int BreakageId)
    {

        StockExpiry objBreakageExpiry = new StockExpiry() { Ref_No = BreakageId };
        int cntr = 0;
        string serviceData = new BreakageExpiryBLL().GetBreakageExpiryDetails(objBreakageExpiry, out cntr);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = serviceData,
            BreakageData = objBreakageExpiry,
            Counter = cntr

        };
        return ser.Serialize(JsonData);
    }
}