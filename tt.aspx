﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="tt.aspx.cs" Inherits="tt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">

<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
    rel="Stylesheet" type="text/css" />
 <script type="text/javascript">
 
        $(function () {
            $("[id*=txtSearch]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: 'tt.aspx/GetCustomers',
                        data: "{ 'username': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0],
                                        val: item.split('-')[1]
                                    };
                                }))
                            } else {
                                response([{ label: 'No results found.', val: -1}]);
                            }
                        }
                    });
                },
                select: function (e, u) {
                    if (u.item.val == -1) {
                        return false;
                    }
                }
            });
 
        });
    </script>
    <form id="form" runat="server">
    <div class="right_col" role="main">
        <div class="">
            <div class="x_panel">

                <div class="x_content">

                    <div class="youhave">

                        <div class="col-md-4">
                            Enter search term:
<asp:TextBox ID="txtSearch" runat="server"/>
<asp:HiddenField ID="hfCustomerId" runat="server" />
<asp:Button ID="Button1" Text="Submit" runat="server" />
                        </div>

                    </div>

                </div>
            </div>

       

            <!-- /page content -->

            <!-- footer content -->
            <%--     <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>--%>
            <!-- /footer content -->

        </div>
    </div>
        </form>
    
</asp:Content>

