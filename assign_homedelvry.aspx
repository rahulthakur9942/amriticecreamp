﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="assign_homedelvry.aspx.cs" Inherits="assign_homedelvry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <style>

        th {
            text-align:center;
        }

    </style>

    <form id="form" runat="server">
             <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="updateProgress" runat="server">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
                                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                                        ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
           <div class="right_col" role="main">
                <div class="">
                                 <div class="page-title">
                        <div class="title_left">
                            <h3>Assign Home Deliveries</h3>
                        </div>
  
                    </div>
                        <div class="clearfix"></div>
               
                    <div class="x_panel">

                        <div style="
    float:  left;
">

            <asp:Button ID="btnsubmit" class="btn btn-primary btn-small" runat="server" Text="View All" OnClick="btnsubmit_Click" />
        </div>
                        <div style="
    float:  right;
">
                            <asp:Button ID="btnkot" runat="server" Text="KOT" class="btn btn-primary btn-small" OnClick="btnkot_Click"/>


                        </div>

                                                <div style="
    float:  right;
">
                                                                <asp:Button ID="btnraisebill" runat="server" Text="RaiseBill" class="btn btn-primary btn-small" OnClick="btnraisebill_Click"/>


                        </div>
                               <div class="x_content">
    <asp:GridView ID="gv_display" AllowSorting="True" runat="server" OnSorting="gv_display_Sorting" class="table_class table table-striped table-bordered table-hover" OnRowDataBound="OnRowDataBound" AutoGenerateColumns="False"  OnRowEditing = "EditCustomer"  OnRowUpdating = "UpdateCustomer" OnRowCancelingEdit = "CancelEdit">

          <Columns>
            <%--   <asp:BoundField DataField="billnowprefix" HeaderText="Billnowprefix" 
                    SortExpression="billnowprefix" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                 --%>

                  <asp:BoundField DataField="bill_date" HeaderText="Bill Date" 
                    SortExpression="bill_date" ReadOnly="true" DataFormatString="{0:dd/MM/yyyy }">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>

<%--                        <asp:BoundField DataField="cashcust_name" HeaderText="Customer Name" 
                    SortExpression="cashcust_name" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>--%>

                 <asp:TemplateField SortExpression="cashcust_name">
            

            <HeaderTemplate>Customer Name</HeaderTemplate>
            <ItemTemplate>
                  <asp:Label ID="lblcstname" runat="server" Text='<%# Eval("cashcust_name")%>'></asp:Label>
                              </ItemTemplate>
        </asp:TemplateField>
             <asp:TemplateField>
            

            <HeaderTemplate>Customer Contact No</HeaderTemplate>
            <ItemTemplate>
                  <asp:Label ID="lblcstcontactno" runat="server" Text='<%# Eval("custmobile")%>'></asp:Label>
                              </ItemTemplate>
        </asp:TemplateField>

                 <%--     <asp:BoundField DataField="net_amount" HeaderText="Amount" 
                    SortExpression="net_amount" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>--%>

                           <asp:TemplateField>
            

            <HeaderTemplate>Amount</HeaderTemplate>
            <ItemTemplate>
                  <asp:Label ID="lblcsamount" runat="server" Text='<%# Eval("net_amount")%>'></asp:Label>
                              </ItemTemplate>
        </asp:TemplateField>
              
                      <asp:BoundField DataField="billmode" HeaderText="Billmode" 
                    SortExpression="billmode" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>

    <%--             <asp:BoundField DataField="empname" HeaderText="Assigned Employee" 
                    SortExpression="empname" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>--%>
          
                  <asp:TemplateField  >
                           <HeaderTemplate>Billnowprefix</HeaderTemplate>
                            <ItemTemplate>
                  <asp:Label ID="lblbillnowprefix_read" runat="server" Text='<%# Eval("billnowprefix")%>'></asp:Label>
                              </ItemTemplate>
              <EditItemTemplate>
                
        <asp:Label ID="lblbillnowprefix_edit" runat="server" Text='<%# Eval("billnowprefix")%>'></asp:Label>
    </EditItemTemplate>
                          </asp:TemplateField>
        <asp:TemplateField>
            

            <HeaderTemplate>Assign Employee</HeaderTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="dd_employee" runat="server" style="font-size: 30px;"></asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
               <asp:CommandField ShowEditButton="True" />
    </Columns>

    </asp:GridView>
<asp:GridView ID="gv_display_viewall" AllowSorting="True" Visible="false" runat="server" OnSorting="gv_display_viewall_Sorting" class="table_class table table-striped table-bordered table-hover" OnRowDataBound="OnRowDataBound" AutoGenerateColumns="False"  OnRowEditing = "EditCustomer"  OnRowUpdating = "UpdateCustomer" OnRowCancelingEdit = "CancelEdit">

          <Columns>
            <%--   <asp:BoundField DataField="billnowprefix" HeaderText="Billnowprefix" 
                    SortExpression="billnowprefix" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                 --%>

                  <asp:BoundField DataField="bill_date" HeaderText="Bill Date" 
                    SortExpression="bill_date" ReadOnly="true" DataFormatString="{0:dd/MM/yyyy }" >  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>

<%--                        <asp:BoundField DataField="cashcust_name" HeaderText="Customer Name" 
                    SortExpression="cashcust_name" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>--%>
                 <asp:BoundField DataField="cashcust_name" HeaderText="Customer Name" SortExpression="cashcust_name" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                         <asp:BoundField DataField="custmobile" HeaderText="Customer Contact No" SortExpression="custmobile" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
   

                      <asp:BoundField DataField="net_amount" HeaderText="Amount" 
                    SortExpression="net_amount" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>

              
                      <asp:BoundField DataField="billmode" HeaderText="Billmode" 
                    SortExpression="billmode" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>

                 <asp:BoundField DataField="empname" HeaderText="Assigned Employee" 
                    SortExpression="empname" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
          
                           <asp:BoundField DataField="billnowprefix" HeaderText="Billnowprefix" 
                    SortExpression="billnowprefix" ReadOnly="true">  <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
        

    </Columns>

    </asp:GridView>
                        </div>
                        </div>
                    
                    </div>
               </div>
                        
</ContentTemplate>
                     </asp:UpdatePanel>
        </form>

</asp:Content>

