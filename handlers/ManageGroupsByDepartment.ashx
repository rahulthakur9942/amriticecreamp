﻿<%@ WebHandler Language="C#" Class="ManageGroupsByDepartment" %>

using System;
using System.Web;

public class ManageGroupsByDepartment : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string Department = context.Request.QueryString["Department"];
        if (Department != null)
        {


            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
             new PropGroupBLL().GetByDepartmentId(Convert.ToInt32(Department))
               );

            context.Response.Write(xss);
        }







    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}