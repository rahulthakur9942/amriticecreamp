﻿<%@ WebHandler Language="C#" Class="ManageOtherPaymentMode" %>

using System;
using System.Web;

public class ManageOtherPaymentMode : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) 
    {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var x = jsonSerializer.Serialize(

                new OtherPaymentModeBLL().GetAll());

            context.Response.Write(x);
        }
        else if (strOperation == "del")
        {


        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}