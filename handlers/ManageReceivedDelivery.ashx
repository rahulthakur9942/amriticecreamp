﻿<%@ WebHandler Language="C#" Class="ManageReceivedDelivery" %>

using System;
using System.Web;

public class ManageReceivedDelivery : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        string dateFrom = context.Request.QueryString["dateFrom"];
        string dateTo = context.Request.QueryString["dateTo"];
        Int32 BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        if (strOperation == null)
        {
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var x = jsonSerializer.Serialize(

                new DeliveryNoteBLL().GetAllReceived(Convert.ToDateTime(dateFrom), Convert.ToDateTime(dateTo), BranchId));

            context.Response.Write(x);
        }
        else if (strOperation == "del")
        {


        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}