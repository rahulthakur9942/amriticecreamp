﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucItemMaster.ascx.cs"
    Inherits="Templates_ucItemMaster" %>
<asp:Literal ID="ltContent" runat="server"></asp:Literal>


<script langauage="javascript" type="text/javascript">
    var Purcode = "";
    var salecode = "";
    var codeauto = false;
    var alphanumericcode = false;
    var itemcodelen = 0;

    var NutritionFactCollection = [];
    function clsFacts() {
        this.Field1 = 0;
        this.Field2 = "";
        this.Field3 = "";
        this.Bold = 0;
        this.Ingredients = 0;
        this.OtherInformation1 = 0;
        this.OtherInformation2 = 0;

    }



    function GetVMResponse(Id, Title, IsActive, Status, Type) {
        $("#dvVMDialog").dialog("close");


        var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
        if (Type == "Department") {

            $("#ddlIMDepartment").append(opt);

        }

        if (Type == "ItemType") {

            $("#ddlIMItemType").append(opt);

        }

        if (Type == "Color") {

            $("#ddlIMCategory").append(opt);

        }

        if (Type == "Location") {

            $("#ddlIMRackShelf").append(opt);

        }

    }

    function OpenVMDialog(Type) {


        $.ajax({
            type: "POST",
            data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
            url: "managearea.aspx/LoadUserControl",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvVMDialog").remove();
                $("body").append("<div id='dvVMDialog'/>");
                $("#dvVMDialog").html(msg.d).dialog({ modal: true });
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }
        });

    }




    function BindIMSubGroups() {

        //        var GroupID = $("#ddlIMGroup").val();
        $.ajax({
            type: "POST",
            data: '{ }',
            url: "manageitemmaster.aspx/BindSubGroups1",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlIMSubGroup").html(obj.SubGroupOptions);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }

    function BindIMGroups() {



        $.ajax({
            type: "POST",
            data: '{ }',
            url: "manageitemmaster.aspx/BindGroups",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlIMGroup").html(obj.GroupOptions);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }

    function BindVariableMaster() {


        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "manageitemmaster.aspx/GetVariableMasters",
            data: {},
            async: false,
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Departments.length; i++) {

                    html = html + "<option value='" + obj.Departments[i]["Id"] + "'>" + obj.Departments[i]["Title"] + "</option>";
                }

                $("#ddlIMDepartment").html(html);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Categories.length; i++) {

                    html = html + "<option value='" + obj.Categories[i]["Id"] + "'>" + obj.Categories[i]["Title"] + "</option>";
                }




                $("#ddlIMCategory").html(html);



                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.ItemTypes.length; i++) {

                    html = html + "<option value='" + obj.ItemTypes[i]["Id"] + "'>" + obj.ItemTypes[i]["Title"] + "</option>";
                }

                $("#ddlIMItemType").html(html);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Locations.length; i++) {

                    html = html + "<option value='" + obj.Locations[i]["Id"] + "'>" + obj.Locations[i]["Title"] + "</option>";
                }
                $("#ddlIMRackShelf").html(html);

                var html = "<option value='0'></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.SaleUnits.length; i++) {

                    html = html + "<option value='" + obj.SaleUnits[i]["Id"] + "'>" + obj.SaleUnits[i]["Title"] + "</option>";
                }
                $("#ddlIMSaleUnit").html(html);
                $("#ddlIMWgtLtr").html(html);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Companies.length; i++) {

                    html = html + "<option value='" + obj.Companies[i]["Id"] + "'>" + obj.Companies[i]["Title"] + "</option>";
                }
                $("#ddlIMCompany").html(html);

                var html = "<option></option>";

                for (var i = 0; i < obj.TaxRates.length; i++) {

                    html = html + "<option value='" + obj.TaxRates[i]["Id"] + "'>" + obj.TaxRates[i]["Title"] + "</option>";
                }
                $("#ddlIMTax").html(html);


                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Excise.length; i++) {

                    html = html + "<option value='" + obj.Excise[i]["Id"] + "'>" + obj.Excise[i]["Title"] + "</option>";
                }
                $("#ddlIMExicise").html(html);

                var html = "<option></option>";

                for (var i = 0; i < obj.HSN.length; i++) {

                    html = html + "<option value='" + obj.HSN[i]["Title"] + "'>" + obj.HSN[i]["Title"] + "</option>";
                }
                $("#ddlHSNCode").html(html);

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $("#ddlIMTax").val(1);
                $("#ddlIMDepartment").val(0);
                $("#ddlIMCompany").val(0);
                $("#ddlIMCategory").val(0);
                $("#ddlIMItemType").val(1);
                $("#ddlIMExicise").val(0);
                $("#ddlHSNCode").val("0");
                $("#ddlIMRackShelf").val(0);
                $("#txtIMPurRate").val("1");
                $("#txtIMMaxRetailPrice").val(1);
                $("#txtIMFranchiseRate").val(1);
                $("#txtIMWholeSalerate").val(1);
                $("#txtIMSaleRate").val(1);
                $("#txtIMMarkup").val(1);
                $("#txtIMDeiveryNoteRate").val(1);
                $("#txtIMSaleRate").val(1);
                $("#ddlIMGroup").val("NONE");
                $("#ddlIMGroup").text("NONE");
                $("#ddlIMTransactionType").val("Sale Only");
                $("#ddlIMSaleUnit").val(4);
                $("#ddlIMWgtLtr").val(4);


            }
        });

    }

    function BindNutritionFacts(objArr) {

        var len = objArr.length;


        var html = "";
        for (var i = 0; i < 14; i++) {

            if (i < len) {
                html = html + "<tr><td>" + (i + 1) + "</td><td><input type='checkbox' name='chkIMNuBold'></td><td><input type='text' name='txtIMField1' value='" + objArr[i]["Title"] + "'/></td><td><input type='text' name='txtIMField2'/></td><td><input type='text' name='txtIMField2'/></td></tr>";
            }
            else {
                html = html + "<tr><td>" + (i + 1) + "</td><td><input type='checkbox' name='chkIMNuBold'></td><td><input type='text' name='txtIMField1'/></td><td><input type='text' name='txtIMField2'/></td><td><input type='text' name='txtIMField2'/></td></tr>";

            }


        }

        $("#tbNutritionInfo").html(html);
    }





    function BindRows() {


        var html = "";
        for (var i = 0; i < NutritionFactCollection.length; i++) {

            html += "<tr><td>" + (i + 1) + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Bold"] + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Field1"] + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Field2"] + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Field3"] + "</td>";

            html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
            html += "</tr>";

            $("#txtNutritionRemarks").val(NutritionFactCollection[i]["Ingredients"]);


        }

        $("#tbNutritionInfo").html(html);

    }





    function ClearItemDialog() {
        validateForm("detach");
        //$("#txtIMMasterCode").val("");
        //$("#txtIMItemCode").val("");
        $("#txtIMItemName").val("");
        $("#txtIMShortName1").val("");
        $("#txtIMShortName2").val("");
        $("#txtIMBarCode").val("");
        //$("#ddlIMDepartment").val("0");
        //$("#ddlIMGroup").val("0");
        //$("#ddlIMSubGroup").val("0");
        //$("#ddlIMCompany").val("0");
        //$("#ddlIMCategory").val("0");
        //$("#ddlIMTax").val(1);
        //$("#ddlIMItemType").val("1");
        //$("#ddlIMExicise").val("0");
        //$("#ddlHSNCode").val("0");
        //$("#txtIMPurRate").val("");
        //$("#txtIMMaxRetailPrice").val("");
        //$("#txtIMFranchiseRate").val("");
        //$("#txtIMWholeSalerate").val("");
        //$("#txtIMSaleRate").val("");
        //$("#txtIMDeiveryNoteRate").val("");
        $("#txtIMSaleRateExcl").val("");
        //$("#txtIMMarkup").val("");
        //$("#ddlIMRackShelf").val("0");
        //$("#ddlIMSaleUnit").val("1");
        //$("#txtIMWgtLtr").val("");
        //$("#ddlIMWgtLtr").val("");
        //$("#ddlIMTransactionType").val("");

        //$('#chkInHousePacking').prop('checked', false);
        //$('#chkEditRate').prop('checked', false);
        //$("#chkIMPackedQty").val("");
        //$('#chkIMDis1Per').prop('checked', false);
        //$("#txtIMDis1Per").val("");
        //$('#txtIMDis1Per').prop('checked', false);
        //$("#txtIMDis2Per").val("");
        //$('#chkIMItemDiscontinued').prop('checked', false);
        //$('#chkIMCouponPrinting').prop('checked', false);
        //$("#txtIMDays").val("");
        //$('#rdoIMExpiry').prop('checked', false);
        //$('#rdoIMBestBefore').prop('checked', false);
        //$('#rdoIMVeg').prop('checked', false);
        //$('#rdoIMNonVeg').prop('checked', false);
        //$("#ddlIMSubItem").val("");
        //$('#chkIMSubItem').prop('checked', false);
        //$("#txtIMQtyToLess").val("");

        //$("#txtIMMaxLevel").val("");
        //$("#txtIMMinLevel").val("");
        //$("#txtIMReOrderOty").val("");
        //$("#txtIMLevel2").val("");
        //$("#txtIMLevel3").val("");
        //$("#txtIMLevel4").val("");
        //$("#ddlIMCurQtyRate").val("");

        //$("#txtIMDealerMargin").val("");
        //$("#txtIMQtyInCase").val("");
        //$("#txtIMWithTax").val("");

        $("#txtIMLatestPurRate").val("");
        $("#txtIMComments").val("");
        //$("#ddlIMTax").val(1);
        //$("#ddlIMDepartment").val(0);
        //$("#ddlIMCompany").val(0);
        //$("#ddlIMCategory").val(0);
        //$("#ddlIMItemType").val(1);
        //$("#ddlIMExicise").val(0);
        //$("#ddlHSNCode").val("0");
        //$("#ddlIMRackShelf").val(0);
        $("#txtIMPurRate").val("1");
        $("#txtIMMaxRetailPrice").val(1);
        $("#txtIMFranchiseRate").val(1);
        $("#txtIMWholeSalerate").val(1);
        $("#txtIMSaleRate").val(1);
        $("#txtIMMarkup").val(1);
        $("#txtIMDeiveryNoteRate").val(1);
        $("#txtIMSaleRate").val(1);
        //$("#ddlIMGroup").val("NONE");
        //$("#ddlIMGroup").text("NONE");
        //$("#ddlIMTransactionType").val("Sale Only");
        //$("#ddlIMSaleUnit").val(4);
        //$("#ddlIMWgtLtr").val(4);
        //$("#dvIMDialog").dialog("close");



    }


    function ClearItemDialog2() {
        validateForm("detach");
        $("#txtIMMasterCode").val("");
        $("#txtIMItemCode").val("");
        $("#txtIMItemName").val("");
        $("#txtIMShortName1").val("");
        $("#txtIMShortName2").val("");
        $("#txtIMBarCode").val("");
        $("#ddlIMDepartment").val("0");
        $("#ddlIMGroup").val("0");
        $("#ddlIMSubGroup").val("0");
        $("#ddlIMCompany").val("0");
        $("#ddlIMCategory").val("0");
        $("#ddlIMTax").val(1);
        $("#ddlIMItemType").val("1");
        $("#ddlIMExicise").val("0");
        $("#ddlHSNCode").val("0");
        $("#txtIMPurRate").val("");
        $("#txtIMMaxRetailPrice").val("");
        $("#txtIMFranchiseRate").val("");
        $("#txtIMWholeSalerate").val("");
        $("#txtIMSaleRate").val("");
        $("#txtIMDeiveryNoteRate").val("");
        $("#txtIMSaleRateExcl").val("");
        $("#txtIMMarkup").val("");
        $("#ddlIMRackShelf").val("0");
        $("#ddlIMSaleUnit").val("1");
        $("#txtIMWgtLtr").val("");
        $("#ddlIMWgtLtr").val("");
        $("#ddlIMTransactionType").val("");

        $('#chkInHousePacking').prop('checked', false);
        $('#chkEditRate').prop('checked', false);
        $("#chkIMPackedQty").val("");
        $('#chkIMDis1Per').prop('checked', false);
        $("#txtIMDis1Per").val("");
        $('#txtIMDis1Per').prop('checked', false);
        $("#txtIMDis2Per").val("");
        $('#chkIMItemDiscontinued').prop('checked', false);
        $('#chkIMCouponPrinting').prop('checked', false);
        $("#txtIMDays").val("");
        $('#rdoIMExpiry').prop('checked', false);
        $('#rdoIMBestBefore').prop('checked', false);
        $('#rdoIMVeg').prop('checked', false);
        $('#rdoIMNonVeg').prop('checked', false);
        $("#ddlIMSubItem").val("");
        $('#chkIMSubItem').prop('checked', false);
        $("#txtIMQtyToLess").val("");

        $("#txtIMMaxLevel").val("");
        $("#txtIMMinLevel").val("");
        $("#txtIMReOrderOty").val("");
        $("#txtIMLevel2").val("");
        $("#txtIMLevel3").val("");
        $("#txtIMLevel4").val("");
        $("#ddlIMCurQtyRate").val("");

        $("#txtIMDealerMargin").val("");
        $("#txtIMQtyInCase").val("");
        $("#txtIMWithTax").val("");

        $("#txtIMLatestPurRate").val("");
        $("#txtIMComments").val("");
        $("#ddlIMTax").val(1);
        $("#ddlIMDepartment").val(0);
        $("#ddlIMCompany").val(0);
        $("#ddlIMCategory").val(0);
        $("#ddlIMItemType").val(1);
        $("#ddlIMExicise").val(0);
        $("#ddlHSNCode").val("0");
        $("#ddlIMRackShelf").val(0);
        $("#txtIMPurRate").val("1");
        $("#txtIMMaxRetailPrice").val(1);
        $("#txtIMFranchiseRate").val(1);
        $("#txtIMWholeSalerate").val(1);
        $("#txtIMSaleRate").val(1);
        $("#txtIMMarkup").val(1);
        $("#txtIMDeiveryNoteRate").val(1);
        $("#txtIMSaleRate").val(1);
        $("#ddlIMGroup").val("NONE");
        $("#ddlIMGroup").text("NONE");
        $("#ddlIMTransactionType").val("Sale Only");
        $("#ddlIMSaleUnit").val(4);
        $("#ddlIMWgtLtr").val(4);
        $("#dvIMDialog").dialog("close");



    }

    $(document).ready(
    function () {

        BindIMGroups();
        BindIMSubGroups();
        $("#chkIMPackedQty").prop('disabled', true);
        $("#txtIMDis1Per").prop('disabled', true);
        $("#txtIMDis2Per").prop('disabled', true);


        $("#chkInHousePacking").change(function () {

            if ($('#chkInHousePacking').prop('checked') == true) {
                $("#chkIMPackedQty").prop('disabled', false);

            }
            else {
                $("#chkIMPackedQty").prop('disabled', true);

                $("#chkIMPackedQty").val("");
            }
        });


        $("#chkIMDis1Per").change(function () {

            if ($('#chkIMDis1Per').prop('checked') == true) {

                $("#txtIMDis1Per").prop('disabled', false);

            }
            else {
                $("#txtIMDis1Per").prop('disabled', true);

                $("#txtIMDis1Per").val("");
            }
        });

        $("#chkIMDis2Per").change(function () {

            if ($('#chkIMDis2Per').prop('checked') == true) {
                $("#txtIMDis2Per").prop('disabled', false);

            }
            else {
                $("#txtIMDis2Per").prop('disabled', true);

                $("#txtIMDis2Per").val("");
            }
        });












        $("#ddlIMSubItem").supersearch({
            Type: "MasterItem",
            Caption: "Please enter Master Items ",
            AccountType: "",
            Width: 214,
            DefaultValue: 0,
            Godown: 0
        });


        $("#btnDel").click(
      function () {
          var RowIndex = Number($(this).closest('tr').index());


          NutritionFactCollection.splice(RowIndex, 1);
          BindRows();

      }

      );


        $("#txtIMSaleRate").keyup(
        function () {




            if ($("#ddlIMTax").val() == "0") {
                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val());
            }
            else {
                var SR = $('#txtIMSaleRate').val();
                var TR = $("#ddlIMTax option:selected").text();

                var TaxAmt = (Number((SR * TR) / (100 + Number(TR)))).toFixed(2);

                var total = $('#txtIMSaleRate').val() - Number(TaxAmt).toFixed(2);

                $("#txtIMSaleRateExcl").val(total.toFixed(2));
            }


            //            if ($('#txtIMSaleRate').val() != "") {
            //                var TaxAmt = ($('#txtIMSaleRate').val() * $("#ddlIMTax option:selected").text()) / 100;
            //                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val() - TaxAmt);
            //            }






        }
        );

        $("#txtIMItemName").keyup(
            function () {
                $("#txtIMShortName1").val($("#txtIMItemName").val())

            }
            );

        //        $("#txtIMMasterCode").keyup(
        //           function () {
        //              
        //           }
        //           );

        $("#txtIMItemCode").keyup(
            function () {
                var code = $(this).val();

                if (code.length > itemcodelen) {
                    $(this).val(code.substr(0, itemcodelen));
                    return;
                }

                if (alphanumericcode == false) {
                    if (isNaN(code)) {
                        $(this).val(code.replace(/\D/g, ''));
                    }
                }

                $("#txtIMBarCode").val($("#txtIMItemCode").val())
            }
            );


        $.ajax({
            type: "POST",
            data: '{ }',
            url: "manageitemmaster.aspx/FillSettings",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                codeauto = obj.setttingData.Auto_GenCode;

                alphanumericcode = obj.setttingData.Alphabet_Code;
                itemcodelen = obj.setttingData.Item_CodeLen;
                if (codeauto == true) {

                    $("#txtIMItemCode").attr('disabled', 'disabled').val("Auto");
                }
                else {

                    $("#txtIMItemCode").removeAttr('disabled');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


            }

        });



        BindVariableMaster();
        // BindNutritionFacts("");

        $("#ddlIMTax").change(function () {
            var taxrate = $("#ddlIMTax option:selected").text();
            $.ajax({
                type: "POST",
                data: '{  "TaxRate":"' + taxrate + '"}',
                url: "manageitemmaster.aspx/GetByTax",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    Purcode = obj.TaxData.Cr_AccCode;
                    salecode = obj.TaxData.Dr_AccCode;


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        });


        function ResetList() {

            $("#txtIMField1").val("");
            $("#txtIMField2").val("");
            $("#txtIMField3").val("");
            $("#chkIMBold").removeAttr("checked");



        }

        $("#btnAddNutrition").click(
           function () {




               TO = new clsFacts();
               TO.Field1 = $("#txtIMField1").val();
               TO.Field2 = $("#txtIMField2").val();
               TO.Field3 = $("#txtIMField3").val();

               if (TO.Field1.trim() == "") {
                   $("#txtIMField1").focus();
                   return;
               }

               var IsBold = false;
               if ($("#chkIMBold").prop("checked")) {
                   IsBold = true;
               }


               TO.Bold = IsBold;


               NutritionFactCollection.push(TO);


               BindRows();
               ResetList();

           }

           );


        if ($("#hdnIMID").val() > 0) {
            $.uiLock('');


            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + $("#hdnIMID").val() + '"}',
                url: "manageitemmaster.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#txtIMMasterCode").val(obj.ItemOptions.Master_Code);
                    $("#txtIMItemCode").val(obj.ItemOptions.Item_Code);
                    $("#txtIMItemName").val(obj.ItemOptions.Item_Name);
                    $("#txtIMShortName1").val(obj.ItemOptions.Short_Name1);
                    $("#txtIMShortName2").val(obj.ItemOptions.Short_Name2);
                    $("#txtIMBarCode").val(obj.ItemOptions.Bar_Code);
                    $("#ddlHSNCode").val(obj.ItemOptions.HSNCode);

                    if (obj.ItemOptions.Master_Code != obj.ItemOptions.Item_Code) {

                        $("#chkIMSubItem").prop("checked", true);
                        $("#ddlIMSubItem").html("<option selected=selected value='" + obj.ItemOptions.Master_Code + "'>" + obj.ItemOptions.MasterItemName + "</option>");
                        $("#txtddlIMSubItem").val(obj.ItemOptions.MasterItemName);

                        $("#txtddlIMSubItem").attr("title", obj.ItemOptions.MasterItemName);

                    }
                    else {
                        $("#chkIMSubItem").prop("checked", false);
                    }






                    $("#ddlIMCurQtyRate").val(obj.ItemOptions.Cursor_On);
                    var department = obj.ItemOptions.Department;

                    $("#ddlIMDepartment option[value='" + department + "']").prop("selected", true);

                    $.ajax({
                        type: "POST",
                        data: '{}',
                        url: "manageitemmaster.aspx/BindGroups",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);


                            $("#ddlIMGroup").html(obj.GroupOptions);


                        }, error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function (msg) {

                            $("#ddlIMGroup").val(obj.ItemOptions.GROUP_ID);
                            var GroupID = obj.ItemOptions.GROUP_ID;

                            $.ajax({
                                type: "POST",
                                data: '{ }',
                                url: "manageitemmaster.aspx/BindSubGroups1",
                                contentType: "application/json",
                                dataType: "json",
                                success: function (msg) {

                                    var obj = jQuery.parseJSON(msg.d);


                                    $("#ddlIMSubGroup").html(obj.SubGroupOptions);


                                }, error: function (xhr, ajaxOptions, thrownError) {

                                    var obj = jQuery.parseJSON(xhr.responseText);
                                    alert(obj.Message);
                                },
                                complete: function (msg) {


                                    $.uiUnlock();

                                    $("#ddlIMSubGroup").val(obj.ItemOptions.SGroup_id);
                                }

                            });



                        }

                    });




                    var company = obj.ItemOptions.COMPANY_ID;

                    $("#ddlIMCompany option[value='" + company + "']").prop("selected", true);


                    var tax = obj.ItemOptions.Tax_ID;
                    $("#ddlIMTax option[value='" + tax + "']").prop("selected", true);

                    var itemtype = obj.ItemOptions.Item_Type;
                    $("#ddlIMItemType option[value='" + itemtype + "']").prop("selected", true);

                    var excise = obj.ItemOptions.Excise_ID;
                    $("#ddlIMExicise option[value='" + excise + "']").prop("selected", true);
                    var HSN = obj.ItemOptions.HSNCode;
                    $("#ddlHSNCode option[value='" + HSN + "']").prop("selected", true);

                    var location = obj.ItemOptions.Location;
                    $("#ddlIMRackShelf option[value='" + location + "']").prop("selected", true);

                    var saleunit = obj.ItemOptions.Sales_In_Unit;
                    $("#ddlIMSaleUnit option[value='" + saleunit + "']").prop("selected", true);

                    var saleunit2 = obj.ItemOptions.Sales_In_Unit2;
                    $("#ddlIMWgtLtr option[value='" + saleunit2 + "']").prop("selected", true);


                    var transactionType = obj.ItemOptions.Transaction_Mode;


                    $("#ddlIMTransactionType option[value='" + transactionType + "']").prop("selected", true);


                    var category = obj.ItemOptions.COLOR_ID;
                    $("#ddlIMCategory option[value='" + category + "']").prop("selected", true);

                    $("#txtIMPurRate").val(obj.ItemOptions.Purchase_Rate);
                    $("#txtIMMaxRetailPrice").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtIMSaleRate").val(obj.ItemOptions.Sale_Rate);
                    $("#txtIMSaleRateExcl").val(obj.ItemOptions.Sale_Rate_Excl);
                    $("#txtIMDeiveryNoteRate").val(obj.ItemOptions.DeliveryNoteRate);
                    $("#txtIMMarkup").val(obj.ItemOptions.Mark_Up);
                    $("#txtIMDis1Per").val(obj.ItemOptions.Dis1Value);
                    $("#txtIMDis2Per").val(obj.ItemOptions.Dis2Value);
                    $("#chkIMPackedQty").val(obj.ItemOptions.InHouse_PackedQty);
                    $("#txtIMWholeSalerate").val(obj.ItemOptions.Whole_Sale_Rate);
                    $("#txtIMFranchiseRate").val(obj.ItemOptions.Franchise_SaleRate);
                    $("#txtIMWgtLtr").val(obj.ItemOptions.Packing);

                    var transactiontype = obj.ItemOptions.Transaction_Mode
                    $("#ddlIMTransactionType option[value='" + transactiontype + "']").prop("selected", true);






                    if (obj.ItemOptions.Discontinued == true) {

                        $('#chkIMItemDiscontinued').prop('checked', true);
                    }
                    else {
                        $('#chkIMItemDiscontinued').prop('checked', false);
                    }

                    if (obj.ItemOptions.Dis1InRs == true) {

                        $('#chkIMDis1Per').prop('checked', true);
                    }
                    else {
                        $('#chkIMDis1Per').prop('checked', false);
                    }


                    if (obj.ItemOptions.Dis2InRs == true) {

                        $('#chkIMDis2Per').prop('checked', true);
                    }
                    else {
                        $('#chkIMDis2Per').prop('checked', false);
                    }



                    if (obj.ItemOptions.InHouse_Packing == true) {

                        $('#chkInHousePacking').prop('checked', true);
                    }
                    else {
                        $('#chkInHousePacking').prop('checked', false);
                    }


                    if (obj.ItemOptions.Edit_SaleRate == true) {

                        $('#chkEditRate').prop('checked', true);
                    }
                    else {
                        $('#chkEditRate').prop('checked', false);
                    }


                    $("#txtIMLevel2").val(obj.ItemOptions.Sale_Rate2);
                    $("#txtIMLevel3").val(obj.ItemOptions.Sale_Rate3);
                    $("#txtIMLevel4").val(obj.ItemOptions.Sale_Rate4);

                    $("#txtIMDealerMargin").val(obj.ItemOptions.Delear_Margin);
                    $("#txtIMQtyInCase").val(obj.ItemOptions.Qty_in_Case);

                    // $("#ddlIMCurQtyRate").val();
                    // $("#txtIMWithTax").val();
                    $("#txtIMMaxLevel").val(obj.ItemOptions.Max_Level);
                    $("#txtIMMinLevel").val(obj.ItemOptions.Min_Level);
                    $("#txtIMReOrderOty").val(obj.ItemOptions.Re_Order_Qty);
                    $("#txtIMQtyToLess").val(obj.ItemOptions.Qty_To_Less);

                    if (obj.ItemOptions.VEG_NonVeg == "Veg") {

                        $('#rdoIMVeg').prop('checked', true);
                    }
                    else {
                        $('#rdoIMNonVeg').prop('checked', true);
                    }

                    $("#txtIMDays").val(obj.ItemOptions.Day);
                    if (obj.ItemOptions.BestExp == true) {

                        $('#rdoIMBestBefore').prop('checked', true);
                    }
                    else {
                        $('#rdoIMBestBefore').prop('checked', false);
                    }

                    $("#txtIMComments").val(obj.ItemOptions.Remarks);


                    if (obj.ItemOptions.CpPrinting == "True") {


                        $('#chkIMCouponPrinting').prop('checked', true);
                    }
                    else {
                        $('#chkIMCouponPrinting').prop('checked', false);
                    }


                    NutritionFactCollection = obj.NutritionFact;
                    BindRows();

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });


        }


        $("#btnIMCancel").click(
        function () {

            $("#hdnIMID").val("0");
            ClearItemDialog2();

        });

        $("#btnIMAdd").click(
        function () {




            if (!validateForm("dvDialog")) {
                return;
            }


            //Rate Validation

            if (parseFloat($("#txtIMMaxRetailPrice").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert("MRP should be greater than Purchase Rate.");
                $('#txtIMMaxRetailPrice').focus();
                return;
            }


            if (parseFloat($("#txtIMFranchiseRate").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert(" Franchise SaleRate should be greater than Purchase Rate.");
                $('#txtIMFranchiseRate').focus();
                return;
            }


            if (parseFloat($("#txtIMFranchiseRate").val()) > parseFloat($("#txtIMMaxRetailPrice").val())) {
                alert(" Franchise SaleRate should be lesser than MRP.");
                $('#txtIMFranchiseRate').focus();
                return;
            }

            if (parseFloat($("#txtIMWholeSalerate").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert("Whole SaleRate should be greater than Purchase Rate.");
                $('#txtIMWholeSalerate').focus();
                return;
            }

            if (parseFloat($("#txtIMWholeSalerate").val()) > parseFloat($("#txtIMMaxRetailPrice").val())) {
                alert(" Whole SaleRate should be lesser than MRP.");
                $('#txtIMWholeSalerate').focus();
                return;
            }



            if (parseFloat($("#txtIMSaleRate").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert("Sale Rate should be greater than Purchase Rate.");
                $('#txtIMSaleRate').focus();
                $('#txtIMSaleRateExcl').val("")
                return;
            }


            if (parseFloat($("#txtIMSaleRate").val()) > parseFloat($("#txtIMMaxRetailPrice").val())) {
                alert("Sale Rate should be lesser than MRP.");
                $('#txtIMSaleRate').focus();
                $('#txtIMSaleRateExcl').val("")
                return;
            }




            //            if ($("#ddlIMTax").val() == "0") {
            //                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val());
            //            }
            //            else {
            //                var TaxAmt = (float.Parse($('#txtIMSaleRate').val()) * $("#ddlIMTax option:selected").text()) / 100;
            //                $("#txtIMSaleRateExcl").val(float.Parse($('#txtIMSaleRate').val()) - TaxAmt);
            //            }


            //            if ($('#txtIMSaleRate').val() != "") {
            //                var TaxAmt = ($('#txtIMSaleRate').val() * $("#ddlIMTax option:selected").text()) / 100;
            //                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val() - TaxAmt);
            //            }

            //-----------

            var objItemMaster = {};
            objItemMaster.ItemId = $("#hdnIMID").val();
            objItemMaster.COMPANY_ID = $("#ddlIMCompany").val();
            objItemMaster.GROUP_ID = $("#ddlIMGroup").val();
            //objItemMaster.Master_Code = $("#txtIMMasterCode").val();
            objItemMaster.Item_Code = $("#txtIMItemCode").val();
            objItemMaster.posid = $(".dd_pos option:selected").val();

            objItemMaster.Bar_Code = $("#txtIMBarCode").val();

            objItemMaster.Item_Name = $("#txtIMItemName").val();
            objItemMaster.Packing = $("#txtIMWgtLtr").val();
            objItemMaster.Short_Name1 = $("#txtIMShortName1").val();
            objItemMaster.Short_Name2 = $("#txtIMShortName2").val();
            objItemMaster.Sales_In_Unit = $("#ddlIMSaleUnit").val();
            objItemMaster.Sales_In_Unit2 = $("#ddlIMWgtLtr").val();

            objItemMaster.Qty_To_Less = $("#txtIMQtyToLess").val();
            objItemMaster.Sale_Rate2 = $("#txtIMLevel2").val();
            objItemMaster.Sale_Rate3 = $("#txtIMLevel3").val();
            objItemMaster.Sale_Rate4 = $("#txtIMLevel4").val();
            objItemMaster.Purchase_Rate = $("#txtIMPurRate").val();


            objItemMaster.Max_Retail_Price = $("#txtIMMaxRetailPrice").val();
            objItemMaster.Mark_Up = $("#txtIMMarkup").val();
            objItemMaster.Qty_in_Case = $("#txtIMQtyInCase").val();
            objItemMaster.Max_Level = $("#txtIMMaxLevel").val();
            objItemMaster.Min_Level = $("#txtIMMinLevel").val();
            objItemMaster.Re_Order_Qty = $("#txtIMReOrderOty").val();

            objItemMaster.Tax_Code = $("#ddlIMTax option:selected").text();
            objItemMaster.Pur_Code = Purcode;
            objItemMaster.Sale_Code = salecode;
            objItemMaster.IsSaleable = true;
            objItemMaster.Delear_Margin = $("#txtIMDealerMargin").val();
            objItemMaster.Tax_AfterBefore = "1";
            objItemMaster.Location = $("#ddlIMRackShelf").val();
            objItemMaster.Discount = 0;
            objItemMaster.Department = $("#ddlIMDepartment").val();
            objItemMaster.Sale_Rate = $("#txtIMSaleRate").val();
            objItemMaster.Sale_Rate_Excl = $("#txtIMSaleRateExcl").val();
            objItemMaster.Whole_Sale_Rate = $("#txtIMWholeSalerate").val();
            objItemMaster.DeliveryNoteRate = $("#txtIMDeiveryNoteRate").val();
            objItemMaster.Delivery_No = 0;
            objItemMaster.SGroup_id = $("#ddlIMSubGroup").val();
            objItemMaster.TAGGED = false;
            objItemMaster.Franchise_SaleRate = $("#txtIMFranchiseRate").val();
            objItemMaster.Transaction_Mode = $("#ddlIMTransactionType option:selected").text();
            objItemMaster.Item_Type = $("#ddlIMItemType").val();
            var cursoron = $("#ddlIMCurQtyRate").val();


            if (cursoron == "(NONE)") {
               
                cursoron = "QTY";
            }
            objItemMaster.Cursor_On = cursoron;


            objItemMaster.Ingredients = $("#txtNutritionRemarks").val();
            objItemMaster.Discontinued = false;
            if ($('#chkIMItemDiscontinued').is(":checked")) {
                objItemMaster.Discontinued = true;
            }


            objItemMaster.InHouse_Packing = false;
            objItemMaster.InHouse_PackedQty = 0;
            if ($('#chkInHousePacking').is(":checked")) {
                objItemMaster.InHouse_Packing = true;
                objItemMaster.InHouse_PackedQty = $("#chkIMPackedQty").val();
            }

            objItemMaster.Dis1InRs = false;
            objItemMaster.Dis1Value = 0;
            if ($('#chkIMDis1Per').is(":checked")) {
                objItemMaster.Dis1InRs = true;
                objItemMaster.Dis1Value = $("#txtIMDis1Per").val();
            }

            objItemMaster.Dis2InRs = false;
            objItemMaster.Dis2Value = 0;
            if ($('#chkIMDis2Per').is(":checked")) {
                objItemMaster.Dis2InRs = true;
                objItemMaster.Dis2Value = $("#txtIMDis2Per").val();
            }
            objItemMaster.IsCompBarCode = false;
            objItemMaster.Remarks = $("#txtIMComments").val();
            objItemMaster.AllowPoint = false;
            objItemMaster.Tax_ID = $("#ddlIMTax").val();

            objItemMaster.CpPrinting = false;
            if ($('#chkIMCouponPrinting').is(":checked")) {
                objItemMaster.CpPrinting = true;
            }
            objItemMaster.Excise_ID = 0;
            objItemMaster.Excise_Code = 0;
            if ($("#ddlIMExicise").val() != 0) {
                objItemMaster.Excise_ID = $("#ddlIMExicise").val();
                objItemMaster.Excise_Code = $("#ddlIMExicise option:selected").val();
            }

            objItemMaster.HSNCode = 0;
            if ($("#ddlHSNCode").val() != 0) {
                objItemMaster.HSNCode = $("#ddlHSNCode option:selected").text();

            }
            objItemMaster.ImageUrl = "";
            objItemMaster.SubGroupId = $("#ddlIMSubGroup").val();
            objItemMaster.COLOR_ID = 0;
            if ($("#ddlIMCategory").val() != 0) {
                objItemMaster.COLOR_ID = $("#ddlIMCategory").val();
            }

            objItemMaster.Likes = 0;
            objItemMaster.Day = $("#txtIMDays").val();

            objItemMaster.BestExp = false;
            if ($('#rdoIMBestBefore').is(":checked")) {
                objItemMaster.BestExp = true;
            }

            objItemMaster.VEG_NonVeg = "";
            if ($('#rdoIMVeg').is(":checked")) {
                objItemMaster.VEG_NonVeg = "Veg";
            }
            else if ($('#rdoIMNonVeg').is(":checked")) {
                objItemMaster.VEG_NonVeg = "NonVeg";
            }

            //            objItemMaster.IsSubItem = false;
            //            if ($('#chkIMSubItem').is(":checked")) {
            //                objItemMaster.IsSubItem = true;
            //            }



            objItemMaster.IsSubItem = false;
            if ($('#chkIMSubItem').is(":checked")) {
                objItemMaster.IsSubItem = true

                if ($("#ddlIMSubItem").val() == null || $("#ddlIMSubItem").val() == "") {

                    $("#txtddlIMSubItem").focus();
                    alert("Please choose Sub Item First");
                    return;

                }


            }

            objItemMaster.ParentItemCode = $("#ddlIMSubItem").val();




            //            objItemMaster.DealerQtyRate = $("#ddlIMCurQtyRate").val();
            //            objItemMaster.PurRateWithTax = $("#txtIMWithTax").val();
            //            objItemMaster.LatestPurRate = $("#txtIMLatestPurRate").val();

            objItemMaster.Expiry = false;
            if ($('#rdoIMExpiry').is(":checked")) {
                objItemMaster.Expiry = true;
            }

            objItemMaster.Edit_SaleRate = false;
            if ($('#chkEditRate').is(":checked")) {
                objItemMaster.Edit_SaleRate = true;
            }
            var DTO = { 'objItemMaster': objItemMaster, 'objNutritionFacts': NutritionFactCollection };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "manageitemmaster.aspx/InsertUpdate",
                data: JSON.stringify(DTO),
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -1) {

                        alert("Insertion Failed.Item already exists.");
                        return;
                    }

                    else if (obj.Status == -3) {
                        alert("Insertion Failed.Item already exists against same company");
                        return;

                    }
                    else if (obj.Status == -2) {

                        alert("Insertion Failed.Item already exists against same Code.");
                        return;
                    }
                    var Id = $("#hdnIMID").val();

                    if (Id == "0") {

                        alert("Item is added successfully.");
                        ClearItemDialog();

                    }
                    else {

                        alert("Item is Updated successfully.");
                        ClearItemDialog();
                    }
                    BindGrid();


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });


        }

        );



    }

    );

    function showHide(val) {

        var divP = document.getElementById("view1");
        var divD = document.getElementById("view2");

        if (val == 1) {
            document.getElementById("ancV1").style.borderBottom = "solid 1px white";
            document.getElementById("ancV1").style.backgroundColor = "white";

            document.getElementById("ancV2").style.borderBottom = "solid 1px #B7B7B7";
            document.getElementById("ancV2").style.backgroundColor = "";

            divP.style.display = "block";
            divD.style.display = "none";

        }
        else {
            document.getElementById("ancV1").style.borderBottom = "solid 1px #B7B7B7";
            document.getElementById("ancV1").style.backgroundColor = "";

            document.getElementById("ancV2").style.borderBottom = "solid 1px white";
            document.getElementById("ancV2").style.backgroundColor = "white";

            divP.style.display = "none";
            divD.style.display = "block";

        }
    }



</script>
<div id="dvDialog" style="font-size:11px;background:#333;color:White;">
    <ul data-persist="true" class="tabs">
        <li><a href="javascript:showHide(1,'ancV1');" style="background-color: white; border-bottom: 1px solid white;"
            id="ancV1">Item Information</a></li>
        <li><a href="javascript:showHide(2,'ancV2');" id="ancV2" style="border-bottom: 1px solid rgb(183, 183, 183);">
            Nutritution Facts</a></li>
    </ul>
    <div class="tabcontents" style="background:none;">
        <div id="view1" style="display: block;">
            <table style="width: 100%">
                <tr>
                    <td>
                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; margin-top: 5px;
                            width: 100%; border-collapse: separate; border-spacing: 5px">
                            <tr>
                                 <th colspan="100%" style='background: #db3030'>
                                    BASIC INFORMATION
                                </th>
                            </tr>
                           <%-- <tr  style="display:none"> <td colspan="2">
                                   Master Code:
                                </td>
                                <td colspan="100%">
                                    
                                    <input type="text" id="txtIMMasterCode" style="width: 80px;"   />
                                </td></tr> 
                            <tr>--%>
                            <tr> 
                                <td>
                                    Code:
                                </td>
                                <td>
                                    
                                    <input type="text" id="txtIMItemCode" data-index="1"  style="width: 80px;text-transform:uppercase;color:Black;"  class="cls validate required"  />
                                </td>
                                <td>
                                    Name:
                                </td>
                                <td>
                                    <input type="text" id="txtIMItemName" data-index="2"  style="text-transform:uppercase;color:Black;"class="cls"  />
                                </td>
                                <td>
                                    Short Name1:
                                </td>
                                <td>
                                    <input type="text" id="txtIMShortName1"  data-index="3" style="text-transform:uppercase;color:Black;" data-index="3"/>
                                </td>
                                <td>
                                    Short Name2:
                                </td>
                                <td>
                                    <input type="text" id="txtIMShortName2"  data-index="4" style="text-transform:uppercase;color:Black;" data-index="4" />
                                </td>
                                <td>
                                    Barcode:
                                </td>
                                <td>
                                    <input type="text" id="txtIMBarCode" data-index="5" style="text-transform:uppercase;color:Black;" style="width: 80px" data-index="5"   />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="margin-top: 15px; margin-bottom: 10px">
                            <tr>
                                <td valign="top" style="padding: 5px">
                                    <table cellspacing="5" cellpadding="5" style="  border: solid 1px silver;
                                        width: 100%; border-collapse: separate; border-spacing: 5px;height:312px">
                                        <thead>
                                            <tr>
                                                <th colspan="100%" colspan="100%" style='background:#db3030'>
                                                    MASTER INFORMATION
                                                </th>
                                            </tr>
                                        </thead>
                                                                                     <tr>
                                                     <td>
                                                POS:
                                            </td>
                                                    <td>
                 <select id="ddpos" class="dd_pos form-control" style="width: 9em;height: 28px;color:  black;">
    <asp:Repeater ID="rptr_ddpos" runat="server">
        <ItemTemplate>
 
    <option value='<%#Eval("posid") %>' ><%#Eval("title") %></option>
                   </ItemTemplate>   </asp:Repeater>                                   </select>

                                                    </td>
                                                </tr>
                                        <tr>
                                            <td>
                                                Department:
                                            </td>
                                            <td>
                                            <table>
   
                                            <tr>
                                            <td>
                                            
                                               <select id="ddlIMDepartment" data-index="6" class="validate ddlrequired" onchange="javascript:BindIMGroups();" style="width: 100px;color:black">
                                                </select>
                                            </td>
                                            <td>
                                            <span style="font-size:17px;padding:10px 0px 0px 3px;cursor:pointer" onclick="javascript:OpenVMDialog('Department')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>


                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Group:
                                            </td>
                                            <td>
                                                <select id="ddlIMGroup" data-index="7" class="validate ddlrequired"  onchange="javascript:BindIMSubGroups()" style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Subgroup:
                                            </td>
                                            <td>
                                                <select id="ddlIMSubGroup" data-index="8"   style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Company:
                                            </td>
                                            <td>
                                                <select id="ddlIMCompany" data-index="9" class="validate ddlrequired"  style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Category:
                                            </td>
                                            <td>

                                             <table>
                                            <tr>
                                            <td>
                                            
                                                <select id="ddlIMCategory" data-index="10"  style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                            <td>
                                            <span style="font-size:17px;padding:10px 0px 0px 3px;cursor:pointer" onclick="javascript:OpenVMDialog('Color')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>



                                               
                                              
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tax:
                                            </td>
                                            <td>
                                                <select id="ddlIMTax" data-index="11" class="validate ddlrequired"  style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Item Type:
                                            </td>
                                            <td>


                                            
                                             <table>
                                            <tr>
                                            <td>
                                            
                                                <select id="ddlIMItemType" data-index="12"  class="validate ddlrequired" style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                            <td>
                                            <span style="font-size:17px;padding:10px 0px 0px 3px;cursor:pointer" onclick="javascript:OpenVMDialog('ItemType')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>

                                               
                                              
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Excise/Terrif:
                                            </td>
                                            <td>
                                                <select id="ddlIMExicise" data-index="13"  style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                        </tr>
										<tr>
                                            <td>
                                                HSN Code:
                                            </td>
                                            <td>
                                                <select id="ddlHSNCode" data-index="14"  style="width: 100px;color:Black;">
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="padding: 5px">
                                    <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; border-spacing: 5px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                RATE INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Pur. Rate/Unit
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMPurRate" data-index="14" class="validate required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Max. Retail Price
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMaxRetailPrice" data-index="15" class="validate  required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>

                                          <tr>
                                            <td>
                                                Franchise Rate
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMFranchiseRate" data-index="16" class="validate  required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                Whole Sale Rate
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMWholeSalerate" data-index="17" class="validate  required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Sale Rate/Unit
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMSaleRate" data-index="18"  class="validate  required float" style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Sale Rate(Excl.)
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMSaleRateExcl" data-index="19" class="validate  required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Mark Up(%)
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMarkup" data-index="20" class="validate  required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Rack & Shelf
                                            </td>
                                            <td>

                                            
                                             <table>
                                            <tr>
                                            <td>
                                            
                                             
                                                <select style="width: 70px;color:Black;" data-index="21"  id="ddlIMRackShelf">
                                                </select>
                                            </td>
                                            <td>
                                            <span style="font-size:17px;padding:10px 0px 0px 3px;cursor:pointer" onclick="javascript:OpenVMDialog('Location')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>


                                             
                                            </td>
                                        </tr>

                                         <tr>
                                            <td>
                                                DeliveryNote Rate
                                            </td>
                                            <td>
                                                <input type="text" data-index="22" id="txtIMDeiveryNoteRate" class="validate  required float"  style="width: 70px;color:Black;" />
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                           
                                            <td>
                                                 <input type="checkbox" data-index="27" id="chkEditRate" /><label>Edit Sale/MRP Rate</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="padding: 5px">
                                    <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; border-spacing: 5px;height: 106px;">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                QTY INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Sales Unit
                                            </td>
                                            <td>
                                                <select style="width: 100px;color:Black;" data-index="23" id="ddlIMSaleUnit">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Wgt/Ltr/Pcs
                                            </td>
                                            <td>
                                                <input type="text" style="width: 33px;margin-right:2px;color:Black;" data-index="24" id="txtIMWgtLtr" /><select id="ddlIMWgtLtr" data-index="25"
                                                    style="width: 65px;height:21px;color:black"></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Transaction Type
                                            </td>
                                            <td>
                                                <select style="width: 100px;color:Black;" data-index="26" id="ddlIMTransactionType">
                                                <option value="Purchase Only">Purchase Only</option>
                                                <option value="Sale Only">Sale Only</option>
                                                <option value="Purchase & Sale Both">Purchase & Sale Both</option>


                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellspacing="5" cellpadding="5" style="margin-top: 5px; border: solid 1px silver;
                                        width: 100%; border-collapse: separate; border-spacing: 5px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                PACKING INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                                <input type="checkbox" data-index="27" id="chkInHousePacking" /><label>InHouse Packing</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Packed Qty.
                                            </td>
                                            <td>
                                                <input type="text" data-index="28" id="chkIMPackedQty" style="width: 100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" data-index="29" id="chkIMDis1Per" />
                                                Dis1(%)
                                            </td>
                                            <td>
                                                <input type="text" data-index="30" id="txtIMDis1Per" style="width: 100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" data-index="31" id="chkIMDis2Per" />
                                                Dis2(%)
                                            </td>
                                            <td>
                                                <input type="text" data-index=32" id="txtIMDis2Per" style="width: 100%" />
                                            </td>
                                        </tr>

                                         <tr>
                                            <td colspan="100%">
                                                <input type="checkbox" data-index="33" id="chkIMItemDiscontinued" /><label>Item Discontinued</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                                <input type="checkbox" data-index="34" id="chkIMCouponPrinting" /><label>Coupon Printing</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="padding: 5px">
                                    <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; border-spacing: 5px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                OTHER INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="5" cellpadding="5" style="width: 100%;color:Black;; border-collapse: separate;
                                                    border-spacing: 5px">
                                                    <tr>
                                                        <td>
                                                            Days:
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtIMDays" data-index="35" value="0"  style="width: 40px" />
                                                        </td>
                                                        <td>
                                                            <input type="radio" checked="checked" data-index="36" name="expiry" id="rdoIMExpiry" /><label>Expiry</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio"  name="expiry" data-index="37" id="rdoIMBestBefore" /><label>Best Before</label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;
                                                    border-spacing: 5px">
                                                    <tr>
                                                        <td>
                                                            <input type="radio" checked="checked" data-index="38"  name="veg" id="rdoIMVeg" /><label>Veg.</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio" name="veg" data-index="39"  id="rdoIMNonVeg" /><label>Non Veg.</label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;
                                                    border-spacing: 5px">
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" id="chkIMSubItem" data-index="40" /><label>Sub-Item</label>
                                                        </td>
                                                        <td>
                                                            <select id="ddlIMSubItem" data-index="41"   ></select>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Qty To Less
                                                        </td>
                                                        <td>
                                                            <input type="text" value="0" data-index="42"  class="validate required  float"    id="txtIMQtyToLess" style="color:black"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellspacing="5" cellpadding="5" style="margin-top: 5px; border: solid 1px silver;
                                        width: 100%; border-collapse: separate; border-spacing: 5px;height:125px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                STOCK LEVELING
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Max Level
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMaxLevel" data-index="43" value="0"  class="validate required  float"  style="width: 50px;color:Black;" />
                                            </td>
                                            <td>
                                                Min Level
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMinLevel"  value="0" data-index="44"  class="validate required  float"   style="width: 50px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Re. Order Qty.
                                                        </td>
                                                        <td colspan="100%">
                                                            <input type="text" value="0" data-index="45"  class="validate required  float"   id="txtIMReOrderOty"style="color:Black;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="padding: 10px">
                                    <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; border-spacing: 5px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                RATE LEVELING
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Level 2
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="46"  id="txtIMLevel2"  class="validate required float" style="width: 80px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Level 3
                                            </td>
                                            <td>
                                                <input type="text"  value="0" data-index="47" id="txtIMLevel3" class="validate required  float"  style="width: 80px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Level 4
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="48"  id="txtIMLevel4" class="validate required  float"  style="width: 80px;color:Black;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 5px">
                                    <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; border-spacing: 5px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                DEALER INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Cursor On(Qty/Rate)
                                            </td>
                                            <td>
                                                <select id="ddlIMCurQtyRate"style="color:Black;" data-index="49">
                                                <option value="(NONE)">(NONE)</option>
                                                <option value="NAME">Name</option>
                                                <option  value="QTY">Qty</option>
                                                <option  value="RATE">Rate</option>
                                                <option  value="AMOUNT">Amount</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Dealer Margin (%)
                                            </td>
                                            <td>
                                                <input type="text" data-index="50" value="0"  id="txtIMDealerMargin" class="validate required  float"  style="width: 80px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Qty In Case
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="51"  id="txtIMQtyInCase" class="validate required  float"  style="width: 80px;color:Black;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 10px">
                                    <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; border-spacing: 5px">
                                        <tr>
                                            <th colspan="100%" style='background: #db3030'>
                                                PURCHASE RATE INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                With Tax
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="52"  id="txtIMWithTax"  class="validate required  float"  style="width: 80px;color:Black;" />
                                            </td>
                                            <td>
                                                Latest Pur. Rate
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="53"  id="txtIMLatestPurRate" class="validate required  float"   style="width: 80px;color:Black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Comments
                                            </td>
                                            <td colspan="100%">
                                                <textarea style="height: 57px;text-transform:uppercase" data-index="54" id="txtIMComments" ></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="middle" style="padding-left: 20px;">
                                    <table>
                                        <tr>
                                            <td>
                                                 <button class="btn btn-primary" data-index="55" id="btnIMAdd" style="width: 120px;height: 45px;background: black;border-color: white;font-size: 15px;color:White;">
                                                  <i class="fa fa-save"></i>
  Save</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button class="btn btn-danger" id="btnIMCancel" style="width: 120px;height: 45px;background: black;border-color: white;color:white;font-size: 15px;">
                                                  <i class="fa fa-mail-reply-all"></i>  Cancel</button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="display: none;" id="view2">
            <div style="height: 300px; overflow-y: scroll;">
                <table class="table" style="width: 100%">
                   <thead>
                    <tr>
                        <th>
                            Sr
                        </th>
                        <th>
                            Bold
                        </th>
                        <th>
                            Field1
                        </th>
                        <th>
                            Field2
                        </th>
                        <th>
                            Field3
                        </th>
                        <th>
                        </th>
                    </tr>

                    <tr><td></td><td><input type="checkbox" id="chkIMBold"/></td><td><input type="text" id="txtIMField1"/></td><td><input type="text" id="txtIMField2"/></td><td><input type="text" id="txtIMField3"/></td>
                    
                    <td>
                    <button type="button" class="btn btn-success"  id="btnAddNutrition">Add</button>
                    </td></tr>
                    </thead>
                   
                      <tbody id="tbNutritionInfo">
              
              
                      </tbody>
                </table>
            </div>
            <br />
            Ingredients:
            <textarea id="txtNutritionRemarks"></textarea>
        </div>
    </div>
</div>
