﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="placeorder.aspx.cs" Inherits="placeorder" %>

<%@ Register src="~/Templates/OrderTemplates/OrderBooking.ascx" tagname="OrderBooking" tagprefix="uc1" %>
<%@ Register src="~/Templates/OrderTemplates/OrderDispatch.ascx" tagname="OrderDispatch" tagprefix="uc2" %>
<%@ Register src="~/Templates/AddCashCustomer.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

    <form id="form1" runat="server">
     <asp:HiddenField ID="hdnDate" runat="server"/>
  
  
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
   
    
    <script src="Scripts/knockout-3.0.0.js"></script>
    <script src="ViewModel/OrderViewModel.js"></script>
   <script src="js/customValidation.js" type="text/javascript"></script>
    <script src="Scripts/knockout.validation.js" type="text/javascript"></script>
  
    <style type="text/css">
    
       .customTextBox
    {
        width:62px;
        margin-right:2px;
        margin-left:2px;
    }
    #tbCal tr td
    {
        padding:2px;
        }
        
        .validationMessage
        {
            color:Red;
            }
    
    </style>
    
 
<div class="right_col" role="main">
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>ORDERS LIST</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                             
                              

                                 <div class="form-group">
                                
                                <table>
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                 <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span1" class="sr-only">(success)</span>
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                 <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span2" class="sr-only">(success)</span>
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  >
                                
                                Go</div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                </div>
                               


                                   <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                <button type="button" id="btnNew" class="btn btn-primary">New</button>
                                                <button type="button" id="btnEdit" data-bind="click:EditOrder" class="btn btn-danger">Edit</button>
                                                <button type="button" id="btnDispatch" class="btn btn-success">Dispatch</button>
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                     

<div class="row" id="orderDialog" style="display:none">
<uc1:OrderBooking ID="ucOrderBooking" runat="server" />
</div>




<div class="row" id="dispatchDialog" style="display:none">
<uc2:OrderDispatch ID="ucOrderDispatch" runat="server" />
     



</div>



<div class="row" id="CustomerDialog" style="display:none">
<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />
</div>

 
                    
                    <script type="text/javascript">

                        function GetCustomerSearchResult(CustomerId, Name, Mobile,Address) {


                            $("#txtMobile").val(Mobile).change();
                            $("#txtCustomerName").val(Name).change();
                            $("#txtAddress").val(Address).change();
                           
                        }



                        var CustomerID = 0;
                        var TotalVat = 0;
                        var TotalAmount = 0;
                        var m_OrderNo = 0;
                        var ProductCollection = [];
                        function clsProduct() {

                            this.ItemCode = 0;
                            this.ItemName = 0;
                            this.Weight = 0;
                            this.Qty = 0;
                            this.DispatchdQty = 0;
                            this.DispatchQty = 0;
                            this.Rate = 0;
                            this.Amount = 0;
                            this.MRP = 0;
                            this.Vat = 0;
                        }

                        function QtyChange(counterId) {


                            var Qty = ProductCollection[counterId]["Qty"];
                            var Disptched = ProductCollection[counterId]["DispatchdQty"];

                            var DQty = $("#txtQty" + counterId).val();
                            if (DQty > Qty - Disptched) {
                                DQty = Qty;

                            }

                            $("#txtQty" + counterId).val(DQty);
                            var ItemCode = ProductCollection[counterId]["ItemCode"];

                            var Rate = ProductCollection[counterId]["Rate"];

                            var Amount = Number(Number(Rate) * Number(DQty));

                            $("#lblamount" + counterId).text(Amount);
                            ProductCollection[counterId]["Amount"] = Amount;
                          
                            commoncalculation();


                        }


                        function InsertUpdateDispatch()
                        {

                     
                            var OrderNo = $("#txtOrderNo").val();
                            var CustomerCode = CustomerID;
                            var DispatchValue = $("#txtBillValue").val();
                            var DiscountPer = $("#txtDiscountPer").val();
                            var Discount = $("#txtDiscountAmt").val();
                            var TaxAmount = $("#txtTotalVat").val();
                            var NetAmount = $("#txtTotalNetAmount").val();
                            var Paymode = $("#ddlPrefix").val();
                            var CrPay = 0;
                            var CrPayLeft = 0;
                            if (Paymode == "Credit")
                            {
                                CrPay = $("#txtCashRecvd").val();
                                CrPayLeft = Number(NetAmount) - Number(CrPay);
                            }


                            var ItemCode = [];
                            var ItemName = [];
                            var Weight = [];
                            var MRP = [];
                            var Rate = [];
                            var Tax = [];
                            var DispQty = [];
                          
                          

                            for (var i = 0; i < ProductCollection.length; i++) {

                                if (ProductCollection[i]["DispatchQty"] != "0")
                                ItemCode[i] = ProductCollection[i]["ItemCode"];
                                ItemName[i] = ProductCollection[i]["ItemName"];
                                Weight[i] = ProductCollection[i]["Weight"];
                                MRP[i] = ProductCollection[i]["MRP"];
                                Rate[i] = ProductCollection[i]["Rate"];
                                Tax[i] = ProductCollection[i]["Vat"];
                                DispQty[i] = ProductCollection[i]["DispatchQty"];
                                
                            }


                           

                $.ajax({
                    type: "POST",
                    data: '{"OrderNo": "' + OrderNo + '","CustomerCode": "' + CustomerCode + '","DispatchValue": "' + DispatchValue + '","Discount": "' + Discount + '","DiscountPer": "' + DiscountPer + '","TaxAmount": "' + TaxAmount + '","NetAmount": "' + NetAmount + '","PayMode": "' + Paymode + '","CrPay": "' + CrPay + '","CrPayLeft": "' + CrPayLeft + '","arrItemCode": "' + ItemCode + '","arrItemName": "' + ItemName + '","arrWeight": "' + Weight + '","arrMRP": "' + MRP + '","arrRate": "' + Rate + '","arrTax": "' + Tax + '","arrDispQty": "' + DispQty + '"}',

                    url: "placeorder.aspx/InsertUpdateDispatch",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);
                        if (obj.Status == "0") {
                        
                        }




                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                     
                    }

                });
            }



                        function BindProducts() {

                            $("#txtBillValue").val(0);
                            $("#txtDiscountAmt").val(0);
                            $("#txtTotalVat").val(0);
                            $("#txtTotalNetAmount").val(0);
                            $("#txtCashRecvd").val(0);

                            ProductCollection = [];

                            $('#tbDispatchProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                            $.ajax({
                                type: "POST",
                                data: '{ "OrderNo": "' + m_OrderNo + '"}',
                                url: "placeorder.aspx/GetDispatchDetailByOrderNo",
                                contentType: "application/json",
                                dataType: "json",
                                success: function (msg) {

                                    ProductCollection = [];
                                    CustomerID = "0";
                                    var tr = "";
                                    var obj = jQuery.parseJSON(msg.d);

                                    $("#txtOrderNo").val(obj.Booking.OrderNo);
                                    CustomerID = obj.Booking.Customer_ID;
                                    $("#txtCname").val(obj.Booking.CustomerName);
                                    $("#txtAdv").val(obj.Booking.Advance);
                                    var OrderValue = (Number(obj.Booking.NetAmount)) - (Number(obj.Booking.VatAmount)) + (Number(obj.Booking.DisAmt));


                                    $("#txtOValue").val(Number(obj.Booking.NetAmount) - Number(obj.Booking.VatAmount) + Number(obj.Booking.DisAmt));
                                    var DiscountPer = Number(obj.Booking.DisAmt) * 100 / Number($("#txtOValue").val());
                                    $("#txtDiscountPer").val(DiscountPer);
                                    $("#txtLastBalance").val(Number(OrderValue) - Number($("#txtAdv").val()));

                                    var counterId = 0;
                                    for (var i = 0; i < obj.BookingData.length; i++) {

                                        tr = tr + "<tr><td>" + obj.BookingData[i]["Code"] + "</td><td>" + obj.BookingData[i]["Name"] + "</td><td>"
 + obj.BookingData[i]["Weight"] + "</td><td>" + obj.BookingData[i]["Qty"] + "</td><td>" + obj.BookingData[i]["DisptchdQty"] + "</td><td>"
 + obj.BookingData[i]["Rate"] + "</td><td><input type='text' style ='width:80px' value ='0' id='txtQty"
 + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' /></td><td><label id='lblamount" + counterId + "'>0</label></td><td>"
 + obj.BookingData[i]["Vat"] + "</td></tr>";


                                        var PO = new clsProduct();
                                        PO.ItemCode = obj.BookingData[i]["Code"];
                                        PO.ItemName = obj.BookingData[i]["Name"];
                                        PO.Weight = obj.BookingData[i]["Weight"];
                                        PO.Qty = obj.BookingData[i]["Qty"];
                                        PO.DispatchdQty = obj.BookingData[i]["DisptchdQty"];
                                        PO.DispatchQty = $("#txtQty" + counterId).val();
                                        PO.Rate = obj.BookingData[i]["Rate"];
                                        PO.Vat = obj.BookingData[i]["Vat"];
                                        PO.Amount = $("#lblamount" + counterId).text();

                                        ProductCollection.push(PO);




                                        TotalVat = Number(TotalVat) + Number(ProductCollection[counterId]["Vat"]);

                                        //   TotalAmount = TotalAmount + ProductCollection[counterId]["Amount"];
                                        counterId = counterId + 1;


                                    }

                                    $("#tbDispatchProducts").append(tr);
                                    //commoncalculation();

                                },
                                complete: function (msg) {



                                    $("#dispatchDialog").dialog({ autoOpen: true,

                                        width: 1115,
                                        resizable: false,
                                        modal: true
                                    });

                                }

                            });
                        }

                        function commoncalculation() {
                            TotalAmount = 0;
                            for (var i = 0; i < ProductCollection.length; i++) {

                                TotalAmount = TotalAmount + ProductCollection[i]["Amount"];
                            }
                            $("#txtBillValue").val(TotalAmount.toFixed(2));
                            var DiscountAmt = Number(TotalAmount) * Number($("#txtDiscountPer").val()) / 100;
                            $("#txtDiscountAmt").val(DiscountAmt).toFixed(2);
                            $("#txtTotalVat").val(TotalVat);
                            $("#txtTotalNetAmount").val((Number(TotalAmount) - Number(DiscountAmt)) + Number(TotalVat)).toFixed(2);

                        }
                        $(document).ready(function () {

                            $("#btnAddCustomer").click(
                            function () {

                                $("#CustomerDialog").dialog({ autoOpen: true,

                                    width: 1115,
                                    resizable: false,
                                    modal: true
                                });
                            }
                            );

                            $("#txtDateFrom,#txtDateTo").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();

                            $("#btnNew").click(
                            function () {


                                $('#orderDialog').dialog({ autoOpen: true,

                                    width: 1115,
                                    resizable: false,
                                    modal: true
                                });
                            }
                            );

                            //                        

                            $("#btnDispatch").click(
                                                     function () {

                                                         if (m_OrderNo == "0") {
                                                             alert("No Order is selected for dispatch");
                                                             return;
                                                         }


                                                         BindProducts();
                                                     }
                                                      );




                            $("#btnBillSave").click(
                         function () {

                             InsertUpdateDispatch();


                         }
                          );


                            $("#btnGo").click(
                     function () {

                         BindGrid();

                     }
                      );





                            $('#txtOrderDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                            $('#txtDeliveryDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            
    </form>


  <script language="javascript">
   function BindGrid()
     {
      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/manageorderbookings.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['OrderNo','OrderDate','CustomerID','CustomerName','Address','DeliveryType','DeliveryTime','DeliveryAddress','NetAmount','VatAmount','Discount','AdvancePaid','PayMode','Balance','Remarks'],
            colModel: [
              { name: 'OrderNo',key:true, index: 'OrderNo', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                       
                         { name: 'strOD', index: 'strOD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},                      
                         { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'CustomerName', index: 'CustomerName', width: 150, stype: 'text', sortable: true, editable: true, hidden: false  ,editrules: { required: true }},
                        { name: 'Address', index: 'Address', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
   		                   { name: 'DeliveryType', index: 'DeliveryType', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'strDD', index: 'strDD', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                      
                      { name: 'DeliveryAddress',key:true, index: 'DeliveryAddress', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                         { name: 'NetAmount', index: 'NetAmount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                 { name: 'VatAmount', index: 'VatAmount', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }},
                         { name: 'DisAmt', index: 'DisAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'Advance', index: 'Advance', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},   
   		                   { name: 'PaymentMode', index: 'PaymentMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'LeftPayRecd', index: 'LeftPayRecd', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                         { name: 'Remarks',key:true, index: 'Remarks', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                       ],
            rowNum: 10,
          
            mtype: 'GET',
              toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'OrderNo',
            viewrecords: true,
            height: "100%",
            width:"1100px",
            sortorder: 'desc',
             ignoreCase: true,
            caption: "Order Bookings List",
         
           
                    
             
        });

        var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });




             $("#jQGridDemo").jqGrid('setGridParam',
         {

             onSelectRow: function (rowid, iRow, iCol, e) {
            

          
            m_OrderNo = 0;
             m_OrderNo = $('#jQGridDemo').jqGrid('getCell', rowid, 'OrderNo');
             
           
             }
         });





           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '1100');
        

      }
  </script>
  <script src="ViewModel/PopUp.js" type="text/javascript"></script>          
</asp:Content>

