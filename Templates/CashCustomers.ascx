﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CashCustomers.ascx.cs" Inherits="Templates_CashCustomers" %>
<script language="javascript">

    var m_CustomrId = -1;

    $(document).ready(function () {



      $("#cm_DOB").val("1900-01-01");
      $("#cm_DOA").val("1900-01-01");
      $("#cm_Discount").val("0");
      $("#cm_Tag").val("0");



      $('#cm_DOB').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1"
      }, function (start, end, label) {


        console.log(start.toISOString(), end.toISOString(), label);
      });


      $('#cm_DOA').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1"
      }, function (start, end, label) {


        console.log(start.toISOString(), end.toISOString(), label);
      });




    });


      function BindInformation(CustomerId) {

          $.ajax({
              type: "POST",
              data: '{"CustomerId":"' + CustomerId + '"}',
              url: "managecashcustomers.aspx/GetByCustomerId",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);
                  m_CustomrId = obj.Customer.Customer_ID;
                  $("#cm_ddlPrefix").val(obj.Customer.Prefix);
                  $("#cm_Name").val(obj.Customer.Customer_Name);
                  $("#cm_Address1").val(obj.Customer.Address_1);
                  $("#cm_Address2").val(obj.Customer.Address_2);
                  $("#cm_DOB").val(obj.Customer.strDOB);
                  $("#cm_DOA").val(obj.Customer.strDOA);
                  $("#cm_Discount").val(obj.Customer.Discount);
                  $("#cm_Tag").val(obj.Customer.Tag);
                  $("#cm_ContactNumber").val(obj.Customer.Contact_No);
                  $("#cm_EmailId").val(obj.Customer.EmailId);
                  $("#txtGSTNo").val(obj.Customer.GSTNo);
                  var city = obj.Customer.City_ID;

                  $("#cm_ddlCities").val(city);

                  var state = obj.Customer.State_ID;
                  $("#cm_ddlState").val(state);
                  var area = obj.Customer.Area_ID;
                  $("#cm_ddlArea").val(area);

                  $('#cm_FOC').prop("checked", false);
                  $('#cm_IsActive').prop("checked", false);

                  if (obj.Customer.FocBill == true) {
                      $('#cm_FOC').prop("checked", true);
                  }


                  if (obj.Customer.IsActive == true) {
                      $('#cm_IsActive').prop("checked", true);
                  }

                  $("#CustomerDialog").dialog({ autoOpen: true,

                      width: 800,
                      resizable: false,
                      modal: true
                  });

              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {
                  $.uiUnlock();
              }
          });


      }


      function GetVMResponse(Id, Title, IsActive, Status, Type) {
          $("#dvVMDialog").dialog("close");

          var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
          if (Type == "Area") {

              $("#cm_ddlArea").append(opt);

          }

          if (Type == "City") {

              $("#cm_ddlCities").append(opt);

          }

          if (Type == "State") {

              $("#cm_ddlState").append(opt);

          }



      }

      function OpenVMDialog(Type) {


          $.ajax({
              type: "POST",
              data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
              url: "managearea.aspx/LoadUserControl",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  $("#dvVMDialog").remove();
                  $("body").append("<div id='dvVMDialog'/>");
                  $("#dvVMDialog").html(msg.d).dialog({ modal: true });
              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

              }
          });

      }


    function ClearCustomerDialog() {
        validateForm("detach");
        m_CustomrId = -1;

        $("#cm_ddlPrefix").val();
        $("#cm_Name").val("");
        $("#cm_Address1").val("");
        $("#cm_Address2").val("");
       // $("#cm_DOB").val("");
        //$("#cm_DOA").val("");
        $("#cm_DOB").val("1900-01-01");
        $("#cm_DOA").val("1900-01-01");
        $("#cm_Discount").val("0");
        $("#cm_Tag").val("0");
        $("#cm_ContactNumber").val("");
        $("#cm_EmailId").val("");
        $("#CustomerDialog").dialog("close");
        $("#txtGSTNo").val("");
        $('#cm_IsActive').prop("checked", true);

        BindGrid();
    }

    function InsertUpdateCustomer() {

        if (!validateForm("formCustomer")) {
            return;
        }
 
        var objCustomer = {};
        objCustomer.Customer_ID = m_CustomrId;
        objCustomer.Prefix = $("#cm_ddlPrefix").val();
        objCustomer.Customer_Name = $("#cm_Name").val();
        objCustomer.Address_1 = $("#cm_Address1").val();
        objCustomer.Address_2 = $("#cm_Address2").val();
        objCustomer.Area_ID = $("#cm_ddlArea").val();
         
        objCustomer.City_ID = $("#cm_ddlCities").val();
        objCustomer.State_ID = $("#cm_ddlState").val();
        if ($("#cm_DOB").val() == "")
        {
            objCustomer.Date_Of_Birth ='1900-01-01';
        }
        else{
            objCustomer.Date_Of_Birth = $("#cm_DOB").val();
        }

        if ($("#cm_DOA").val() == "") {
            objCustomer.Date_Anniversary = '1900-01-01';
        }
        else {
            objCustomer.Date_Anniversary = $("#cm_DOA").val();
        }

       



        
        objCustomer.Discount = $("#cm_Discount").val();
        objCustomer.Contact_No = $("#cm_ContactNumber").val();

        objCustomer.EmailId = $("#cm_EmailId").val();
        objCustomer.Tag = $("#cm_Tag").val();
        //objCustomer.GSTNo = $("#txtGSTNo").val();
        var Foc = false;

        if ($("#cm_Tag").val() == "") {
          objCustomer.Tag = '0';
        }
        else {
          objCustomer.Tag = $("#cm_Tag").val();
        }

        if ($("#txtGSTNo").val() == "") {
            objCustomer.GSTNo = '0';
        }
        else {
            objCustomer.GSTNo = $("#txtGSTNo").val();
        }
        if ($('#cm_FOC').is(":checked")) {
            Foc = true;
        }

        objCustomer.FocBill = Foc;
        var IsActive = false;
        if ($('#cm_IsActive').is(":checked")) {
            IsActive = true;
        }

        objCustomer.IsActive = IsActive;

        var DTO = { 'objCustomer': objCustomer };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "managecashcustomers.aspx/InsertUpdateCustomer",
            data: JSON.stringify(DTO),
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {

                    alert("Sorry. Contact Number Already Registered with our Database");
                    $("#cm_ContactNumber").focus();
                    return;
                }
                else {
                    alert("CustomerSaved Successfully");
                    ClearCustomerDialog();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


            }

        });


    }
</script>


<style>
    .form-control {height: 30px;}
    /*[aria-labelledby=ui-id-1] {width: 100% !important;left: 0px !important;}*/
    .Customer_Information {
    width: 60%;
    margin: 25px auto !important;
    float: unset;
    box-shadow: 0px 0px 8px #9a9a9a;
    padding: 0px 25px 20px !important;}
    .Customer_Information table#formCustomer label {text-align: left;}
    
    
    @media screen and (max-width:767px) and (min-width:580px)
    {
        .Customer_Information table#formCustomer { margin: 0 auto;}
        .Customer_Information #formCustomer label {padding: 0px;}
        .Customer_Information #formCustomer select {margin-top: 0px; margin-bottom: 0px;}
        .Customer_Information #formCustomer input {margin-top: 0px; margin-bottom: 0px;}
        .Customer_Information {width: 78%; padding: 0px 25px !important;}
    }

</style>
  <asp:HiddenField ID="hdnDate" runat="server"/>

 <div class="x_panel">
  <div class="x_title" style="text-align:  center;">
                                    <h2 style="text-decoration:  underline;">Customer Information</h2>
                          <%--          <ul class="nav navbar-right panel_toolbox">
                                        
                                        <li class="dropdown">
                                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                    </ul>--%>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content Customer_Information">
                                    <br>
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="formCustomer" >
                             <tr>
                                 
                                 <td>
     
                              <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Prefix <span >*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">

                                             <asp:DropDownList  class="form-control" style="width: 60px;float:  left;" ID="cm_ddlPrefix" ClientIDMode="Static" runat="server"></asp:DropDownList>  <input type="text" class="txt form-control col-md-7 col-xs-12 validate required alphanumeric" placeholder="Customer Name"   id="cm_Name" style="float:  right;width: 137px;"> 
                                       
                                         
                                            </div>
                                        </div>
     
<%--                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Customer Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                              
                                            </div>
                                        </div>--%>
                                  
                             <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Address1 <span>*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                              
                                              <textarea class="txt form-control col-md-7 col-xs-12  validate required" id="cm_Address1"   ></textarea>
                                             <%--   <input type="text" class="txt form-control col-md-7 col-xs-12  validate required" id="cm_Address1" style="height: 72px;">--%>
                                             </div>
                                        </div>


                                      
   <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Area <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                           <table>
                                               <tr>
  <td> <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlArea" ClientIDMode="Static"  runat="server" style="width: 88px;
"></asp:DropDownList></td> 
                                                  <td>  <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlCities" ClientIDMode="Static"  runat="server" style="
    width: 100px;
   
"></asp:DropDownList></td>
                                                  
                                                                                                     <td><asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlState" ClientIDMode="Static"  runat="server" style="width: 97%;"></asp:DropDownList></td>
                                                  
                                                    <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                    <span class="fa fa-plus" onclick="javascript:OpenVMDialog('Area')"  style="font-size:18px;cursor:pointer;margin-top:-5px"></span>
                                                        </td>
                                               </tr>

                                           </table>
                                           
                                      
                                         
                                            </div>
                                        </div>


                                      <%--    <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">State <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <table>
                                                <tr>
                                                    <td> </td>
                                                      <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                        <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>

                                                    </td>
                                                </tr>

                                                </table>
                                          
                                      
                                         
                                            </div>
                                        </div>--%>



                                        
                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Contact<span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">


                                                <input type="text" class="txt form-control col-md-7 col-xs-12 validate required valNumber" placeholder="Mobil no"  id="cm_ContactNumber" style="width: 33%;"> 
                                                  <input type="text" class="txt form-control col-md-7 col-xs-12" placeholder="Email" id="cm_EmailId" style="width: 202px;"> 
                                            </div>
                                        </div>
                                        

                                   <%--       <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">EmailId<span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                              
                                            </div>
                                        </div>--%>

                                                               <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Discount <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                            
                                                <input type="text" class="txt form-control col-md- col-xs-12 validate required valNumber" value="0"    id="cm_Discount" disabled="disabled"/> 
                                            </div>
                                        </div>
                                     
                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">


                                                <input type="date"   class="txt form-control col-md-7 col-xs-12" id="cm_DOB"> 
                                            </div>
                                        </div>
                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Anniversary Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">


                                                <input type="date" class="form-control col-md-7 col-xs-12"  required="required" id="cm_DOA"> 
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tag 
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">


                                                <input type="text" class="txt form-control col-md-7 col-xs-12 validate required alphanumeric"  id="cm_Tag"> 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">GST No 
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">


                                                <input type="text" class="txt form-control col-md-7 col-xs-12"  id="txtGSTNo"> 
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">IsActive* 
                                            </label> <input type="checkbox" checked ="checked" id="cm_IsActive" />
                                            <div style="float:  right;width: 36%;">

                                           
                                                <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">FOC <span class="required">*</span> <input type="checkbox" id="cm_FOC" disabled="disabled"/>
                                           
                                            </div>
                                        </div>



<%--                                          
                                                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">FOC <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                           
                                           
                                            </div>
                                        </div> --%>
                             </td>
                             
                             <td valign="top">

                                           
                             

                                


                                      <%--    <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address2 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">--%>
                                             <%--   <input type="text" class="txt form-control col-md-7 col-xs-12" id="cm_Address2" style="height: 72px;"/>--%>
                                    <%--               <textarea class="txt form-control col-md-7 col-xs-12  validate required" id="cm_Address2"   ></textarea>
                                             </div>
                                        </div>--%>
                                  

                     <%--               <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">City <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                           <table>
                                               <tr>
                                                   <td>
                                                     
                                                   </td>
                                                     <td  style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                     <span class="fa fa-plus" onclick="javascript:OpenVMDialog('City')"  style="font-size:18px;cursor:pointer;margin-top:-5px"></span>

                                                   </td>
                                               </tr>

                                           </table>
                                          
                                      
                                         
                                         
                                            </div>
                                        </div>--%>

           

                     
                                        
 
                                        
                                        
                                        
                                          
                             </td>
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="text-align:  center;">
                                                
                                                <button class="btn btn-primary" type="button" onclick="javascript:InsertUpdateCustomer()"><i class="fa fa-external-link"></i> Submit</button>
                                           <button class="btn btn-danger" onclick="javascript:ClearCustomerDialog()" type="button"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                                <%-- <button class="btn btn-danger"  id="closkey" type="button"><i class="fa fa-mail-reply-all"></i> Close keyboard</button>--%>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
<%--<script>
    $(document).ready(function () {

        $("#closkey").click(function () {
     
            $("#mlkeyboard").css('display','none');
            //$("#closkey").css('display', 'none');
        });

    });
</script>--%>
 
                            
   
 

 
 