﻿function Bank(data) {
    this.BankId = ko.observable(data.BankId);
    this.Title = ko.observable(data.Title);
    this.Addresss = ko.observable(data.Addresss);

}


function ViewModel() {
    var self = this;

    self.Title = ko.observable('').extend({ required: true });
    self.Address = ko.observable('').extend({ required: true });



     self.errors = ko.validation.group([self.Title,self.Address]);
    self.hasError = function () {
        return self.errors().length > 0;
    };


    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    /*start validation, if no error create */
    self.create = function () {
        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }
        alert('No error found');
    };


    /*initialize vm*/
//    self.reset = function () {
//        self.firstNumber('');
//        self.secondNumber('');
//        self.thirdNumber('');
//        self.removeErrors();
//    };
//    self.init = function () {
//        self.reset();
//    };
}


$(document).ready(function () {
    var vm = new ViewModel();
    ko.applyBindingsWithValidation(vm);
});