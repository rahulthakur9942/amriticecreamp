﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StockAdjustment.ascx.cs" Inherits="Templates_StockAdjustment" %>

<script type ="text/javascript">


    $(document).ready(
    function () {


        $("#rdbDept").change(function () {

            BindDealer();

        });

        $("#rdbGroup").change(function () {

            BindDealer();

        });
        $("#rdbcompany").change(function () {

            BindDealer();

        });

        BindDealer();


        function BindDealer() {


            var Type = "";
            if ($("#rdbGroup").is(':checked') == true) {
                Type = "G";
            }
            else if ($("#rdbDept").is(':checked') == true) {
                Type = "D";
            }
            else if ($("#rdbcompany").is(':checked') == true) {
                Type = "C";
            }


            $("#ddlGroup").html("<option ></option>");

            $.ajax({
                type: "POST",

                contentType: "application/json; charset=utf-8",
                url: "StockAdjustment.aspx/BindDealers",
                data: '{"Type":"' + Type + '"}',
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html1 = "<option value = 0>--SELECT--</option>";

                    for (var i = 0; i < obj.GodownOptions.length; i++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                    }

                    $("#ddlGodown").html(html1);
                    $("#ddlGroup").append(obj.GroupStock);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled", true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });




                }
            });

        }


    }
    );
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px;background-color: #333;">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px; background-color:#333">
                                <div class="x_title">
                                    <h2 style="font-weight:bold; color:White; text-align:right;margin-left:7px;font-size:20px">Stock Adjustment</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                    <div class="form-group">
                                        
                                        <label class="col-sm-1 control-label" style="font-weight:bold; color:White;font-size:15px; margin-left:-13px" >RefNo:</label>
                                         <div class="col-md-2 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" style="width:130px;height:30px" class="form-control has-feedback-left" id="txtRefNo" >
                                                           
                                                        </div>

                                              <label class="col-sm-1 control-label" style="font-weight:bold; color:White;font-size:16px;margin-right:-12;margin-left:-9px">Date:</label>
                                             <div class="col-md-2 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" style="width:172px;height:30px" class="form-control has-feedback-left" id="txtBreakageDate" placeholder="MM/DD/YYYY" aria-describedby="inputSuccess2Status">
                                                            <span class="fa fa-calendar  form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span1" class="sr-only">(success)</span>
                                                        </div>
                                                
                                       
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="showColumn" class="btn-group" data-toggle="buttons">
                                                
                                                 <input type="radio" id= "rdbFinished" name="Finished" value="Finished" checked="checked"style="margin-left:8px;"  /> &nbsp;<label for="rdbFinished"  style="font-weight:bold; font-size:13px; color:White">Finished</label>   &nbsp;
                                             <input type="radio"  id="rdbSemiFinished" name="Finished" value="Semifinished"style="margin-left:21pxpx;"   />  &nbsp; <label for="rdbSemiFinished" style="font-weight:bold; font-size:13px;  color:White">Semi Finished</label>  &nbsp;
                                             <%--<input type="radio"  id ="rdbRaw" name="Finished" value="Raw"   /> --%>
                                             <input id="Radio1" type="radio" value="Raw" name="Finished" style="margin-left:16px; font-size:14px; margin-right: -20px;">
                                              <%--<label for="rdbRaw"style="font-weight:bold; color:White">Raw</label>  --%>
                                            <label style="font-weight: bold; color: White; text-align: right;font-size:14px; margin-left: 30px;margin-bottom:16px;" for="rdbRaw">Raw</label>
                                            
                                                  
                                                    
                                                </div>
                                            </div>
                                        </div>
                                   



                                   
                                         


                                        

                                        <div class="row">
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table width="100%" class="table" style="margin:0px">
                                    <tr>
                                    <th style="color: White; font-size: 15px;padding-top:6px; font-weight: bold;margin-left:30px;">Type</th>
                                    <%--<th style="font-weight:bold; color:White">Type</th>--%>
                                    <th style="font-weight:bold; color:White;padding-top:6px; font-size: 14px;">Godown</th>
                                    <th style="font-weight:bold; color:White; font-size:14px"><span id="spName">Group</span></th><th></th>
                                    </tr>
                                    <tr>
                                    <td style="width:48%;">
                                     <input type="radio"  id="rdbcompany" onchange="javascript:ChangeText('Company')"  name="Department"  value="Company"  /> &nbsp;<label for="rdbcompany" style="font-weight:bold; color:White; font-size:14px">Company</label> &nbsp;
                                            <input type="radio"  id="rdbDept" onchange="javascript:ChangeText('Department')"  name="Department"  value="Department"  /> &nbsp;<label for="rdbDept" style="font-weight:bold; color:White; font-size:14px">Department</label> &nbsp;
                                                        <input type="radio" onchange="javascript:ChangeText('Group')" id="rdbGroup" name="Department" value="Group" checked="" required /> <label for="rdbGroup" style="font-weight:bold; color:White;  font-size:14px">Group</label> 
                                                 </td>
                                    
                                    <td style="width:40%;">
                                    <select id="ddlGodown"  style="width:120px;height:30px">
                                                <option></option>
                                               </select>
                                    </td>

                                    <td>
                                    
                                       <select id="ddlGroup"  style="width:130px;height:30px">
                                                <option></option>
                                               </select>
                                    </td>
                                    

                                    <td>
                              
                                    </td>

                                    </tr>



                                      
                                        </table>
                                        </div>


                                        </div>


                                        <div class="row">
                                        <div class="col-md-12">
                                        <table width="100%" style="margin:10px 0 0  0;background:#FFFFD7;" class="table">
                                        <tr style="background-color:#333">
                                        <td>
                                 
                                               
                                               <input type="radio"  id="rdbCode" name="Code" value="Code"  /> &nbsp;<label style="font-weight:bold; color:White; font-size:14px" color:White" for="rdbCode;">Sort By Code </label> &nbsp;
                                                        <input type="radio"  id ="rdbName" name="Code" value="Name" checked="" required /> <label style="font-weight:bold; color:White; color:White;text-align:center; font-size:14px"for="rdbName">Sort By Name </label>
                                         
                                        
                                        
                                        </td>
                                       <%-- <td style="text-align:right;padding-right:20px">
                                                 <input type="radio"  id="rdbUnit" name="Column" value="Unit"  /> &nbsp; <label style="font-weight:normal" for="rdbUnit">Show Unit Column </label>&nbsp;
                                                 
                                                    
                                                        <input type="radio" id="rdbRate" name="Column" value="Rate" checked="" required /> <label style="font-weight:normal" for="rdbRate">Sort Rate Column</label>
                                              
                                           
                                        </td>--%>
                                        
                                        </tr>
                                        
                                        </table>
                                        
                                        </div>
                                        
                                        </div>


                            
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    
                          


                    <div class="row">
                  <div class="col-md-12">
                            <div class="x_panel" style="background:seashell;padding:0px; background:transparent">
                               
                                <div class="x_content" style="padding-bottom:0px">
                                  
                               <table>
                               <tr><td>
                               <table style="border-collapse:separate;border-spacing:2px; background-color: #333">
<thead>
<tr  style="background-color: #db3030">
<th style="font-weight:normal; color:White;text-align:center">Item/Code</th>
<th style="font-weight:normal; color:White;text-align:center">Code</th>
<th style="font-weight:normal; color:White;text-align:center">Name</th>
<th style="font-weight:normal; color:White;text-align:center">Stock</th>
<th style="font-weight:normal; color:White;text-align:center">ActualStock</th>
<th style="font-weight:normal; color:White;text-align:center">Rate</th>
<th style="font-weight:normal; color:White;text-align:center">MRP</th>
<th style="font-weight:normal; color:White;text-align:center">Adjusted</th>
<th style="font-weight:normal; color:White;text-align:center">Amount</th>
</tr>

</thead>
<tbody>
<tr>
<td id="DKID">
 
 


<select style="width:154px" id="ddlProducts" class="form-control"></select>
</td>
<td>
<input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox"  />
</td><td><input id="txtName" type="text"  class="form-control customTextBox"   style="width:120px"/></td>
<td><input type="text" id="txtStock" class="form-control customTextBox"  readonly="readonly"  /></td>
<td><input type="text" id="txtQty" class="form-control customTextBox"  /></td>
<td><input type="text" id="txtRate"  class="form-control customTextBox" readonly="readonly"  /></td>
<td><input type="text" id="txtMarketPrice" class="form-control customTextBox" readonly="readonly" /></td>
<td><input type="text" id="txtAdjusted" class="form-control customTextBox" /></td>
<td>
<input type="text"  class="form-control customTextBox" readonly="readonly" id="txtAmount"  /></td>
<td>
<%--<button id="Button1" class="btn btn-success" style="width: 52px; margin-right: -5px; background-color: Black; border-color: white; font-weight: bold; height: 26px; margin-bottom: 0px; margin-top: -1px;" type="button">Add</button>--%>

<button type="button" style="width: 52px; margin-right: -5px; background-color: Black; border-color: white; font-weight: bold; height: 26px; margin-bottom: 0px; margin-top: -1px;" class="btn btn-success" id="btnAddKitItems">Add</button></td></tr>
</tbody>

</table>
                               </td></tr>
                              
                               
                               </table>  

                                   
                                </div>
                            </div>

                      
                        </div>
                    

                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:150px;overflow-y:auto;min-height:150px;background-color:#333">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content"> 

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr>
<th style="font-weight:bold; color:White;font-size:20px">Code</th>
<th style="font-weight:bold; color:White;font-size:20px" >Name</th>
<th style="font-weight:bold; color:White;font-size:20px">Bal.Stock</th>
<th style="font-weight:bold; color:White;font-size:20px">ActualStock</th>
<th style="font-weight:bold; color:White;font-size:20px">Rate</th>
<th style="font-weight:bold; color:White;font-size:20px">MRP</th>
<th  style="font-weight:bold; color:White;font-size:20px">Adjusted</th>
<th style="font-weight:bold; color:White;font-size:20px">Amount</th>
</tr>
</thead>
<tbody id="tbKitProducts">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content"style="margin-top:0px;padding:0px;">
                                    
                                       <table style="width:100%">
                                     <tr>
                                    <td style="background-color:#333">
                                     <table >
                                     <tr><td>
                                       <div id="btnSave" style="margin-top:9px;font-weight:bold; margin-left:11px;width: 128px;background-color:Black;border-color: white;" data-bind="click: $root.PlaceOrder" class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialog" style="margin-top:9px;font-weight:bold;margin-left:11px;width: 128px;background-color:Black;border-color: white;" data-bind="click: $root.CancelOrder" class="btn btn-danger"   > <i class="fa fa-mail-reply-all"></i>Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>

                            </div>
                        </div>
  
