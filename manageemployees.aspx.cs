﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Data;

public partial class manageemployees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCities();
            BindArea();
            BindState();
            BindDesignation();
        }
        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EMPLOYEES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }



    void BindCities()
    {

        ddlCity.DataSource = new CitiesBLL().GetAll();
        ddlCity.DataValueField = "City_ID";
        ddlCity.DataTextField = "City_Name";
        ddlCity.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose City--";
        li1.Value = "0";
        ddlCity.Items.Insert(0, li1);

    }
    void BindArea()
    {

        ddlArea.DataSource = new AreaBLL().GetAll();
        ddlArea.DataValueField = "Area_ID";
        ddlArea.DataTextField = "Area_Name";
        ddlArea.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Area--";
        li1.Value = "0";
        ddlArea.Items.Insert(0, li1);

    }
    void BindState()
    {

        ddlState.DataSource = new StateBLL().GetAll();
        ddlState.DataValueField = "State_ID";
        ddlState.DataTextField = "State_Name";
        ddlState.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose State--";
        li1.Value = "0";
        ddlState.Items.Insert(0, li1);

    }
    void BindDesignation()
    {
        designation designation = new designation();
        DataTable dt = designation.ManageDesignation();
        ddldesignation.DataSource = dt;
        ddldesignation.DataValueField = "desgid";
        ddldesignation.DataTextField = "desgname";
        ddldesignation.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Designation--";
        li1.Value = "0";
        ddldesignation.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string Insert(Employees objEmployee)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EMPLOYEES));

        string[] arrRoles = sesRoles.Split(',');


        if (objEmployee.Code == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        objEmployee.UserId = Id;

      
        objEmployee.BranchId = Branch;






        status = new EmployeeBLL().InsertUpdate(objEmployee);
        var JsonData = new
        {
            employee = objEmployee,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 EmployeeId)
    {

        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EMPLOYEES));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Employees objEmployee = new Employees()
        {
            Code = EmployeeId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new EmployeeBLL().DeleteEmployee(objEmployee);
        var JsonData = new
        {
            employee = objEmployee,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}