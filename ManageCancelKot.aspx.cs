﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class ManageCancelKot : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
         
        }
    }

    [WebMethod]

    public static string Delete(Int32 KotNo)
    {
        KOT objKot = new KOT()
        {
            KOTNo = KotNo,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new kotBLL().cancelKOt(objKot);
        var JsonData = new
        {
            kot = objKot,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByKotNo(int KotNo)
    {

     
        List<KotDetail> lstKot = new kotBLL().GetByKotNo(KotNo);

        var JsonData = new
        {
            productLists = lstKot
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }
}