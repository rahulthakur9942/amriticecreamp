﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    public string DayOpen { get { return Request.QueryString["DayOpen"] != null ? Convert.ToString(Request.QueryString["DayOpen"]) : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (DayOpen == "Close")
        {
            Response.Write("<script>alert('Day is Not Opened.Please Open Your Day')</script>");

        }
    }
}