﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="ManageTable.aspx.cs" Inherits="ManageTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <style>
    th {
      text-align: center;
    }
    .table table  tbody  tr  td a ,
.table table  tbody  tr  td  span {
position: relative;
float: left;
padding: 6px 12px;
margin-left: -1px;
line-height: 1.42857143;
color: #337ab7;
text-decoration: none;
background-color: #fff;
border: 1px solid #ddd;
}

.table table > tbody > tr > td > span {
z-index: 3;
color: #fff;
cursor: default;
background-color: #337ab7;
border-color: #337ab7;
}

.table table > tbody > tr > td:first-child > a,
.table table > tbody > tr > td:first-child > span {
margin-left: 0;
border-top-left-radius: 4px;
border-bottom-left-radius: 4px;
}

.table table > tbody > tr > td:last-child > a,
.table table > tbody > tr > td:last-child > span {
border-top-right-radius: 4px;
border-bottom-right-radius: 4px;
}

.table table > tbody > tr > td > a:hover,
.table   table > tbody > tr > td > span:hover,
.table table > tbody > tr > td > a:focus,
.table table > tbody > tr > td > span:focus {
z-index: 2;
color: #23527c;
background-color: #eee;
border-color: #ddd;
}
  </style>
  <form runat="server" id="formID" method="post">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
        <asp:UpdateProgress ID="updateProgress" runat="server">
          <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
              <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
            </div>
          </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <div class="right_col" role="main">
          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3>Manage Table</h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Add/Edit MangeTable</h2>

                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed">

                  <tr>
                    <td class="headings">Table Name:</td>
                    <td>
                      <input type="text" required name="txtTableName" class="form-control validate required alphanumeric" runat="server" data-index="1" id="txtTableName" style="width: 213px" /></td>
                      <td>
                       MultiPrint:   <asp:CheckBox ID="chkmultiprint" runat="server" /></td>
                  </tr>

                  <tr>
                    <td></td>
                    <td>
                      <asp:Button ID="btnsave" class="btn btn-primary btn-small" runat="server" Text="Submit" OnClick="btnsave_Click" />

                    </td>
                  </tr>


                </table>





              </div>

            </div>


            <div class="x_panel">

              <div class="x_content">

                <div class="youhave">

                  <div class="col-md-4">
                    <asp:GridView ID="gv_display" runat="server" OnPageIndexChanging="OnPaging"
                      EnableModelValidation="True" AllowPaging="True"
                      PagerSettings-PageButtonCount="10" PagerStyle-HorizontalAlign="Right" OnRowCommand="gv_display_OnRowCommand" AutoGenerateColumns="false"
                      class="table table-striped table-bordered table-hover" PageSize="13">

                      <Columns>
                        <asp:BoundField DataField="TableID" HeaderText="TableID" HeaderStyle-HorizontalAlign="Center">
                          <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                          <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TableName" HeaderText="Table Name" HeaderStyle-HorizontalAlign="Center">
                          <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                          <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:TemplateField>
                          <ItemTemplate>

                            <asp:LinkButton ID="lnkedit" runat="server" CommandName="select" CommandArgument='<%#Eval("TableID") %>'><i class="fa fa-edit" style="font-size:20px;color:red"></i></asp:LinkButton>

                          </ItemTemplate>

                        </asp:TemplateField>

                      </Columns>


                    </asp:GridView>
                  </div>

                </div>

              </div>
            </div>


          </div>
          <!-- /page content -->

          <!-- footer content -->
          <%--     <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>--%>
          <!-- /footer content -->

        </div>
        <script type="text/javascript">
          $(document).ready(function () {
            $(".table_class tbody").eq(1).children().children().attr('style', 'width:20px')
          })
        </script>
      </ContentTemplate>
    </asp:UpdatePanel>
  </form>
</asp:Content>

