﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managediscounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.DISCOUNT));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string FillControl(string Category)
    {
        string Options = "";
        if (Category == "CompanyDiscount")
        {
           Options = new CompanyBLL().GetOptions();
        }
        else if (Category == "GroupDiscount")
        {
            Options = new PropGroupBLL().GetOptions();
        }
        else if (Category == "SubGroupDiscount")
        {
            Options = new PropSGroupBLL().GetOptions();
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            OptionData = Options,
          

        };
        return ser.Serialize(JsonData);
      
    }


    [WebMethod]
    public static string Insert(DiscountMaster objDiscount)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objDiscount.UserId = Id;

        string date = objDiscount.Start_Date.ToString("d") + " " + objDiscount.START_TIME;
        objDiscount.Start_Date = Convert.ToDateTime(date);

        string date2 = objDiscount.End_Date.ToString("d") + " " + objDiscount.END_TIME;
        objDiscount.End_Date = Convert.ToDateTime(date2);
        
        int status = new DiscountBLL().InsertUpdate(objDiscount);
        var JsonData = new
        {
            discount = objDiscount,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 DiscountId)
    {
        DiscountMaster objDiscount = new DiscountMaster()
        {
            Discount_ID = DiscountId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new DiscountBLL().DeleteDiscount(objDiscount);
        var JsonData = new
        {
            discount = objDiscount,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}