﻿/// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
/// <reference path="../Scripts/knockout.validation.js" />

 
function Product(data) {
    this.ProcessingId = ko.observable(data.ProcessingId);
    this.Item_Code = ko.observable(data.Item_Code);
    this.Item_Name = ko.observable(data.Item_Name);
    this.ProcessingTime = ko.observable(data.ProcessingTime);
    this.Status = ko.observable(data.Status);
    this.TableNo = ko.observable(data.TableNo);
    this.ImageUrl = ko.observable(data.ImageUrl);
    this.IsStarted = ko.observable(data.IsStarted);
    this.IsNew = ko.observable(data.IsNew);
    this.BillNowPrefix = ko.observable(data.BillNowPrefix);
    this.ButtonText = ko.observable(data.ButtonText);
    this.KotImagePath = ko.observable(data.KotImagePath);
    this.IsNewDelivery = ko.observable(data.IsNewDelivery);
    this.IsDelivered = ko.observable(data.IsDelivered);
    this.IsActive = ko.observable(data.IsActive);
    this.ProcessedStatus = ko.observable(data.ProcessedStatus);
    this.Remarks = ko.observable(data.Remarks);
}


ko.observableArray.fn.distinct = function (prop) {

    var target = this;
    target.index = {};
    target.index[prop] = ko.observable({});

    ko.computed(function () {
        //rebuild index
        var propIndex = {};

        ko.utils.arrayForEach(target(), function (item) {
            var key = ko.utils.unwrapObservable(item[prop]);
            if (key) {
                propIndex[key] = propIndex[key] || [];
                propIndex[key].push(item);
            }
        });

        target.index[prop](propIndex);
    });

    return target;
};


 



function KOTViewModel() {
    var self = this;
    self.ProcessingId = ko.observable();
    self.Products = ko.observableArray([]).distinct('BillNowPrefix');  






    self.uniqueCountries = ko.computed(function () {

        var people = self.Products(),
          index = {},

          length = people.length,
          i, country;

        var uniqueCountries =ko.observableArray([]);
        // var uniqueParent = ko.observableArray([]);


        for (i = 0; i < length; i++) {


            country = people[i].BillNowPrefix();

            if (!index[country]) {
                index[country] = true;

 

                uniqueCountries.push(country);
            }
        }


        return uniqueCountries();
    });












      self.RefreshList = function () {
 
       
   
    $.ajax({
        type: "POST",
        url: 'kotscreen.aspx/FetchProducts',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var products = $.map(results.d, function (item) {
              return new Product(item)
            });
            self.Products(products);


        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });
     

    }



    self.StartProcessing = function (product) {

        if (product.IsActive() == false) {
            alert("Item Does not belong to this Department");
            return;

        }


        if (product.Status() == "Started") {
            alert("Processing already Started");
            return;
        }


        var Status = "Started";

        $.uiLock('');

        $.ajax({
            type: "POST",
            url: 'kotscreen.aspx/UpdateStatus',
            data: '{ "ProcessingId": "' + product.ProcessingId() + '", "Status": "' + Status + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {

                $.uiUnlock();



                if (result.d == "1") {
                    product.KotImagePath("images/stop.png");
                    product.Status("Started");
                    product.IsStarted(true);
                    product.IsNew(false);
                    product.ButtonText("Started");
                    //  alert("Processing Started");


                    //             self.Products.sort(function (a, b) {

                    //                 return a.Status().toLowerCase() > b.Status().toLowerCase() ? 1 : -1;

                    //             });

                }






            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });
    }




    self.MarkProcessed = function (product) {

        if (product.Status() == "New") {
            alert("Please first Start Processing of Item");
            return;
        }


        var Status = "Processed";
        $.uiLock('');
        $.ajax({
            type: "POST",
            url: 'kotscreen.aspx/UpdateStatus',
            data: '{ "ProcessingId": "' + product.ProcessingId() + '", "Status": "' + Status + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {



                $.uiUnlock();
                if (result.d == "1") {


                    product.IsStarted(false);
                    product.IsNewDelivery(true);
                    product.ProcessedStatus(true);
                    //self.Products.remove(product);
                }






            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });
    }



    self.GetNewList = function () {
 


  $.ajax({
        type: "POST",
        url: 'kotscreen.aspx/FetchProducts',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var products = $.map(results.d, function (item) {
              return new Product(item)
            });
            self.Products(products);


        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });

//        $.ajax({
//            type: "POST",
//            url: 'kotscreen.aspx/GetNewProcessingItems',
//            data: ko.toJSON({ data: self.Products }),
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (results) {
//          
//            
//                var products = $.map(results.d, function (item) {


//                    self.Products.push(new Product({
//                        ProcessingId:item.ProcessingId,
//                        Item_Code:item.Item_Code,
//                        Item_Name: item.Item_Name,
//                        ProcessingTime: item.ProcessingTime,
//                        Status: item.Status,
//                        TableNo: item.TableNo,
//                        ImageUrl: item.ImageUrl,
//                        IsStarted :false,
//                        IsNew:true,
//                        ButtonText:"Start"

//                        
//                    }));



//                });



//             self.Products.sort(function (a, b) {

//                  return a.Status().toLowerCase() > b.Status().toLowerCase() ? 1 : -1;

//             });


//            },
//            error: function (err) {
//                alert(err.status + " - " + err.statusText);
//            }
//        });
//   
    
 





    }

         setInterval(self.GetNewList, 10000);


  //------------------FETCH ALL GROUPS ON PAGE LOAD-------------------
  
    
    $.ajax({
        type: "POST",
        url: 'kotscreen.aspx/FetchProducts',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var products = $.map(results.d, function (item) {
              return new Product(item)
            });
            self.Products(products);


        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });
 

    //--------------------------------------------------------------------------


}


$(document).ready(function () {
    ko.applyBindings(new KOTViewModel());
});