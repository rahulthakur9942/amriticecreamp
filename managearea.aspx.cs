﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.IO;
using DevExpress.Utils.OAuth.Provider;
using System.Web.Services.Description;

public partial class managearea : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.AREA));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string LoadUserControl(string Id,string Type)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/ucVariables.ascx");
            (userControl.FindControl("ltContent") as Literal).Text = "<input type='hidden' id='hdnVMID' value='" + Id + "'/><input type='hidden' id='hdnVMType' value='" + Type + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }




    [WebMethod]
    public static string Insert(int AreaId, string Title)
    {

        Areas objArea = new Areas()
        {
            Area_ID = AreaId,
            Area_Name = Title.Trim().ToUpper(),
          

        };
        int status = new AreaBLL().InsertUpdate(objArea);
        var JsonData = new
        {
            Area = objArea,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]

    public static string Delete(Int32 AreaId)
    {
        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.AREA));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }

        Areas objArea = new Areas()
        {
            Area_ID = AreaId,
           
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();
        
        Status =  new AreaBLL().DeleteArea(objArea);
        
        var JsonData = new
        {
           Area = objArea,
           status  = Status 
        };
        return ser.Serialize(JsonData);
    }

}