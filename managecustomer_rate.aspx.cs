﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class managecustomer_rate : System.Web.UI.Page
{
    mst_customer_rate msr = new mst_customer_rate();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
            //binditemlist();
            bind_ddcustomer();
            //bind_dditem();

        }
    }

    protected void bindlist()
    {
        msr.req = "bindlist";
        msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        DataTable dt = msr.bind_item_dd();
        rpt_itemlist.DataSource = dt;
        rpt_itemlist.DataBind();
        if (dt.Rows.Count > 0)
        {

            //btnsave.Visible = true;
        }

    }

    public  void bind_ddcustomer()
    {
        msr.req = "bind_ddcustomer";
        DataTable dt = msr.bind_item_dd();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "customer_name";
        dd_customername.DataValueField = "cst_id";
        dd_customername.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select Customer-", "0");
        dd_customername.Items.Insert(0, listItem1);
     
    }

    //public void bind_dditem()
    //{
    //    msr.req = "bind_dditem";
    //    DataTable dt = msr.bind_gride_dd();
    //    dd_itemname.DataSource = dt;
    //    dd_itemname.DataTextField = "item_name";
    //    dd_itemname.DataValueField = "item_Code";
    //    dd_itemname.DataBind();
    //    System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select Item-", "0");
    //    dd_itemname.Items.Insert(0, listItem1);
    //}



    protected void btnsave_Click(object sender, EventArgs e)
    {
        msr.req = "del_cst";
        msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        msr.insert_update_customer();
        foreach (RepeaterItem item in rpt_itemlist.Items)
        {
            string itemcode =((Label)item.FindControl("lblitemcode")).Text;
            decimal rate = Convert.ToDecimal(((System.Web.UI.HtmlControls.HtmlInputControl)item.FindControl("tbrate")).Value);
            //if(rate>0)
            //{
            msr.req = "insert";
            msr.item_Code = itemcode;
            msr.rate = rate;
            msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
            msr.insert_update_customer();
            //}

        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);
    }

    protected void SubmiteDetails(object sender, EventArgs e)
    {
        RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
        string itemcode = (item.FindControl("lblitemcode") as Label).Text;
        string rate = (item.FindControl("tbrate") as TextBox).Text;

        msr.req = "insert";
        msr.item_Code = itemcode;
        msr.rate = Convert.ToDecimal(rate);
        msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        msr.insert_update_customer();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);

    }
    protected void rpt_itemlist_Itemcommand(object source, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "SaveItem")
        //{
        //    string itemcode = e.CommandArgument.ToString();
        //    System.Web.UI.HtmlControls.HtmlInputText rate =(System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("tbrate");
        //    msr.req = "insert";
        //    msr.item_Code = itemcode;
        //    msr.rate =Convert.ToDecimal(rate);
        //    msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        //    msr.insert_update_customer();

        //}
    }

    protected void dd_customername_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindlist();
    }
}