﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="kotscreen.aspx.cs" Inherits="kotscreen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <script src="Scripts/jquery-2.0.3.min.js"></script>
    <script src="Scripts/knockout-3.0.0.js"></script>
     <script src="js/jquery.uilock.js" type="text/javascript"></script>
<link href="css3/bootstrap.css" rel="stylesheet" />
<link href="css3/KOTStylesheet.css" rel="stylesheet" />
    <script src="ViewModel/KotViewModel.js" type="text/javascript"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Index</title>
 


<style type="text/css">
    
    .SelectedRow
    {
    background:#FDF0DF;    
    }
    .ProcessedRow
    {
        background:palegreen;
        }

#table tr
{
	border-bottom:dashed 1px silver;
	 
}

#table tr td
{
	padding:5px;
	font-weight:bold;
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	color:black;
}
	
</style>


</head>



<body>

<div class="container-fluid">
<div class="background">

<div class="row">
<div class="col-lg-12">
<div class="butt">
<table style="width:100%">
<tr><td width="100px"><div class="btn btn-danger" style="cursor:pointer" data-bind="click: $root.RefreshList"  >REFRESH</div></td>
<td align="center"> <div style="color:White;font-weight:bold;font-size:20px">
<asp:Literal ID="ltDepartment" runat="server"></asp:Literal>
</div></td>
</tr>
</table>


</div>
</div>
</div>

<div class="row">
<div class="col-md-12">

<div data-bind="foreach:uniqueCountries"  >


<div class="col-md-6" style="padding:0px">

<div class="menubg">

 

<div class="bgh" style="max-height:300px;min-height:300px;overflow-y:scroll">

<span data-bind="text:$data" style="font-size:25px"></span>

 
<table width="100%" id="table">
<tbody data-bind="foreach: $root.Products.index.BillNowPrefix()[$data]">


<tr  data-bind="css: { 'SelectedRow': IsStarted,'ProcessedRow':ProcessedStatus}">
<%--<td data-bind="text: Item_Code" > </td>--%>
<td style="width:400px">
<span   data-bind="text: Item_Name" style="font-size:20px;"></span><br />
<span data-bind="text:Remarks"></span>


 </td>
<%--<td data-bind="text: ProcessingTime"  style="font-size:20px"> </td>--%>
<%--<td data-bind="text: BillNowPrefix"  style="font-size:20px"> </td>--%>
<%--<td>
 



 
</td>--%>
<td>

<div data-bind="visible:IsActive">

<div class="btn" style="cursor:pointer;font-size:20px;width:60px" data-bind="click: $root.StartProcessing,css: { 'btn-success': IsStarted,'btn-warning': IsNew },visible:IsNew" >

 
 
<div  style="width:30px;float:right;text-align:left;">
<img data-bind="attr:{src: KotImagePath}" style="height:25px;"> 
 
</div>
 



</div>


 
<div class="btn"  style="padding-right:2px;padding-left:2px;cursor:pointer;font-size:20px;width:60px" data-bind="click: $root.MarkProcessed,css:{'btn-danger': IsStarted,'btn-warning':IsNew},visible:IsStarted " >

<%-- 
<img src="images/done.png" style="height:25px" />
 --%>
  
<img src="images/stop.png" style="height:25px" />
 

</div>


<div class="btn btn-success" data-bind="visible:IsNewDelivery"  style="padding-right:2px;padding-left:2px;cursor:pointer;font-size:20px;width:60px" >
 
<img src="images/done.png" style="height:25px" />
 

</div>

<div class="btn btn-success" data-bind="visible:IsDelivered"  style="padding-right:2px;padding-left:2px;cursor:pointer;font-size:20px;width:60px" >
 
<img src="images/delivered.png" style="height:25px" />
 

</div>

</div>



</td>
</tr>

</tbody> 



</table>	


 

</div>
</div>

</div>
</div>

<br>
<br>

</div>
</div>

</div>


</div>


</body>
</html>

