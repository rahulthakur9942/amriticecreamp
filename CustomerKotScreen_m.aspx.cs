﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class CustomerKotScreen_m : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cookies[Constants.posid].Value = "3";
    
        hdnempid.Value = HttpContext.Current.Request.Cookies[Constants.EmployeeId].Value;
        lblusername.Text = HttpContext.Current.Request.Cookies[Constants.EmployeeName].Value;
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
           // BindSteward();

            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }
        }
    }

    //public void BindSteward()
    //{
    //    //ddlsteward.DataSource = new EmployeeBLL().GetAll();
    //    //ddlsteward.DataTextField = "Name";
    //    //ddlsteward.DataValueField = "Code";
    //    //ddlsteward.DataBind();

    //    ListItem li1 = new ListItem();
    //    li1.Text = "Choose";
    //    li1.Value = "0";
    //    //ddlsteward.Items.Insert(0, li1);

    //    ddlTableOpt.DataSource = new TablesBLL().GetAll();
    //    ddlTableOpt.DataTextField = "TableName";
    //    ddlTableOpt.DataValueField = "TableID";
    //    ddlTableOpt.DataBind();
    //    ddlTableOpt.Items.Insert(0, li1);



    //    ddlSourceTable.DataSource = new TablesBLL().GetAllKotTables();
    //    ddlSourceTable.DataTextField = "TableName";
    //    ddlSourceTable.DataValueField = "TableID";
    //    ddlSourceTable.DataBind();
    //    ddlSourceTable.Items.Insert(0, li1);

    //    ddlDestTable.DataSource = new TablesBLL().GetAll();
    //    ddlDestTable.DataTextField = "TableName";
    //    ddlDestTable.DataValueField = "TableID";
    //    ddlDestTable.DataBind();
    //    ddlDestTable.Items.Insert(0, li1);


    //}

    [WebMethod]
    public static string BindTables()
    {

        string TablesData = new TablesBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            TableOptions = TablesData

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetKotDetail(int KotNo)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<KotDetail> lstKot = new kotBLL().GetByKotNoDetail(KotNo, Branch);

        var JsonData = new
        {
            productLists = lstKot
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindCategories()
    {
        int CategoryId = 0;
        Settings objSettings = new Settings();
        string catData = new CategoriesBLL().GetCategoriesHTML(objSettings, out CategoryId);
        var JsonData = new
        {
            categoryData = catData,
            setttingData = objSettings,
            CategoryId = CategoryId
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string AdvancedSearch(int CategoryId, string Keyword)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string prodData = new ProductBLL().AdvancedSearch(CategoryId, Keyword.Trim(), Branch);
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByItemCode(string ItemCode, int billtype)
    {
        Product objProduct = new Product() { Item_Code = ItemCode };
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new ProductBLL().GetByItemCode(objProduct, Branch, billtype);
        var JsonData = new
        {
            productData = objProduct
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }






    [WebMethod]
    public static string InsertUpdate(int MKOTNo, int TableID, int PaxNo, int R_Code, int M_Code, decimal Value, decimal DisPercentage, decimal DisAmount, decimal ServiceCharges, decimal TaxAmt, decimal TotalAmount, bool Complementary
        , bool Happy, int EmpCode, string itemcodeArr, string priceArr, string qtyArr, string AmountArr, string taxArr, string TaxAmountArr, string AddOnArr, string EditValArr, bool TakeAway)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        // Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        //Int32 Employee = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.EmployeeId].Value);
        Int32 Employee = EmpCode;
        KOT objKot = new KOT()
        {
            MKOTNo = MKOTNo,
            TableID = TableID,
            PaxNo = PaxNo,
            R_Code = R_Code,
            M_Code = M_Code,
            Value = Value,
            DisPercentage = DisPercentage,
            DisAmount = DisAmount,
            ServiceCharges = ServiceCharges,
            TaxAmount = TaxAmt,
            TotalAmount = TotalAmount,
            Complementary = Complementary,
            Happy = Happy,
            EmpCode = Employee,
            KOTNo = 1,
            BranchId = Branch,
            TakeAway = TakeAway,
        };

        string[] ItemCode = itemcodeArr.Split(',');
        string[] Price = priceArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Amount = AmountArr.Split(',');
        string[] Tax = taxArr.Split(',');
        string[] TaxAmount = TaxAmountArr.Split(',');
        string[] AddOn = AddOnArr.Split(',');
        string[] EditVal = EditValArr.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax");
        dt.Columns.Add("Tax_Amount");
        dt.Columns.Add("AddOn");


        for (int i = 0; i < ItemCode.Length; i++)
        {
            if (Convert.ToInt16(EditVal[i]) == 0)
            {
                DataRow dr = dt.NewRow();
                dr["Item_Code"] = ItemCode[i];
                dr["Rate"] = Convert.ToDecimal(Price[i]);
                dr["Qty"] = Convert.ToDecimal(Qty[i]);
                dr["Amount"] = Convert.ToDecimal(Amount[i]);
                dr["Tax"] = Convert.ToDecimal(Tax[i]);
                dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
                dr["AddOn"] = Convert.ToString(AddOn[i]);
                dt.Rows.Add(dr);
            }
        }




        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new kotBLL().Insert(objKot, dt, Employee);
        var JsonData = new
        {
            Bill = objKot,
            BNF = objKot.KOTNo,
            Status = status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetKotByTableNo(int TableNo)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<KOT> lstKot = new kotBLL().GetAllKotByTableNo(TableNo, Branch);

        var JsonData = new
        {
            KOT = lstKot
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string TransfrTable(int FromTable, int ToTable, string qry)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int SourceTable = FromTable;
        int DestTable = ToTable;
        string fqry = qry;
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new kotBLL().TransferTable(SourceTable, DestTable, Branch, fqry);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string KOTprint()
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        List<KotPrint> objkot = new kotBLL().GetDataByUserNo(UserNo);

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            kotprint = objkot

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string EmployeeLoginCheck(string Steward, string Password)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        Employees objEmployee = new Employees()
        {
            Name = Steward,
            KotPswrd = Password,

        };



        Int32 status = new kotBLL().EmployeeLoginCheck(objEmployee, Branch);



        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            Status = status

        };
        return ser.Serialize(JsonData);

    }
}